
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CrossIndustryInvoiceType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CrossIndustryInvoiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ExchangedDocumentContext" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ExchangedDocumentContextType"/&gt;
 *         &lt;element name="ExchangedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ExchangedDocumentType"/&gt;
 *         &lt;element name="SupplyChainTradeTransaction" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}SupplyChainTradeTransactionType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CrossIndustryInvoiceType", namespace = "urn:un:unece:uncefact:data:standard:CrossIndustryInvoice:100", propOrder = {
    "exchangedDocumentContext",
    "exchangedDocument",
    "supplyChainTradeTransaction"
})
public class CrossIndustryInvoiceType {

    @XmlElement(name = "ExchangedDocumentContext", required = true)
    protected ExchangedDocumentContextType exchangedDocumentContext;
    @XmlElement(name = "ExchangedDocument", required = true)
    protected ExchangedDocumentType exchangedDocument;
    @XmlElement(name = "SupplyChainTradeTransaction", required = true)
    protected SupplyChainTradeTransactionType supplyChainTradeTransaction;

    /**
     * Ruft den Wert der exchangedDocumentContext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExchangedDocumentContextType }
     *     
     */
    public ExchangedDocumentContextType getExchangedDocumentContext() {
        return exchangedDocumentContext;
    }

    /**
     * Legt den Wert der exchangedDocumentContext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangedDocumentContextType }
     *     
     */
    public void setExchangedDocumentContext(ExchangedDocumentContextType value) {
        this.exchangedDocumentContext = value;
    }

    /**
     * Ruft den Wert der exchangedDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExchangedDocumentType }
     *     
     */
    public ExchangedDocumentType getExchangedDocument() {
        return exchangedDocument;
    }

    /**
     * Legt den Wert der exchangedDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangedDocumentType }
     *     
     */
    public void setExchangedDocument(ExchangedDocumentType value) {
        this.exchangedDocument = value;
    }

    /**
     * Ruft den Wert der supplyChainTradeTransaction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SupplyChainTradeTransactionType }
     *     
     */
    public SupplyChainTradeTransactionType getSupplyChainTradeTransaction() {
        return supplyChainTradeTransaction;
    }

    /**
     * Legt den Wert der supplyChainTradeTransaction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplyChainTradeTransactionType }
     *     
     */
    public void setSupplyChainTradeTransaction(SupplyChainTradeTransactionType value) {
        this.supplyChainTradeTransaction = value;
    }

}
