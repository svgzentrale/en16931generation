
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java-Klasse für TaxCategoryCodeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TaxCategoryCodeType"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;urn:un:unece:uncefact:data:standard:QualifiedDataType:100&gt;TaxCategoryCodeContentType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxCategoryCodeType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100", propOrder = {
    "value"
})
public class TaxCategoryCodeType {

    @XmlValue
    protected TaxCategoryCodeContentType value;

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TaxCategoryCodeContentType }
     *     
     */
    public TaxCategoryCodeContentType getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxCategoryCodeContentType }
     *     
     */
    public void setValue(TaxCategoryCodeContentType value) {
        this.value = value;
    }

}
