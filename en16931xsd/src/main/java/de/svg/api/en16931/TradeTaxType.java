
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TradeTaxType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TradeTaxType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CalculatedAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/&gt;
 *         &lt;element name="TypeCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}TaxTypeCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ExemptionReason" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/&gt;
 *         &lt;element name="BasisAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/&gt;
 *         &lt;element name="LineTotalBasisAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/&gt;
 *         &lt;element name="AllowanceChargeBasisAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/&gt;
 *         &lt;element name="CategoryCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}TaxCategoryCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ExemptionReasonCode" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}CodeType" minOccurs="0"/&gt;
 *         &lt;element name="TaxPointDate" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}DateType" minOccurs="0"/&gt;
 *         &lt;element name="DueDateTypeCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}TimeReferenceCodeType" minOccurs="0"/&gt;
 *         &lt;element name="RateApplicablePercent" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}PercentType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeTaxType", propOrder = {
    "calculatedAmount",
    "typeCode",
    "exemptionReason",
    "basisAmount",
    "lineTotalBasisAmount",
    "allowanceChargeBasisAmount",
    "categoryCode",
    "exemptionReasonCode",
    "taxPointDate",
    "dueDateTypeCode",
    "rateApplicablePercent"
})
public class TradeTaxType {

    @XmlElement(name = "CalculatedAmount")
    protected AmountType calculatedAmount;
    @XmlElement(name = "TypeCode")
    protected TaxTypeCodeType typeCode;
    @XmlElement(name = "ExemptionReason")
    protected TextType exemptionReason;
    @XmlElement(name = "BasisAmount")
    protected AmountType basisAmount;
    @XmlElement(name = "LineTotalBasisAmount")
    protected AmountType lineTotalBasisAmount;
    @XmlElement(name = "AllowanceChargeBasisAmount")
    protected AmountType allowanceChargeBasisAmount;
    @XmlElement(name = "CategoryCode")
    protected TaxCategoryCodeType categoryCode;
    @XmlElement(name = "ExemptionReasonCode")
    protected CodeType exemptionReasonCode;
    @XmlElement(name = "TaxPointDate")
    protected DateType taxPointDate;
    @XmlElement(name = "DueDateTypeCode")
    protected TimeReferenceCodeType dueDateTypeCode;
    @XmlElement(name = "RateApplicablePercent")
    protected PercentType rateApplicablePercent;

    /**
     * Ruft den Wert der calculatedAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getCalculatedAmount() {
        return calculatedAmount;
    }

    /**
     * Legt den Wert der calculatedAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setCalculatedAmount(AmountType value) {
        this.calculatedAmount = value;
    }

    /**
     * Ruft den Wert der typeCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TaxTypeCodeType }
     *     
     */
    public TaxTypeCodeType getTypeCode() {
        return typeCode;
    }

    /**
     * Legt den Wert der typeCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxTypeCodeType }
     *     
     */
    public void setTypeCode(TaxTypeCodeType value) {
        this.typeCode = value;
    }

    /**
     * Ruft den Wert der exemptionReason-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getExemptionReason() {
        return exemptionReason;
    }

    /**
     * Legt den Wert der exemptionReason-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setExemptionReason(TextType value) {
        this.exemptionReason = value;
    }

    /**
     * Ruft den Wert der basisAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getBasisAmount() {
        return basisAmount;
    }

    /**
     * Legt den Wert der basisAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setBasisAmount(AmountType value) {
        this.basisAmount = value;
    }

    /**
     * Ruft den Wert der lineTotalBasisAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getLineTotalBasisAmount() {
        return lineTotalBasisAmount;
    }

    /**
     * Legt den Wert der lineTotalBasisAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setLineTotalBasisAmount(AmountType value) {
        this.lineTotalBasisAmount = value;
    }

    /**
     * Ruft den Wert der allowanceChargeBasisAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getAllowanceChargeBasisAmount() {
        return allowanceChargeBasisAmount;
    }

    /**
     * Legt den Wert der allowanceChargeBasisAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setAllowanceChargeBasisAmount(AmountType value) {
        this.allowanceChargeBasisAmount = value;
    }

    /**
     * Ruft den Wert der categoryCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TaxCategoryCodeType }
     *     
     */
    public TaxCategoryCodeType getCategoryCode() {
        return categoryCode;
    }

    /**
     * Legt den Wert der categoryCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxCategoryCodeType }
     *     
     */
    public void setCategoryCode(TaxCategoryCodeType value) {
        this.categoryCode = value;
    }

    /**
     * Ruft den Wert der exemptionReasonCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodeType }
     *     
     */
    public CodeType getExemptionReasonCode() {
        return exemptionReasonCode;
    }

    /**
     * Legt den Wert der exemptionReasonCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeType }
     *     
     */
    public void setExemptionReasonCode(CodeType value) {
        this.exemptionReasonCode = value;
    }

    /**
     * Ruft den Wert der taxPointDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getTaxPointDate() {
        return taxPointDate;
    }

    /**
     * Legt den Wert der taxPointDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setTaxPointDate(DateType value) {
        this.taxPointDate = value;
    }

    /**
     * Ruft den Wert der dueDateTypeCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeReferenceCodeType }
     *     
     */
    public TimeReferenceCodeType getDueDateTypeCode() {
        return dueDateTypeCode;
    }

    /**
     * Legt den Wert der dueDateTypeCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeReferenceCodeType }
     *     
     */
    public void setDueDateTypeCode(TimeReferenceCodeType value) {
        this.dueDateTypeCode = value;
    }

    /**
     * Ruft den Wert der rateApplicablePercent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PercentType }
     *     
     */
    public PercentType getRateApplicablePercent() {
        return rateApplicablePercent;
    }

    /**
     * Legt den Wert der rateApplicablePercent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PercentType }
     *     
     */
    public void setRateApplicablePercent(PercentType value) {
        this.rateApplicablePercent = value;
    }

}
