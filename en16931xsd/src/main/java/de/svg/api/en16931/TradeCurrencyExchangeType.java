
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TradeCurrencyExchangeType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TradeCurrencyExchangeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SourceCurrencyCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}CurrencyCodeType"/&gt;
 *         &lt;element name="TargetCurrencyCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}CurrencyCodeType"/&gt;
 *         &lt;element name="ConversionRate" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}RateType"/&gt;
 *         &lt;element name="ConversionRateDateTime" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}DateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeCurrencyExchangeType", propOrder = {
    "sourceCurrencyCode",
    "targetCurrencyCode",
    "conversionRate",
    "conversionRateDateTime"
})
public class TradeCurrencyExchangeType {

    @XmlElement(name = "SourceCurrencyCode", required = true)
    protected CurrencyCodeType sourceCurrencyCode;
    @XmlElement(name = "TargetCurrencyCode", required = true)
    protected CurrencyCodeType targetCurrencyCode;
    @XmlElement(name = "ConversionRate", required = true)
    protected RateType conversionRate;
    @XmlElement(name = "ConversionRateDateTime")
    protected DateTimeType conversionRateDateTime;

    /**
     * Ruft den Wert der sourceCurrencyCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getSourceCurrencyCode() {
        return sourceCurrencyCode;
    }

    /**
     * Legt den Wert der sourceCurrencyCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setSourceCurrencyCode(CurrencyCodeType value) {
        this.sourceCurrencyCode = value;
    }

    /**
     * Ruft den Wert der targetCurrencyCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getTargetCurrencyCode() {
        return targetCurrencyCode;
    }

    /**
     * Legt den Wert der targetCurrencyCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setTargetCurrencyCode(CurrencyCodeType value) {
        this.targetCurrencyCode = value;
    }

    /**
     * Ruft den Wert der conversionRate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RateType }
     *     
     */
    public RateType getConversionRate() {
        return conversionRate;
    }

    /**
     * Legt den Wert der conversionRate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RateType }
     *     
     */
    public void setConversionRate(RateType value) {
        this.conversionRate = value;
    }

    /**
     * Ruft den Wert der conversionRateDateTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeType }
     *     
     */
    public DateTimeType getConversionRateDateTime() {
        return conversionRateDateTime;
    }

    /**
     * Legt den Wert der conversionRateDateTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeType }
     *     
     */
    public void setConversionRateDateTime(DateTimeType value) {
        this.conversionRateDateTime = value;
    }

}
