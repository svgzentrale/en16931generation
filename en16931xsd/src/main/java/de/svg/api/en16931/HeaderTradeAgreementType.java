
package de.svg.api.en16931;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für HeaderTradeAgreementType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HeaderTradeAgreementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BuyerReference" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/&gt;
 *         &lt;element name="SellerTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType"/&gt;
 *         &lt;element name="BuyerTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType"/&gt;
 *         &lt;element name="SellerTaxRepresentativeTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/&gt;
 *         &lt;element name="ProductEndUserTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/&gt;
 *         &lt;element name="ApplicableTradeDeliveryTerms" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeDeliveryTermsType" minOccurs="0"/&gt;
 *         &lt;element name="SellerOrderReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/&gt;
 *         &lt;element name="BuyerOrderReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/&gt;
 *         &lt;element name="ContractReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/&gt;
 *         &lt;element name="AdditionalReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedProcuringProject" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ProcuringProjectType" minOccurs="0"/&gt;
 *         &lt;element name="UltimateCustomerOrderReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderTradeAgreementType", propOrder = {
    "buyerReference",
    "sellerTradeParty",
    "buyerTradeParty",
    "sellerTaxRepresentativeTradeParty",
    "productEndUserTradeParty",
    "applicableTradeDeliveryTerms",
    "sellerOrderReferencedDocument",
    "buyerOrderReferencedDocument",
    "contractReferencedDocument",
    "additionalReferencedDocument",
    "specifiedProcuringProject",
    "ultimateCustomerOrderReferencedDocument"
})
public class HeaderTradeAgreementType {

    @XmlElement(name = "BuyerReference")
    protected TextType buyerReference;
    @XmlElement(name = "SellerTradeParty", required = true)
    protected TradePartyType sellerTradeParty;
    @XmlElement(name = "BuyerTradeParty", required = true)
    protected TradePartyType buyerTradeParty;
    @XmlElement(name = "SellerTaxRepresentativeTradeParty")
    protected TradePartyType sellerTaxRepresentativeTradeParty;
    @XmlElement(name = "ProductEndUserTradeParty")
    protected TradePartyType productEndUserTradeParty;
    @XmlElement(name = "ApplicableTradeDeliveryTerms")
    protected TradeDeliveryTermsType applicableTradeDeliveryTerms;
    @XmlElement(name = "SellerOrderReferencedDocument")
    protected ReferencedDocumentType sellerOrderReferencedDocument;
    @XmlElement(name = "BuyerOrderReferencedDocument")
    protected ReferencedDocumentType buyerOrderReferencedDocument;
    @XmlElement(name = "ContractReferencedDocument")
    protected ReferencedDocumentType contractReferencedDocument;
    @XmlElement(name = "AdditionalReferencedDocument")
    protected List<ReferencedDocumentType> additionalReferencedDocument;
    @XmlElement(name = "SpecifiedProcuringProject")
    protected ProcuringProjectType specifiedProcuringProject;
    @XmlElement(name = "UltimateCustomerOrderReferencedDocument")
    protected List<ReferencedDocumentType> ultimateCustomerOrderReferencedDocument;

    /**
     * Ruft den Wert der buyerReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getBuyerReference() {
        return buyerReference;
    }

    /**
     * Legt den Wert der buyerReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setBuyerReference(TextType value) {
        this.buyerReference = value;
    }

    /**
     * Ruft den Wert der sellerTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getSellerTradeParty() {
        return sellerTradeParty;
    }

    /**
     * Legt den Wert der sellerTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setSellerTradeParty(TradePartyType value) {
        this.sellerTradeParty = value;
    }

    /**
     * Ruft den Wert der buyerTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getBuyerTradeParty() {
        return buyerTradeParty;
    }

    /**
     * Legt den Wert der buyerTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setBuyerTradeParty(TradePartyType value) {
        this.buyerTradeParty = value;
    }

    /**
     * Ruft den Wert der sellerTaxRepresentativeTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getSellerTaxRepresentativeTradeParty() {
        return sellerTaxRepresentativeTradeParty;
    }

    /**
     * Legt den Wert der sellerTaxRepresentativeTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setSellerTaxRepresentativeTradeParty(TradePartyType value) {
        this.sellerTaxRepresentativeTradeParty = value;
    }

    /**
     * Ruft den Wert der productEndUserTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getProductEndUserTradeParty() {
        return productEndUserTradeParty;
    }

    /**
     * Legt den Wert der productEndUserTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setProductEndUserTradeParty(TradePartyType value) {
        this.productEndUserTradeParty = value;
    }

    /**
     * Ruft den Wert der applicableTradeDeliveryTerms-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradeDeliveryTermsType }
     *     
     */
    public TradeDeliveryTermsType getApplicableTradeDeliveryTerms() {
        return applicableTradeDeliveryTerms;
    }

    /**
     * Legt den Wert der applicableTradeDeliveryTerms-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeDeliveryTermsType }
     *     
     */
    public void setApplicableTradeDeliveryTerms(TradeDeliveryTermsType value) {
        this.applicableTradeDeliveryTerms = value;
    }

    /**
     * Ruft den Wert der sellerOrderReferencedDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getSellerOrderReferencedDocument() {
        return sellerOrderReferencedDocument;
    }

    /**
     * Legt den Wert der sellerOrderReferencedDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setSellerOrderReferencedDocument(ReferencedDocumentType value) {
        this.sellerOrderReferencedDocument = value;
    }

    /**
     * Ruft den Wert der buyerOrderReferencedDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getBuyerOrderReferencedDocument() {
        return buyerOrderReferencedDocument;
    }

    /**
     * Legt den Wert der buyerOrderReferencedDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setBuyerOrderReferencedDocument(ReferencedDocumentType value) {
        this.buyerOrderReferencedDocument = value;
    }

    /**
     * Ruft den Wert der contractReferencedDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getContractReferencedDocument() {
        return contractReferencedDocument;
    }

    /**
     * Legt den Wert der contractReferencedDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setContractReferencedDocument(ReferencedDocumentType value) {
        this.contractReferencedDocument = value;
    }

    /**
     * Gets the value of the additionalReferencedDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalReferencedDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalReferencedDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedDocumentType }
     * 
     * 
     */
    public List<ReferencedDocumentType> getAdditionalReferencedDocument() {
        if (additionalReferencedDocument == null) {
            additionalReferencedDocument = new ArrayList<ReferencedDocumentType>();
        }
        return this.additionalReferencedDocument;
    }

    /**
     * Ruft den Wert der specifiedProcuringProject-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProcuringProjectType }
     *     
     */
    public ProcuringProjectType getSpecifiedProcuringProject() {
        return specifiedProcuringProject;
    }

    /**
     * Legt den Wert der specifiedProcuringProject-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcuringProjectType }
     *     
     */
    public void setSpecifiedProcuringProject(ProcuringProjectType value) {
        this.specifiedProcuringProject = value;
    }

    /**
     * Gets the value of the ultimateCustomerOrderReferencedDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ultimateCustomerOrderReferencedDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUltimateCustomerOrderReferencedDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferencedDocumentType }
     * 
     * 
     */
    public List<ReferencedDocumentType> getUltimateCustomerOrderReferencedDocument() {
        if (ultimateCustomerOrderReferencedDocument == null) {
            ultimateCustomerOrderReferencedDocument = new ArrayList<ReferencedDocumentType>();
        }
        return this.ultimateCustomerOrderReferencedDocument;
    }

}
