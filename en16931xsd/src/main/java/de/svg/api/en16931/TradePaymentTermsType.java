
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TradePaymentTermsType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TradePaymentTermsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Description" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/&gt;
 *         &lt;element name="DueDateDateTime" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}DateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DirectDebitMandateID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/&gt;
 *         &lt;element name="PartialPaymentAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/&gt;
 *         &lt;element name="ApplicableTradePaymentPenaltyTerms" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePaymentPenaltyTermsType" minOccurs="0"/&gt;
 *         &lt;element name="ApplicableTradePaymentDiscountTerms" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePaymentDiscountTermsType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradePaymentTermsType", propOrder = {
    "description",
    "dueDateDateTime",
    "directDebitMandateID",
    "partialPaymentAmount",
    "applicableTradePaymentPenaltyTerms",
    "applicableTradePaymentDiscountTerms"
})
public class TradePaymentTermsType {

    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "DueDateDateTime")
    protected DateTimeType dueDateDateTime;
    @XmlElement(name = "DirectDebitMandateID")
    protected IDType directDebitMandateID;
    @XmlElement(name = "PartialPaymentAmount")
    protected AmountType partialPaymentAmount;
    @XmlElement(name = "ApplicableTradePaymentPenaltyTerms")
    protected TradePaymentPenaltyTermsType applicableTradePaymentPenaltyTerms;
    @XmlElement(name = "ApplicableTradePaymentDiscountTerms")
    protected TradePaymentDiscountTermsType applicableTradePaymentDiscountTerms;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der dueDateDateTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeType }
     *     
     */
    public DateTimeType getDueDateDateTime() {
        return dueDateDateTime;
    }

    /**
     * Legt den Wert der dueDateDateTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeType }
     *     
     */
    public void setDueDateDateTime(DateTimeType value) {
        this.dueDateDateTime = value;
    }

    /**
     * Ruft den Wert der directDebitMandateID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getDirectDebitMandateID() {
        return directDebitMandateID;
    }

    /**
     * Legt den Wert der directDebitMandateID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setDirectDebitMandateID(IDType value) {
        this.directDebitMandateID = value;
    }

    /**
     * Ruft den Wert der partialPaymentAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getPartialPaymentAmount() {
        return partialPaymentAmount;
    }

    /**
     * Legt den Wert der partialPaymentAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setPartialPaymentAmount(AmountType value) {
        this.partialPaymentAmount = value;
    }

    /**
     * Ruft den Wert der applicableTradePaymentPenaltyTerms-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePaymentPenaltyTermsType }
     *     
     */
    public TradePaymentPenaltyTermsType getApplicableTradePaymentPenaltyTerms() {
        return applicableTradePaymentPenaltyTerms;
    }

    /**
     * Legt den Wert der applicableTradePaymentPenaltyTerms-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePaymentPenaltyTermsType }
     *     
     */
    public void setApplicableTradePaymentPenaltyTerms(TradePaymentPenaltyTermsType value) {
        this.applicableTradePaymentPenaltyTerms = value;
    }

    /**
     * Ruft den Wert der applicableTradePaymentDiscountTerms-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePaymentDiscountTermsType }
     *     
     */
    public TradePaymentDiscountTermsType getApplicableTradePaymentDiscountTerms() {
        return applicableTradePaymentDiscountTerms;
    }

    /**
     * Legt den Wert der applicableTradePaymentDiscountTerms-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePaymentDiscountTermsType }
     *     
     */
    public void setApplicableTradePaymentDiscountTerms(TradePaymentDiscountTermsType value) {
        this.applicableTradePaymentDiscountTerms = value;
    }

}
