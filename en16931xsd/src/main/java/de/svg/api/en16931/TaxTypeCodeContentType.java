
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TaxTypeCodeContentType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="TaxTypeCodeContentType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *     &lt;enumeration value="AAA"/&gt;
 *     &lt;enumeration value="AAB"/&gt;
 *     &lt;enumeration value="AAC"/&gt;
 *     &lt;enumeration value="AAD"/&gt;
 *     &lt;enumeration value="AAE"/&gt;
 *     &lt;enumeration value="AAF"/&gt;
 *     &lt;enumeration value="AAG"/&gt;
 *     &lt;enumeration value="AAH"/&gt;
 *     &lt;enumeration value="AAI"/&gt;
 *     &lt;enumeration value="AAJ"/&gt;
 *     &lt;enumeration value="AAK"/&gt;
 *     &lt;enumeration value="AAL"/&gt;
 *     &lt;enumeration value="AAM"/&gt;
 *     &lt;enumeration value="ADD"/&gt;
 *     &lt;enumeration value="BOL"/&gt;
 *     &lt;enumeration value="CAP"/&gt;
 *     &lt;enumeration value="CAR"/&gt;
 *     &lt;enumeration value="COC"/&gt;
 *     &lt;enumeration value="CST"/&gt;
 *     &lt;enumeration value="CUD"/&gt;
 *     &lt;enumeration value="CVD"/&gt;
 *     &lt;enumeration value="ENV"/&gt;
 *     &lt;enumeration value="EXC"/&gt;
 *     &lt;enumeration value="EXP"/&gt;
 *     &lt;enumeration value="FET"/&gt;
 *     &lt;enumeration value="FRE"/&gt;
 *     &lt;enumeration value="GCN"/&gt;
 *     &lt;enumeration value="GST"/&gt;
 *     &lt;enumeration value="ILL"/&gt;
 *     &lt;enumeration value="IMP"/&gt;
 *     &lt;enumeration value="IND"/&gt;
 *     &lt;enumeration value="LAC"/&gt;
 *     &lt;enumeration value="LCN"/&gt;
 *     &lt;enumeration value="LDP"/&gt;
 *     &lt;enumeration value="LOC"/&gt;
 *     &lt;enumeration value="LST"/&gt;
 *     &lt;enumeration value="MCA"/&gt;
 *     &lt;enumeration value="MCD"/&gt;
 *     &lt;enumeration value="OTH"/&gt;
 *     &lt;enumeration value="PDB"/&gt;
 *     &lt;enumeration value="PDC"/&gt;
 *     &lt;enumeration value="PRF"/&gt;
 *     &lt;enumeration value="SCN"/&gt;
 *     &lt;enumeration value="SSS"/&gt;
 *     &lt;enumeration value="STT"/&gt;
 *     &lt;enumeration value="SUP"/&gt;
 *     &lt;enumeration value="SUR"/&gt;
 *     &lt;enumeration value="SWT"/&gt;
 *     &lt;enumeration value="TAC"/&gt;
 *     &lt;enumeration value="TOT"/&gt;
 *     &lt;enumeration value="TOX"/&gt;
 *     &lt;enumeration value="TTA"/&gt;
 *     &lt;enumeration value="VAD"/&gt;
 *     &lt;enumeration value="VAT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TaxTypeCodeContentType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100")
@XmlEnum
public enum TaxTypeCodeContentType {

    AAA,
    AAB,
    AAC,
    AAD,
    AAE,
    AAF,
    AAG,
    AAH,
    AAI,
    AAJ,
    AAK,
    AAL,
    AAM,
    ADD,
    BOL,
    CAP,
    CAR,
    COC,
    CST,
    CUD,
    CVD,
    ENV,
    EXC,
    EXP,
    FET,
    FRE,
    GCN,
    GST,
    ILL,
    IMP,
    IND,
    LAC,
    LCN,
    LDP,
    LOC,
    LST,
    MCA,
    MCD,
    OTH,
    PDB,
    PDC,
    PRF,
    SCN,
    SSS,
    STT,
    SUP,
    SUR,
    SWT,
    TAC,
    TOT,
    TOX,
    TTA,
    VAD,
    VAT;

    public String value() {
        return name();
    }

    public static TaxTypeCodeContentType fromValue(String v) {
        return valueOf(v);
    }

}
