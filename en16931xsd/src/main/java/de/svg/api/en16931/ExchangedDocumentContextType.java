
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ExchangedDocumentContextType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ExchangedDocumentContextType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TestIndicator" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IndicatorType" minOccurs="0"/&gt;
 *         &lt;element name="BusinessProcessSpecifiedDocumentContextParameter" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}DocumentContextParameterType" minOccurs="0"/&gt;
 *         &lt;element name="GuidelineSpecifiedDocumentContextParameter" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}DocumentContextParameterType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangedDocumentContextType", propOrder = {
    "testIndicator",
    "businessProcessSpecifiedDocumentContextParameter",
    "guidelineSpecifiedDocumentContextParameter"
})
public class ExchangedDocumentContextType {

    @XmlElement(name = "TestIndicator")
    protected IndicatorType testIndicator;
    @XmlElement(name = "BusinessProcessSpecifiedDocumentContextParameter")
    protected DocumentContextParameterType businessProcessSpecifiedDocumentContextParameter;
    @XmlElement(name = "GuidelineSpecifiedDocumentContextParameter", required = true)
    protected DocumentContextParameterType guidelineSpecifiedDocumentContextParameter;

    /**
     * Ruft den Wert der testIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IndicatorType }
     *     
     */
    public IndicatorType getTestIndicator() {
        return testIndicator;
    }

    /**
     * Legt den Wert der testIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IndicatorType }
     *     
     */
    public void setTestIndicator(IndicatorType value) {
        this.testIndicator = value;
    }

    /**
     * Ruft den Wert der businessProcessSpecifiedDocumentContextParameter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DocumentContextParameterType }
     *     
     */
    public DocumentContextParameterType getBusinessProcessSpecifiedDocumentContextParameter() {
        return businessProcessSpecifiedDocumentContextParameter;
    }

    /**
     * Legt den Wert der businessProcessSpecifiedDocumentContextParameter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentContextParameterType }
     *     
     */
    public void setBusinessProcessSpecifiedDocumentContextParameter(DocumentContextParameterType value) {
        this.businessProcessSpecifiedDocumentContextParameter = value;
    }

    /**
     * Ruft den Wert der guidelineSpecifiedDocumentContextParameter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DocumentContextParameterType }
     *     
     */
    public DocumentContextParameterType getGuidelineSpecifiedDocumentContextParameter() {
        return guidelineSpecifiedDocumentContextParameter;
    }

    /**
     * Legt den Wert der guidelineSpecifiedDocumentContextParameter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentContextParameterType }
     *     
     */
    public void setGuidelineSpecifiedDocumentContextParameter(DocumentContextParameterType value) {
        this.guidelineSpecifiedDocumentContextParameter = value;
    }

}
