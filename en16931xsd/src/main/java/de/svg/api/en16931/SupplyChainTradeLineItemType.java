
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SupplyChainTradeLineItemType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SupplyChainTradeLineItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AssociatedDocumentLineDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}DocumentLineDocumentType"/&gt;
 *         &lt;element name="SpecifiedTradeProduct" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeProductType"/&gt;
 *         &lt;element name="SpecifiedLineTradeAgreement" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LineTradeAgreementType"/&gt;
 *         &lt;element name="SpecifiedLineTradeDelivery" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LineTradeDeliveryType" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedLineTradeSettlement" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LineTradeSettlementType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SupplyChainTradeLineItemType", propOrder = {
    "associatedDocumentLineDocument",
    "specifiedTradeProduct",
    "specifiedLineTradeAgreement",
    "specifiedLineTradeDelivery",
    "specifiedLineTradeSettlement"
})
public class SupplyChainTradeLineItemType {

    @XmlElement(name = "AssociatedDocumentLineDocument", required = true)
    protected DocumentLineDocumentType associatedDocumentLineDocument;
    @XmlElement(name = "SpecifiedTradeProduct", required = true)
    protected TradeProductType specifiedTradeProduct;
    @XmlElement(name = "SpecifiedLineTradeAgreement", required = true)
    protected LineTradeAgreementType specifiedLineTradeAgreement;
    @XmlElement(name = "SpecifiedLineTradeDelivery")
    protected LineTradeDeliveryType specifiedLineTradeDelivery;
    @XmlElement(name = "SpecifiedLineTradeSettlement", required = true)
    protected LineTradeSettlementType specifiedLineTradeSettlement;

    /**
     * Ruft den Wert der associatedDocumentLineDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DocumentLineDocumentType }
     *     
     */
    public DocumentLineDocumentType getAssociatedDocumentLineDocument() {
        return associatedDocumentLineDocument;
    }

    /**
     * Legt den Wert der associatedDocumentLineDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentLineDocumentType }
     *     
     */
    public void setAssociatedDocumentLineDocument(DocumentLineDocumentType value) {
        this.associatedDocumentLineDocument = value;
    }

    /**
     * Ruft den Wert der specifiedTradeProduct-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradeProductType }
     *     
     */
    public TradeProductType getSpecifiedTradeProduct() {
        return specifiedTradeProduct;
    }

    /**
     * Legt den Wert der specifiedTradeProduct-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeProductType }
     *     
     */
    public void setSpecifiedTradeProduct(TradeProductType value) {
        this.specifiedTradeProduct = value;
    }

    /**
     * Ruft den Wert der specifiedLineTradeAgreement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LineTradeAgreementType }
     *     
     */
    public LineTradeAgreementType getSpecifiedLineTradeAgreement() {
        return specifiedLineTradeAgreement;
    }

    /**
     * Legt den Wert der specifiedLineTradeAgreement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LineTradeAgreementType }
     *     
     */
    public void setSpecifiedLineTradeAgreement(LineTradeAgreementType value) {
        this.specifiedLineTradeAgreement = value;
    }

    /**
     * Ruft den Wert der specifiedLineTradeDelivery-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LineTradeDeliveryType }
     *     
     */
    public LineTradeDeliveryType getSpecifiedLineTradeDelivery() {
        return specifiedLineTradeDelivery;
    }

    /**
     * Legt den Wert der specifiedLineTradeDelivery-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LineTradeDeliveryType }
     *     
     */
    public void setSpecifiedLineTradeDelivery(LineTradeDeliveryType value) {
        this.specifiedLineTradeDelivery = value;
    }

    /**
     * Ruft den Wert der specifiedLineTradeSettlement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LineTradeSettlementType }
     *     
     */
    public LineTradeSettlementType getSpecifiedLineTradeSettlement() {
        return specifiedLineTradeSettlement;
    }

    /**
     * Legt den Wert der specifiedLineTradeSettlement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LineTradeSettlementType }
     *     
     */
    public void setSpecifiedLineTradeSettlement(LineTradeSettlementType value) {
        this.specifiedLineTradeSettlement = value;
    }

}
