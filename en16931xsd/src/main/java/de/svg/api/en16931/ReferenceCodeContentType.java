
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ReferenceCodeContentType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ReferenceCodeContentType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *     &lt;enumeration value="AAA"/&gt;
 *     &lt;enumeration value="AAB"/&gt;
 *     &lt;enumeration value="AAC"/&gt;
 *     &lt;enumeration value="AAD"/&gt;
 *     &lt;enumeration value="AAE"/&gt;
 *     &lt;enumeration value="AAF"/&gt;
 *     &lt;enumeration value="AAG"/&gt;
 *     &lt;enumeration value="AAH"/&gt;
 *     &lt;enumeration value="AAI"/&gt;
 *     &lt;enumeration value="AAJ"/&gt;
 *     &lt;enumeration value="AAK"/&gt;
 *     &lt;enumeration value="AAL"/&gt;
 *     &lt;enumeration value="AAM"/&gt;
 *     &lt;enumeration value="AAN"/&gt;
 *     &lt;enumeration value="AAO"/&gt;
 *     &lt;enumeration value="AAP"/&gt;
 *     &lt;enumeration value="AAQ"/&gt;
 *     &lt;enumeration value="AAR"/&gt;
 *     &lt;enumeration value="AAS"/&gt;
 *     &lt;enumeration value="AAT"/&gt;
 *     &lt;enumeration value="AAU"/&gt;
 *     &lt;enumeration value="AAV"/&gt;
 *     &lt;enumeration value="AAW"/&gt;
 *     &lt;enumeration value="AAX"/&gt;
 *     &lt;enumeration value="AAY"/&gt;
 *     &lt;enumeration value="AAZ"/&gt;
 *     &lt;enumeration value="ABA"/&gt;
 *     &lt;enumeration value="ABB"/&gt;
 *     &lt;enumeration value="ABC"/&gt;
 *     &lt;enumeration value="ABD"/&gt;
 *     &lt;enumeration value="ABE"/&gt;
 *     &lt;enumeration value="ABF"/&gt;
 *     &lt;enumeration value="ABG"/&gt;
 *     &lt;enumeration value="ABH"/&gt;
 *     &lt;enumeration value="ABI"/&gt;
 *     &lt;enumeration value="ABJ"/&gt;
 *     &lt;enumeration value="ABK"/&gt;
 *     &lt;enumeration value="ABL"/&gt;
 *     &lt;enumeration value="ABM"/&gt;
 *     &lt;enumeration value="ABN"/&gt;
 *     &lt;enumeration value="ABO"/&gt;
 *     &lt;enumeration value="ABP"/&gt;
 *     &lt;enumeration value="ABQ"/&gt;
 *     &lt;enumeration value="ABR"/&gt;
 *     &lt;enumeration value="ABS"/&gt;
 *     &lt;enumeration value="ABT"/&gt;
 *     &lt;enumeration value="ABU"/&gt;
 *     &lt;enumeration value="ABV"/&gt;
 *     &lt;enumeration value="ABW"/&gt;
 *     &lt;enumeration value="ABX"/&gt;
 *     &lt;enumeration value="ABY"/&gt;
 *     &lt;enumeration value="ABZ"/&gt;
 *     &lt;enumeration value="AC"/&gt;
 *     &lt;enumeration value="ACA"/&gt;
 *     &lt;enumeration value="ACB"/&gt;
 *     &lt;enumeration value="ACC"/&gt;
 *     &lt;enumeration value="ACD"/&gt;
 *     &lt;enumeration value="ACE"/&gt;
 *     &lt;enumeration value="ACF"/&gt;
 *     &lt;enumeration value="ACG"/&gt;
 *     &lt;enumeration value="ACH"/&gt;
 *     &lt;enumeration value="ACI"/&gt;
 *     &lt;enumeration value="ACJ"/&gt;
 *     &lt;enumeration value="ACK"/&gt;
 *     &lt;enumeration value="ACL"/&gt;
 *     &lt;enumeration value="ACN"/&gt;
 *     &lt;enumeration value="ACO"/&gt;
 *     &lt;enumeration value="ACP"/&gt;
 *     &lt;enumeration value="ACQ"/&gt;
 *     &lt;enumeration value="ACR"/&gt;
 *     &lt;enumeration value="ACT"/&gt;
 *     &lt;enumeration value="ACU"/&gt;
 *     &lt;enumeration value="ACV"/&gt;
 *     &lt;enumeration value="ACW"/&gt;
 *     &lt;enumeration value="ACX"/&gt;
 *     &lt;enumeration value="ACY"/&gt;
 *     &lt;enumeration value="ACZ"/&gt;
 *     &lt;enumeration value="ADA"/&gt;
 *     &lt;enumeration value="ADB"/&gt;
 *     &lt;enumeration value="ADC"/&gt;
 *     &lt;enumeration value="ADD"/&gt;
 *     &lt;enumeration value="ADE"/&gt;
 *     &lt;enumeration value="ADF"/&gt;
 *     &lt;enumeration value="ADG"/&gt;
 *     &lt;enumeration value="ADI"/&gt;
 *     &lt;enumeration value="ADJ"/&gt;
 *     &lt;enumeration value="ADK"/&gt;
 *     &lt;enumeration value="ADL"/&gt;
 *     &lt;enumeration value="ADM"/&gt;
 *     &lt;enumeration value="ADN"/&gt;
 *     &lt;enumeration value="ADO"/&gt;
 *     &lt;enumeration value="ADP"/&gt;
 *     &lt;enumeration value="ADQ"/&gt;
 *     &lt;enumeration value="ADT"/&gt;
 *     &lt;enumeration value="ADU"/&gt;
 *     &lt;enumeration value="ADV"/&gt;
 *     &lt;enumeration value="ADW"/&gt;
 *     &lt;enumeration value="ADX"/&gt;
 *     &lt;enumeration value="ADY"/&gt;
 *     &lt;enumeration value="ADZ"/&gt;
 *     &lt;enumeration value="AE"/&gt;
 *     &lt;enumeration value="AEA"/&gt;
 *     &lt;enumeration value="AEB"/&gt;
 *     &lt;enumeration value="AEC"/&gt;
 *     &lt;enumeration value="AED"/&gt;
 *     &lt;enumeration value="AEE"/&gt;
 *     &lt;enumeration value="AEF"/&gt;
 *     &lt;enumeration value="AEG"/&gt;
 *     &lt;enumeration value="AEH"/&gt;
 *     &lt;enumeration value="AEI"/&gt;
 *     &lt;enumeration value="AEJ"/&gt;
 *     &lt;enumeration value="AEK"/&gt;
 *     &lt;enumeration value="AEL"/&gt;
 *     &lt;enumeration value="AEM"/&gt;
 *     &lt;enumeration value="AEN"/&gt;
 *     &lt;enumeration value="AEO"/&gt;
 *     &lt;enumeration value="AEP"/&gt;
 *     &lt;enumeration value="AEQ"/&gt;
 *     &lt;enumeration value="AER"/&gt;
 *     &lt;enumeration value="AES"/&gt;
 *     &lt;enumeration value="AET"/&gt;
 *     &lt;enumeration value="AEU"/&gt;
 *     &lt;enumeration value="AEV"/&gt;
 *     &lt;enumeration value="AEW"/&gt;
 *     &lt;enumeration value="AEX"/&gt;
 *     &lt;enumeration value="AEY"/&gt;
 *     &lt;enumeration value="AEZ"/&gt;
 *     &lt;enumeration value="AF"/&gt;
 *     &lt;enumeration value="AFA"/&gt;
 *     &lt;enumeration value="AFB"/&gt;
 *     &lt;enumeration value="AFC"/&gt;
 *     &lt;enumeration value="AFD"/&gt;
 *     &lt;enumeration value="AFE"/&gt;
 *     &lt;enumeration value="AFF"/&gt;
 *     &lt;enumeration value="AFG"/&gt;
 *     &lt;enumeration value="AFH"/&gt;
 *     &lt;enumeration value="AFI"/&gt;
 *     &lt;enumeration value="AFJ"/&gt;
 *     &lt;enumeration value="AFK"/&gt;
 *     &lt;enumeration value="AFL"/&gt;
 *     &lt;enumeration value="AFM"/&gt;
 *     &lt;enumeration value="AFN"/&gt;
 *     &lt;enumeration value="AFO"/&gt;
 *     &lt;enumeration value="AFP"/&gt;
 *     &lt;enumeration value="AFQ"/&gt;
 *     &lt;enumeration value="AFR"/&gt;
 *     &lt;enumeration value="AFS"/&gt;
 *     &lt;enumeration value="AFT"/&gt;
 *     &lt;enumeration value="AFU"/&gt;
 *     &lt;enumeration value="AFV"/&gt;
 *     &lt;enumeration value="AFW"/&gt;
 *     &lt;enumeration value="AFX"/&gt;
 *     &lt;enumeration value="AFY"/&gt;
 *     &lt;enumeration value="AFZ"/&gt;
 *     &lt;enumeration value="AGA"/&gt;
 *     &lt;enumeration value="AGB"/&gt;
 *     &lt;enumeration value="AGC"/&gt;
 *     &lt;enumeration value="AGD"/&gt;
 *     &lt;enumeration value="AGE"/&gt;
 *     &lt;enumeration value="AGF"/&gt;
 *     &lt;enumeration value="AGG"/&gt;
 *     &lt;enumeration value="AGH"/&gt;
 *     &lt;enumeration value="AGI"/&gt;
 *     &lt;enumeration value="AGJ"/&gt;
 *     &lt;enumeration value="AGK"/&gt;
 *     &lt;enumeration value="AGL"/&gt;
 *     &lt;enumeration value="AGM"/&gt;
 *     &lt;enumeration value="AGN"/&gt;
 *     &lt;enumeration value="AGO"/&gt;
 *     &lt;enumeration value="AGP"/&gt;
 *     &lt;enumeration value="AGQ"/&gt;
 *     &lt;enumeration value="AGR"/&gt;
 *     &lt;enumeration value="AGS"/&gt;
 *     &lt;enumeration value="AGT"/&gt;
 *     &lt;enumeration value="AGU"/&gt;
 *     &lt;enumeration value="AGV"/&gt;
 *     &lt;enumeration value="AGW"/&gt;
 *     &lt;enumeration value="AGX"/&gt;
 *     &lt;enumeration value="AGY"/&gt;
 *     &lt;enumeration value="AGZ"/&gt;
 *     &lt;enumeration value="AHA"/&gt;
 *     &lt;enumeration value="AHB"/&gt;
 *     &lt;enumeration value="AHC"/&gt;
 *     &lt;enumeration value="AHD"/&gt;
 *     &lt;enumeration value="AHE"/&gt;
 *     &lt;enumeration value="AHF"/&gt;
 *     &lt;enumeration value="AHG"/&gt;
 *     &lt;enumeration value="AHH"/&gt;
 *     &lt;enumeration value="AHI"/&gt;
 *     &lt;enumeration value="AHJ"/&gt;
 *     &lt;enumeration value="AHK"/&gt;
 *     &lt;enumeration value="AHL"/&gt;
 *     &lt;enumeration value="AHM"/&gt;
 *     &lt;enumeration value="AHN"/&gt;
 *     &lt;enumeration value="AHO"/&gt;
 *     &lt;enumeration value="AHP"/&gt;
 *     &lt;enumeration value="AHQ"/&gt;
 *     &lt;enumeration value="AHR"/&gt;
 *     &lt;enumeration value="AHS"/&gt;
 *     &lt;enumeration value="AHT"/&gt;
 *     &lt;enumeration value="AHU"/&gt;
 *     &lt;enumeration value="AHV"/&gt;
 *     &lt;enumeration value="AHX"/&gt;
 *     &lt;enumeration value="AHY"/&gt;
 *     &lt;enumeration value="AHZ"/&gt;
 *     &lt;enumeration value="AIA"/&gt;
 *     &lt;enumeration value="AIB"/&gt;
 *     &lt;enumeration value="AIC"/&gt;
 *     &lt;enumeration value="AID"/&gt;
 *     &lt;enumeration value="AIE"/&gt;
 *     &lt;enumeration value="AIF"/&gt;
 *     &lt;enumeration value="AIG"/&gt;
 *     &lt;enumeration value="AIH"/&gt;
 *     &lt;enumeration value="AII"/&gt;
 *     &lt;enumeration value="AIJ"/&gt;
 *     &lt;enumeration value="AIK"/&gt;
 *     &lt;enumeration value="AIL"/&gt;
 *     &lt;enumeration value="AIM"/&gt;
 *     &lt;enumeration value="AIN"/&gt;
 *     &lt;enumeration value="AIO"/&gt;
 *     &lt;enumeration value="AIP"/&gt;
 *     &lt;enumeration value="AIQ"/&gt;
 *     &lt;enumeration value="AIR"/&gt;
 *     &lt;enumeration value="AIS"/&gt;
 *     &lt;enumeration value="AIT"/&gt;
 *     &lt;enumeration value="AIU"/&gt;
 *     &lt;enumeration value="AIV"/&gt;
 *     &lt;enumeration value="AIW"/&gt;
 *     &lt;enumeration value="AIX"/&gt;
 *     &lt;enumeration value="AIY"/&gt;
 *     &lt;enumeration value="AIZ"/&gt;
 *     &lt;enumeration value="AJA"/&gt;
 *     &lt;enumeration value="AJB"/&gt;
 *     &lt;enumeration value="AJC"/&gt;
 *     &lt;enumeration value="AJD"/&gt;
 *     &lt;enumeration value="AJE"/&gt;
 *     &lt;enumeration value="AJF"/&gt;
 *     &lt;enumeration value="AJG"/&gt;
 *     &lt;enumeration value="AJH"/&gt;
 *     &lt;enumeration value="AJI"/&gt;
 *     &lt;enumeration value="AJJ"/&gt;
 *     &lt;enumeration value="AJK"/&gt;
 *     &lt;enumeration value="AJL"/&gt;
 *     &lt;enumeration value="AJM"/&gt;
 *     &lt;enumeration value="AJN"/&gt;
 *     &lt;enumeration value="AJO"/&gt;
 *     &lt;enumeration value="AJP"/&gt;
 *     &lt;enumeration value="AJQ"/&gt;
 *     &lt;enumeration value="AJR"/&gt;
 *     &lt;enumeration value="AJS"/&gt;
 *     &lt;enumeration value="AJT"/&gt;
 *     &lt;enumeration value="AJU"/&gt;
 *     &lt;enumeration value="AJV"/&gt;
 *     &lt;enumeration value="AJW"/&gt;
 *     &lt;enumeration value="AJX"/&gt;
 *     &lt;enumeration value="AJY"/&gt;
 *     &lt;enumeration value="AJZ"/&gt;
 *     &lt;enumeration value="AKA"/&gt;
 *     &lt;enumeration value="AKB"/&gt;
 *     &lt;enumeration value="AKC"/&gt;
 *     &lt;enumeration value="AKD"/&gt;
 *     &lt;enumeration value="AKE"/&gt;
 *     &lt;enumeration value="AKF"/&gt;
 *     &lt;enumeration value="AKG"/&gt;
 *     &lt;enumeration value="AKH"/&gt;
 *     &lt;enumeration value="AKI"/&gt;
 *     &lt;enumeration value="AKJ"/&gt;
 *     &lt;enumeration value="AKK"/&gt;
 *     &lt;enumeration value="AKL"/&gt;
 *     &lt;enumeration value="AKM"/&gt;
 *     &lt;enumeration value="AKN"/&gt;
 *     &lt;enumeration value="AKO"/&gt;
 *     &lt;enumeration value="AKP"/&gt;
 *     &lt;enumeration value="AKQ"/&gt;
 *     &lt;enumeration value="AKR"/&gt;
 *     &lt;enumeration value="AKS"/&gt;
 *     &lt;enumeration value="AKT"/&gt;
 *     &lt;enumeration value="AKU"/&gt;
 *     &lt;enumeration value="AKV"/&gt;
 *     &lt;enumeration value="AKW"/&gt;
 *     &lt;enumeration value="AKX"/&gt;
 *     &lt;enumeration value="AKY"/&gt;
 *     &lt;enumeration value="AKZ"/&gt;
 *     &lt;enumeration value="ALA"/&gt;
 *     &lt;enumeration value="ALB"/&gt;
 *     &lt;enumeration value="ALC"/&gt;
 *     &lt;enumeration value="ALD"/&gt;
 *     &lt;enumeration value="ALE"/&gt;
 *     &lt;enumeration value="ALF"/&gt;
 *     &lt;enumeration value="ALG"/&gt;
 *     &lt;enumeration value="ALH"/&gt;
 *     &lt;enumeration value="ALI"/&gt;
 *     &lt;enumeration value="ALJ"/&gt;
 *     &lt;enumeration value="ALK"/&gt;
 *     &lt;enumeration value="ALL"/&gt;
 *     &lt;enumeration value="ALM"/&gt;
 *     &lt;enumeration value="ALN"/&gt;
 *     &lt;enumeration value="ALO"/&gt;
 *     &lt;enumeration value="ALP"/&gt;
 *     &lt;enumeration value="ALQ"/&gt;
 *     &lt;enumeration value="ALR"/&gt;
 *     &lt;enumeration value="ALS"/&gt;
 *     &lt;enumeration value="ALT"/&gt;
 *     &lt;enumeration value="ALU"/&gt;
 *     &lt;enumeration value="ALV"/&gt;
 *     &lt;enumeration value="ALW"/&gt;
 *     &lt;enumeration value="ALX"/&gt;
 *     &lt;enumeration value="ALY"/&gt;
 *     &lt;enumeration value="ALZ"/&gt;
 *     &lt;enumeration value="AMA"/&gt;
 *     &lt;enumeration value="AMB"/&gt;
 *     &lt;enumeration value="AMC"/&gt;
 *     &lt;enumeration value="AMD"/&gt;
 *     &lt;enumeration value="AME"/&gt;
 *     &lt;enumeration value="AMF"/&gt;
 *     &lt;enumeration value="AMG"/&gt;
 *     &lt;enumeration value="AMH"/&gt;
 *     &lt;enumeration value="AMI"/&gt;
 *     &lt;enumeration value="AMJ"/&gt;
 *     &lt;enumeration value="AMK"/&gt;
 *     &lt;enumeration value="AML"/&gt;
 *     &lt;enumeration value="AMM"/&gt;
 *     &lt;enumeration value="AMN"/&gt;
 *     &lt;enumeration value="AMO"/&gt;
 *     &lt;enumeration value="AMP"/&gt;
 *     &lt;enumeration value="AMQ"/&gt;
 *     &lt;enumeration value="AMR"/&gt;
 *     &lt;enumeration value="AMS"/&gt;
 *     &lt;enumeration value="AMT"/&gt;
 *     &lt;enumeration value="AMU"/&gt;
 *     &lt;enumeration value="AMV"/&gt;
 *     &lt;enumeration value="AMW"/&gt;
 *     &lt;enumeration value="AMX"/&gt;
 *     &lt;enumeration value="AMY"/&gt;
 *     &lt;enumeration value="AMZ"/&gt;
 *     &lt;enumeration value="ANA"/&gt;
 *     &lt;enumeration value="ANB"/&gt;
 *     &lt;enumeration value="ANC"/&gt;
 *     &lt;enumeration value="AND"/&gt;
 *     &lt;enumeration value="ANE"/&gt;
 *     &lt;enumeration value="ANF"/&gt;
 *     &lt;enumeration value="ANG"/&gt;
 *     &lt;enumeration value="ANH"/&gt;
 *     &lt;enumeration value="ANI"/&gt;
 *     &lt;enumeration value="ANJ"/&gt;
 *     &lt;enumeration value="ANK"/&gt;
 *     &lt;enumeration value="ANL"/&gt;
 *     &lt;enumeration value="ANM"/&gt;
 *     &lt;enumeration value="ANN"/&gt;
 *     &lt;enumeration value="ANO"/&gt;
 *     &lt;enumeration value="ANP"/&gt;
 *     &lt;enumeration value="ANQ"/&gt;
 *     &lt;enumeration value="ANR"/&gt;
 *     &lt;enumeration value="ANS"/&gt;
 *     &lt;enumeration value="ANT"/&gt;
 *     &lt;enumeration value="ANU"/&gt;
 *     &lt;enumeration value="ANV"/&gt;
 *     &lt;enumeration value="ANW"/&gt;
 *     &lt;enumeration value="ANX"/&gt;
 *     &lt;enumeration value="ANY"/&gt;
 *     &lt;enumeration value="AOA"/&gt;
 *     &lt;enumeration value="AOD"/&gt;
 *     &lt;enumeration value="AOE"/&gt;
 *     &lt;enumeration value="AOF"/&gt;
 *     &lt;enumeration value="AOG"/&gt;
 *     &lt;enumeration value="AOH"/&gt;
 *     &lt;enumeration value="AOI"/&gt;
 *     &lt;enumeration value="AOJ"/&gt;
 *     &lt;enumeration value="AOK"/&gt;
 *     &lt;enumeration value="AOL"/&gt;
 *     &lt;enumeration value="AOM"/&gt;
 *     &lt;enumeration value="AON"/&gt;
 *     &lt;enumeration value="AOO"/&gt;
 *     &lt;enumeration value="AOP"/&gt;
 *     &lt;enumeration value="AOQ"/&gt;
 *     &lt;enumeration value="AOR"/&gt;
 *     &lt;enumeration value="AOS"/&gt;
 *     &lt;enumeration value="AOT"/&gt;
 *     &lt;enumeration value="AOU"/&gt;
 *     &lt;enumeration value="AOV"/&gt;
 *     &lt;enumeration value="AOW"/&gt;
 *     &lt;enumeration value="AOX"/&gt;
 *     &lt;enumeration value="AOY"/&gt;
 *     &lt;enumeration value="AOZ"/&gt;
 *     &lt;enumeration value="AP"/&gt;
 *     &lt;enumeration value="APA"/&gt;
 *     &lt;enumeration value="APB"/&gt;
 *     &lt;enumeration value="APC"/&gt;
 *     &lt;enumeration value="APD"/&gt;
 *     &lt;enumeration value="APE"/&gt;
 *     &lt;enumeration value="APF"/&gt;
 *     &lt;enumeration value="APG"/&gt;
 *     &lt;enumeration value="APH"/&gt;
 *     &lt;enumeration value="API"/&gt;
 *     &lt;enumeration value="APJ"/&gt;
 *     &lt;enumeration value="APK"/&gt;
 *     &lt;enumeration value="APL"/&gt;
 *     &lt;enumeration value="APM"/&gt;
 *     &lt;enumeration value="APN"/&gt;
 *     &lt;enumeration value="APO"/&gt;
 *     &lt;enumeration value="APP"/&gt;
 *     &lt;enumeration value="APQ"/&gt;
 *     &lt;enumeration value="APR"/&gt;
 *     &lt;enumeration value="APS"/&gt;
 *     &lt;enumeration value="APT"/&gt;
 *     &lt;enumeration value="APU"/&gt;
 *     &lt;enumeration value="APV"/&gt;
 *     &lt;enumeration value="APW"/&gt;
 *     &lt;enumeration value="APX"/&gt;
 *     &lt;enumeration value="APY"/&gt;
 *     &lt;enumeration value="APZ"/&gt;
 *     &lt;enumeration value="AQA"/&gt;
 *     &lt;enumeration value="AQB"/&gt;
 *     &lt;enumeration value="AQC"/&gt;
 *     &lt;enumeration value="AQD"/&gt;
 *     &lt;enumeration value="AQE"/&gt;
 *     &lt;enumeration value="AQF"/&gt;
 *     &lt;enumeration value="AQG"/&gt;
 *     &lt;enumeration value="AQH"/&gt;
 *     &lt;enumeration value="AQI"/&gt;
 *     &lt;enumeration value="AQJ"/&gt;
 *     &lt;enumeration value="AQK"/&gt;
 *     &lt;enumeration value="AQL"/&gt;
 *     &lt;enumeration value="AQM"/&gt;
 *     &lt;enumeration value="AQN"/&gt;
 *     &lt;enumeration value="AQO"/&gt;
 *     &lt;enumeration value="AQP"/&gt;
 *     &lt;enumeration value="AQQ"/&gt;
 *     &lt;enumeration value="AQR"/&gt;
 *     &lt;enumeration value="AQS"/&gt;
 *     &lt;enumeration value="AQT"/&gt;
 *     &lt;enumeration value="AQU"/&gt;
 *     &lt;enumeration value="AQV"/&gt;
 *     &lt;enumeration value="AQW"/&gt;
 *     &lt;enumeration value="AQX"/&gt;
 *     &lt;enumeration value="AQY"/&gt;
 *     &lt;enumeration value="AQZ"/&gt;
 *     &lt;enumeration value="ARA"/&gt;
 *     &lt;enumeration value="ARB"/&gt;
 *     &lt;enumeration value="ARC"/&gt;
 *     &lt;enumeration value="ARD"/&gt;
 *     &lt;enumeration value="ARE"/&gt;
 *     &lt;enumeration value="ARF"/&gt;
 *     &lt;enumeration value="ARG"/&gt;
 *     &lt;enumeration value="ARH"/&gt;
 *     &lt;enumeration value="ARI"/&gt;
 *     &lt;enumeration value="ARJ"/&gt;
 *     &lt;enumeration value="ARK"/&gt;
 *     &lt;enumeration value="ARL"/&gt;
 *     &lt;enumeration value="ARM"/&gt;
 *     &lt;enumeration value="ARN"/&gt;
 *     &lt;enumeration value="ARO"/&gt;
 *     &lt;enumeration value="ARP"/&gt;
 *     &lt;enumeration value="ARQ"/&gt;
 *     &lt;enumeration value="ARR"/&gt;
 *     &lt;enumeration value="ARS"/&gt;
 *     &lt;enumeration value="ART"/&gt;
 *     &lt;enumeration value="ARU"/&gt;
 *     &lt;enumeration value="ARV"/&gt;
 *     &lt;enumeration value="ARW"/&gt;
 *     &lt;enumeration value="ARX"/&gt;
 *     &lt;enumeration value="ARY"/&gt;
 *     &lt;enumeration value="ARZ"/&gt;
 *     &lt;enumeration value="ASA"/&gt;
 *     &lt;enumeration value="ASB"/&gt;
 *     &lt;enumeration value="ASC"/&gt;
 *     &lt;enumeration value="ASD"/&gt;
 *     &lt;enumeration value="ASE"/&gt;
 *     &lt;enumeration value="ASF"/&gt;
 *     &lt;enumeration value="ASG"/&gt;
 *     &lt;enumeration value="ASH"/&gt;
 *     &lt;enumeration value="ASI"/&gt;
 *     &lt;enumeration value="ASJ"/&gt;
 *     &lt;enumeration value="ASK"/&gt;
 *     &lt;enumeration value="ASL"/&gt;
 *     &lt;enumeration value="ASM"/&gt;
 *     &lt;enumeration value="ASN"/&gt;
 *     &lt;enumeration value="ASO"/&gt;
 *     &lt;enumeration value="ASP"/&gt;
 *     &lt;enumeration value="ASQ"/&gt;
 *     &lt;enumeration value="ASR"/&gt;
 *     &lt;enumeration value="ASS"/&gt;
 *     &lt;enumeration value="AST"/&gt;
 *     &lt;enumeration value="ASU"/&gt;
 *     &lt;enumeration value="ASV"/&gt;
 *     &lt;enumeration value="ASW"/&gt;
 *     &lt;enumeration value="ASX"/&gt;
 *     &lt;enumeration value="ASY"/&gt;
 *     &lt;enumeration value="ASZ"/&gt;
 *     &lt;enumeration value="ATA"/&gt;
 *     &lt;enumeration value="ATB"/&gt;
 *     &lt;enumeration value="ATC"/&gt;
 *     &lt;enumeration value="ATD"/&gt;
 *     &lt;enumeration value="ATE"/&gt;
 *     &lt;enumeration value="ATF"/&gt;
 *     &lt;enumeration value="ATG"/&gt;
 *     &lt;enumeration value="ATH"/&gt;
 *     &lt;enumeration value="ATI"/&gt;
 *     &lt;enumeration value="ATJ"/&gt;
 *     &lt;enumeration value="ATK"/&gt;
 *     &lt;enumeration value="ATL"/&gt;
 *     &lt;enumeration value="ATM"/&gt;
 *     &lt;enumeration value="ATN"/&gt;
 *     &lt;enumeration value="ATO"/&gt;
 *     &lt;enumeration value="ATP"/&gt;
 *     &lt;enumeration value="ATQ"/&gt;
 *     &lt;enumeration value="ATR"/&gt;
 *     &lt;enumeration value="ATS"/&gt;
 *     &lt;enumeration value="ATT"/&gt;
 *     &lt;enumeration value="ATU"/&gt;
 *     &lt;enumeration value="ATV"/&gt;
 *     &lt;enumeration value="ATW"/&gt;
 *     &lt;enumeration value="ATX"/&gt;
 *     &lt;enumeration value="ATY"/&gt;
 *     &lt;enumeration value="ATZ"/&gt;
 *     &lt;enumeration value="AU"/&gt;
 *     &lt;enumeration value="AUA"/&gt;
 *     &lt;enumeration value="AUB"/&gt;
 *     &lt;enumeration value="AUC"/&gt;
 *     &lt;enumeration value="AUD"/&gt;
 *     &lt;enumeration value="AUE"/&gt;
 *     &lt;enumeration value="AUF"/&gt;
 *     &lt;enumeration value="AUG"/&gt;
 *     &lt;enumeration value="AUH"/&gt;
 *     &lt;enumeration value="AUI"/&gt;
 *     &lt;enumeration value="AUJ"/&gt;
 *     &lt;enumeration value="AUK"/&gt;
 *     &lt;enumeration value="AUL"/&gt;
 *     &lt;enumeration value="AUM"/&gt;
 *     &lt;enumeration value="AUN"/&gt;
 *     &lt;enumeration value="AUO"/&gt;
 *     &lt;enumeration value="AUP"/&gt;
 *     &lt;enumeration value="AUQ"/&gt;
 *     &lt;enumeration value="AUR"/&gt;
 *     &lt;enumeration value="AUS"/&gt;
 *     &lt;enumeration value="AUT"/&gt;
 *     &lt;enumeration value="AUU"/&gt;
 *     &lt;enumeration value="AUV"/&gt;
 *     &lt;enumeration value="AUW"/&gt;
 *     &lt;enumeration value="AUX"/&gt;
 *     &lt;enumeration value="AUY"/&gt;
 *     &lt;enumeration value="AUZ"/&gt;
 *     &lt;enumeration value="AV"/&gt;
 *     &lt;enumeration value="AVA"/&gt;
 *     &lt;enumeration value="AVB"/&gt;
 *     &lt;enumeration value="AVC"/&gt;
 *     &lt;enumeration value="AVD"/&gt;
 *     &lt;enumeration value="AVE"/&gt;
 *     &lt;enumeration value="AVF"/&gt;
 *     &lt;enumeration value="AVG"/&gt;
 *     &lt;enumeration value="AVH"/&gt;
 *     &lt;enumeration value="AVI"/&gt;
 *     &lt;enumeration value="AVJ"/&gt;
 *     &lt;enumeration value="AVK"/&gt;
 *     &lt;enumeration value="AVL"/&gt;
 *     &lt;enumeration value="AVM"/&gt;
 *     &lt;enumeration value="AVN"/&gt;
 *     &lt;enumeration value="AVO"/&gt;
 *     &lt;enumeration value="AVP"/&gt;
 *     &lt;enumeration value="AVQ"/&gt;
 *     &lt;enumeration value="AVR"/&gt;
 *     &lt;enumeration value="AVS"/&gt;
 *     &lt;enumeration value="AVT"/&gt;
 *     &lt;enumeration value="AVU"/&gt;
 *     &lt;enumeration value="AVV"/&gt;
 *     &lt;enumeration value="AVW"/&gt;
 *     &lt;enumeration value="AVX"/&gt;
 *     &lt;enumeration value="AVY"/&gt;
 *     &lt;enumeration value="AVZ"/&gt;
 *     &lt;enumeration value="AWA"/&gt;
 *     &lt;enumeration value="AWB"/&gt;
 *     &lt;enumeration value="AWC"/&gt;
 *     &lt;enumeration value="AWD"/&gt;
 *     &lt;enumeration value="AWE"/&gt;
 *     &lt;enumeration value="AWF"/&gt;
 *     &lt;enumeration value="AWG"/&gt;
 *     &lt;enumeration value="AWH"/&gt;
 *     &lt;enumeration value="AWI"/&gt;
 *     &lt;enumeration value="AWJ"/&gt;
 *     &lt;enumeration value="AWK"/&gt;
 *     &lt;enumeration value="AWL"/&gt;
 *     &lt;enumeration value="AWM"/&gt;
 *     &lt;enumeration value="AWN"/&gt;
 *     &lt;enumeration value="AWO"/&gt;
 *     &lt;enumeration value="AWP"/&gt;
 *     &lt;enumeration value="AWQ"/&gt;
 *     &lt;enumeration value="AWR"/&gt;
 *     &lt;enumeration value="AWS"/&gt;
 *     &lt;enumeration value="AWT"/&gt;
 *     &lt;enumeration value="AWU"/&gt;
 *     &lt;enumeration value="AWV"/&gt;
 *     &lt;enumeration value="AWW"/&gt;
 *     &lt;enumeration value="AWX"/&gt;
 *     &lt;enumeration value="AWY"/&gt;
 *     &lt;enumeration value="AWZ"/&gt;
 *     &lt;enumeration value="AXA"/&gt;
 *     &lt;enumeration value="AXB"/&gt;
 *     &lt;enumeration value="AXC"/&gt;
 *     &lt;enumeration value="AXD"/&gt;
 *     &lt;enumeration value="AXE"/&gt;
 *     &lt;enumeration value="AXF"/&gt;
 *     &lt;enumeration value="AXG"/&gt;
 *     &lt;enumeration value="AXH"/&gt;
 *     &lt;enumeration value="AXI"/&gt;
 *     &lt;enumeration value="AXJ"/&gt;
 *     &lt;enumeration value="AXK"/&gt;
 *     &lt;enumeration value="AXL"/&gt;
 *     &lt;enumeration value="AXM"/&gt;
 *     &lt;enumeration value="AXN"/&gt;
 *     &lt;enumeration value="AXO"/&gt;
 *     &lt;enumeration value="AXP"/&gt;
 *     &lt;enumeration value="AXQ"/&gt;
 *     &lt;enumeration value="AXR"/&gt;
 *     &lt;enumeration value="AXS"/&gt;
 *     &lt;enumeration value="BA"/&gt;
 *     &lt;enumeration value="BC"/&gt;
 *     &lt;enumeration value="BD"/&gt;
 *     &lt;enumeration value="BE"/&gt;
 *     &lt;enumeration value="BH"/&gt;
 *     &lt;enumeration value="BM"/&gt;
 *     &lt;enumeration value="BN"/&gt;
 *     &lt;enumeration value="BO"/&gt;
 *     &lt;enumeration value="BR"/&gt;
 *     &lt;enumeration value="BT"/&gt;
 *     &lt;enumeration value="BTP"/&gt;
 *     &lt;enumeration value="BW"/&gt;
 *     &lt;enumeration value="CAS"/&gt;
 *     &lt;enumeration value="CAT"/&gt;
 *     &lt;enumeration value="CAU"/&gt;
 *     &lt;enumeration value="CAV"/&gt;
 *     &lt;enumeration value="CAW"/&gt;
 *     &lt;enumeration value="CAX"/&gt;
 *     &lt;enumeration value="CAY"/&gt;
 *     &lt;enumeration value="CAZ"/&gt;
 *     &lt;enumeration value="CBA"/&gt;
 *     &lt;enumeration value="CBB"/&gt;
 *     &lt;enumeration value="CD"/&gt;
 *     &lt;enumeration value="CEC"/&gt;
 *     &lt;enumeration value="CED"/&gt;
 *     &lt;enumeration value="CFE"/&gt;
 *     &lt;enumeration value="CFF"/&gt;
 *     &lt;enumeration value="CFO"/&gt;
 *     &lt;enumeration value="CG"/&gt;
 *     &lt;enumeration value="CH"/&gt;
 *     &lt;enumeration value="CK"/&gt;
 *     &lt;enumeration value="CKN"/&gt;
 *     &lt;enumeration value="CM"/&gt;
 *     &lt;enumeration value="CMR"/&gt;
 *     &lt;enumeration value="CN"/&gt;
 *     &lt;enumeration value="CNO"/&gt;
 *     &lt;enumeration value="COF"/&gt;
 *     &lt;enumeration value="CP"/&gt;
 *     &lt;enumeration value="CR"/&gt;
 *     &lt;enumeration value="CRN"/&gt;
 *     &lt;enumeration value="CS"/&gt;
 *     &lt;enumeration value="CST"/&gt;
 *     &lt;enumeration value="CT"/&gt;
 *     &lt;enumeration value="CU"/&gt;
 *     &lt;enumeration value="CV"/&gt;
 *     &lt;enumeration value="CW"/&gt;
 *     &lt;enumeration value="CZ"/&gt;
 *     &lt;enumeration value="DA"/&gt;
 *     &lt;enumeration value="DAN"/&gt;
 *     &lt;enumeration value="DB"/&gt;
 *     &lt;enumeration value="DI"/&gt;
 *     &lt;enumeration value="DL"/&gt;
 *     &lt;enumeration value="DM"/&gt;
 *     &lt;enumeration value="DQ"/&gt;
 *     &lt;enumeration value="DR"/&gt;
 *     &lt;enumeration value="EA"/&gt;
 *     &lt;enumeration value="EB"/&gt;
 *     &lt;enumeration value="ED"/&gt;
 *     &lt;enumeration value="EE"/&gt;
 *     &lt;enumeration value="EEP"/&gt;
 *     &lt;enumeration value="EI"/&gt;
 *     &lt;enumeration value="EN"/&gt;
 *     &lt;enumeration value="EQ"/&gt;
 *     &lt;enumeration value="ER"/&gt;
 *     &lt;enumeration value="ERN"/&gt;
 *     &lt;enumeration value="ET"/&gt;
 *     &lt;enumeration value="EX"/&gt;
 *     &lt;enumeration value="FC"/&gt;
 *     &lt;enumeration value="FF"/&gt;
 *     &lt;enumeration value="FI"/&gt;
 *     &lt;enumeration value="FLW"/&gt;
 *     &lt;enumeration value="FN"/&gt;
 *     &lt;enumeration value="FO"/&gt;
 *     &lt;enumeration value="FS"/&gt;
 *     &lt;enumeration value="FT"/&gt;
 *     &lt;enumeration value="FV"/&gt;
 *     &lt;enumeration value="FX"/&gt;
 *     &lt;enumeration value="GA"/&gt;
 *     &lt;enumeration value="GC"/&gt;
 *     &lt;enumeration value="GD"/&gt;
 *     &lt;enumeration value="GDN"/&gt;
 *     &lt;enumeration value="GN"/&gt;
 *     &lt;enumeration value="HS"/&gt;
 *     &lt;enumeration value="HWB"/&gt;
 *     &lt;enumeration value="IA"/&gt;
 *     &lt;enumeration value="IB"/&gt;
 *     &lt;enumeration value="ICA"/&gt;
 *     &lt;enumeration value="ICE"/&gt;
 *     &lt;enumeration value="ICO"/&gt;
 *     &lt;enumeration value="II"/&gt;
 *     &lt;enumeration value="IL"/&gt;
 *     &lt;enumeration value="INB"/&gt;
 *     &lt;enumeration value="INN"/&gt;
 *     &lt;enumeration value="INO"/&gt;
 *     &lt;enumeration value="IP"/&gt;
 *     &lt;enumeration value="IS"/&gt;
 *     &lt;enumeration value="IT"/&gt;
 *     &lt;enumeration value="IV"/&gt;
 *     &lt;enumeration value="JB"/&gt;
 *     &lt;enumeration value="JE"/&gt;
 *     &lt;enumeration value="LA"/&gt;
 *     &lt;enumeration value="LAN"/&gt;
 *     &lt;enumeration value="LAR"/&gt;
 *     &lt;enumeration value="LB"/&gt;
 *     &lt;enumeration value="LC"/&gt;
 *     &lt;enumeration value="LI"/&gt;
 *     &lt;enumeration value="LO"/&gt;
 *     &lt;enumeration value="LRC"/&gt;
 *     &lt;enumeration value="LS"/&gt;
 *     &lt;enumeration value="MA"/&gt;
 *     &lt;enumeration value="MB"/&gt;
 *     &lt;enumeration value="MF"/&gt;
 *     &lt;enumeration value="MG"/&gt;
 *     &lt;enumeration value="MH"/&gt;
 *     &lt;enumeration value="MR"/&gt;
 *     &lt;enumeration value="MRN"/&gt;
 *     &lt;enumeration value="MS"/&gt;
 *     &lt;enumeration value="MSS"/&gt;
 *     &lt;enumeration value="MWB"/&gt;
 *     &lt;enumeration value="NA"/&gt;
 *     &lt;enumeration value="NF"/&gt;
 *     &lt;enumeration value="OH"/&gt;
 *     &lt;enumeration value="OI"/&gt;
 *     &lt;enumeration value="ON"/&gt;
 *     &lt;enumeration value="OP"/&gt;
 *     &lt;enumeration value="OR"/&gt;
 *     &lt;enumeration value="PB"/&gt;
 *     &lt;enumeration value="PC"/&gt;
 *     &lt;enumeration value="PD"/&gt;
 *     &lt;enumeration value="PE"/&gt;
 *     &lt;enumeration value="PF"/&gt;
 *     &lt;enumeration value="PI"/&gt;
 *     &lt;enumeration value="PK"/&gt;
 *     &lt;enumeration value="PL"/&gt;
 *     &lt;enumeration value="POR"/&gt;
 *     &lt;enumeration value="PP"/&gt;
 *     &lt;enumeration value="PQ"/&gt;
 *     &lt;enumeration value="PR"/&gt;
 *     &lt;enumeration value="PS"/&gt;
 *     &lt;enumeration value="PW"/&gt;
 *     &lt;enumeration value="PY"/&gt;
 *     &lt;enumeration value="RA"/&gt;
 *     &lt;enumeration value="RC"/&gt;
 *     &lt;enumeration value="RCN"/&gt;
 *     &lt;enumeration value="RE"/&gt;
 *     &lt;enumeration value="REN"/&gt;
 *     &lt;enumeration value="RF"/&gt;
 *     &lt;enumeration value="RR"/&gt;
 *     &lt;enumeration value="RT"/&gt;
 *     &lt;enumeration value="SA"/&gt;
 *     &lt;enumeration value="SB"/&gt;
 *     &lt;enumeration value="SD"/&gt;
 *     &lt;enumeration value="SE"/&gt;
 *     &lt;enumeration value="SEA"/&gt;
 *     &lt;enumeration value="SF"/&gt;
 *     &lt;enumeration value="SH"/&gt;
 *     &lt;enumeration value="SI"/&gt;
 *     &lt;enumeration value="SM"/&gt;
 *     &lt;enumeration value="SN"/&gt;
 *     &lt;enumeration value="SP"/&gt;
 *     &lt;enumeration value="SQ"/&gt;
 *     &lt;enumeration value="SRN"/&gt;
 *     &lt;enumeration value="SS"/&gt;
 *     &lt;enumeration value="STA"/&gt;
 *     &lt;enumeration value="SW"/&gt;
 *     &lt;enumeration value="SZ"/&gt;
 *     &lt;enumeration value="TB"/&gt;
 *     &lt;enumeration value="TCR"/&gt;
 *     &lt;enumeration value="TE"/&gt;
 *     &lt;enumeration value="TF"/&gt;
 *     &lt;enumeration value="TI"/&gt;
 *     &lt;enumeration value="TIN"/&gt;
 *     &lt;enumeration value="TL"/&gt;
 *     &lt;enumeration value="TN"/&gt;
 *     &lt;enumeration value="TP"/&gt;
 *     &lt;enumeration value="UAR"/&gt;
 *     &lt;enumeration value="UC"/&gt;
 *     &lt;enumeration value="UCN"/&gt;
 *     &lt;enumeration value="UN"/&gt;
 *     &lt;enumeration value="UO"/&gt;
 *     &lt;enumeration value="URI"/&gt;
 *     &lt;enumeration value="VA"/&gt;
 *     &lt;enumeration value="VC"/&gt;
 *     &lt;enumeration value="VGR"/&gt;
 *     &lt;enumeration value="VM"/&gt;
 *     &lt;enumeration value="VN"/&gt;
 *     &lt;enumeration value="VON"/&gt;
 *     &lt;enumeration value="VOR"/&gt;
 *     &lt;enumeration value="VP"/&gt;
 *     &lt;enumeration value="VR"/&gt;
 *     &lt;enumeration value="VS"/&gt;
 *     &lt;enumeration value="VT"/&gt;
 *     &lt;enumeration value="VV"/&gt;
 *     &lt;enumeration value="WE"/&gt;
 *     &lt;enumeration value="WM"/&gt;
 *     &lt;enumeration value="WN"/&gt;
 *     &lt;enumeration value="WR"/&gt;
 *     &lt;enumeration value="WS"/&gt;
 *     &lt;enumeration value="WY"/&gt;
 *     &lt;enumeration value="XA"/&gt;
 *     &lt;enumeration value="XC"/&gt;
 *     &lt;enumeration value="XP"/&gt;
 *     &lt;enumeration value="ZZZ"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ReferenceCodeContentType", namespace = "urn:un:unece:uncefact:data:standard:QualifiedDataType:100")
@XmlEnum
public enum ReferenceCodeContentType {

    AAA,
    AAB,
    AAC,
    AAD,
    AAE,
    AAF,
    AAG,
    AAH,
    AAI,
    AAJ,
    AAK,
    AAL,
    AAM,
    AAN,
    AAO,
    AAP,
    AAQ,
    AAR,
    AAS,
    AAT,
    AAU,
    AAV,
    AAW,
    AAX,
    AAY,
    AAZ,
    ABA,
    ABB,
    ABC,
    ABD,
    ABE,
    ABF,
    ABG,
    ABH,
    ABI,
    ABJ,
    ABK,
    ABL,
    ABM,
    ABN,
    ABO,
    ABP,
    ABQ,
    ABR,
    ABS,
    ABT,
    ABU,
    ABV,
    ABW,
    ABX,
    ABY,
    ABZ,
    AC,
    ACA,
    ACB,
    ACC,
    ACD,
    ACE,
    ACF,
    ACG,
    ACH,
    ACI,
    ACJ,
    ACK,
    ACL,
    ACN,
    ACO,
    ACP,
    ACQ,
    ACR,
    ACT,
    ACU,
    ACV,
    ACW,
    ACX,
    ACY,
    ACZ,
    ADA,
    ADB,
    ADC,
    ADD,
    ADE,
    ADF,
    ADG,
    ADI,
    ADJ,
    ADK,
    ADL,
    ADM,
    ADN,
    ADO,
    ADP,
    ADQ,
    ADT,
    ADU,
    ADV,
    ADW,
    ADX,
    ADY,
    ADZ,
    AE,
    AEA,
    AEB,
    AEC,
    AED,
    AEE,
    AEF,
    AEG,
    AEH,
    AEI,
    AEJ,
    AEK,
    AEL,
    AEM,
    AEN,
    AEO,
    AEP,
    AEQ,
    AER,
    AES,
    AET,
    AEU,
    AEV,
    AEW,
    AEX,
    AEY,
    AEZ,
    AF,
    AFA,
    AFB,
    AFC,
    AFD,
    AFE,
    AFF,
    AFG,
    AFH,
    AFI,
    AFJ,
    AFK,
    AFL,
    AFM,
    AFN,
    AFO,
    AFP,
    AFQ,
    AFR,
    AFS,
    AFT,
    AFU,
    AFV,
    AFW,
    AFX,
    AFY,
    AFZ,
    AGA,
    AGB,
    AGC,
    AGD,
    AGE,
    AGF,
    AGG,
    AGH,
    AGI,
    AGJ,
    AGK,
    AGL,
    AGM,
    AGN,
    AGO,
    AGP,
    AGQ,
    AGR,
    AGS,
    AGT,
    AGU,
    AGV,
    AGW,
    AGX,
    AGY,
    AGZ,
    AHA,
    AHB,
    AHC,
    AHD,
    AHE,
    AHF,
    AHG,
    AHH,
    AHI,
    AHJ,
    AHK,
    AHL,
    AHM,
    AHN,
    AHO,
    AHP,
    AHQ,
    AHR,
    AHS,
    AHT,
    AHU,
    AHV,
    AHX,
    AHY,
    AHZ,
    AIA,
    AIB,
    AIC,
    AID,
    AIE,
    AIF,
    AIG,
    AIH,
    AII,
    AIJ,
    AIK,
    AIL,
    AIM,
    AIN,
    AIO,
    AIP,
    AIQ,
    AIR,
    AIS,
    AIT,
    AIU,
    AIV,
    AIW,
    AIX,
    AIY,
    AIZ,
    AJA,
    AJB,
    AJC,
    AJD,
    AJE,
    AJF,
    AJG,
    AJH,
    AJI,
    AJJ,
    AJK,
    AJL,
    AJM,
    AJN,
    AJO,
    AJP,
    AJQ,
    AJR,
    AJS,
    AJT,
    AJU,
    AJV,
    AJW,
    AJX,
    AJY,
    AJZ,
    AKA,
    AKB,
    AKC,
    AKD,
    AKE,
    AKF,
    AKG,
    AKH,
    AKI,
    AKJ,
    AKK,
    AKL,
    AKM,
    AKN,
    AKO,
    AKP,
    AKQ,
    AKR,
    AKS,
    AKT,
    AKU,
    AKV,
    AKW,
    AKX,
    AKY,
    AKZ,
    ALA,
    ALB,
    ALC,
    ALD,
    ALE,
    ALF,
    ALG,
    ALH,
    ALI,
    ALJ,
    ALK,
    ALL,
    ALM,
    ALN,
    ALO,
    ALP,
    ALQ,
    ALR,
    ALS,
    ALT,
    ALU,
    ALV,
    ALW,
    ALX,
    ALY,
    ALZ,
    AMA,
    AMB,
    AMC,
    AMD,
    AME,
    AMF,
    AMG,
    AMH,
    AMI,
    AMJ,
    AMK,
    AML,
    AMM,
    AMN,
    AMO,
    AMP,
    AMQ,
    AMR,
    AMS,
    AMT,
    AMU,
    AMV,
    AMW,
    AMX,
    AMY,
    AMZ,
    ANA,
    ANB,
    ANC,
    AND,
    ANE,
    ANF,
    ANG,
    ANH,
    ANI,
    ANJ,
    ANK,
    ANL,
    ANM,
    ANN,
    ANO,
    ANP,
    ANQ,
    ANR,
    ANS,
    ANT,
    ANU,
    ANV,
    ANW,
    ANX,
    ANY,
    AOA,
    AOD,
    AOE,
    AOF,
    AOG,
    AOH,
    AOI,
    AOJ,
    AOK,
    AOL,
    AOM,
    AON,
    AOO,
    AOP,
    AOQ,
    AOR,
    AOS,
    AOT,
    AOU,
    AOV,
    AOW,
    AOX,
    AOY,
    AOZ,
    AP,
    APA,
    APB,
    APC,
    APD,
    APE,
    APF,
    APG,
    APH,
    API,
    APJ,
    APK,
    APL,
    APM,
    APN,
    APO,
    APP,
    APQ,
    APR,
    APS,
    APT,
    APU,
    APV,
    APW,
    APX,
    APY,
    APZ,
    AQA,
    AQB,
    AQC,
    AQD,
    AQE,
    AQF,
    AQG,
    AQH,
    AQI,
    AQJ,
    AQK,
    AQL,
    AQM,
    AQN,
    AQO,
    AQP,
    AQQ,
    AQR,
    AQS,
    AQT,
    AQU,
    AQV,
    AQW,
    AQX,
    AQY,
    AQZ,
    ARA,
    ARB,
    ARC,
    ARD,
    ARE,
    ARF,
    ARG,
    ARH,
    ARI,
    ARJ,
    ARK,
    ARL,
    ARM,
    ARN,
    ARO,
    ARP,
    ARQ,
    ARR,
    ARS,
    ART,
    ARU,
    ARV,
    ARW,
    ARX,
    ARY,
    ARZ,
    ASA,
    ASB,
    ASC,
    ASD,
    ASE,
    ASF,
    ASG,
    ASH,
    ASI,
    ASJ,
    ASK,
    ASL,
    ASM,
    ASN,
    ASO,
    ASP,
    ASQ,
    ASR,
    ASS,
    AST,
    ASU,
    ASV,
    ASW,
    ASX,
    ASY,
    ASZ,
    ATA,
    ATB,
    ATC,
    ATD,
    ATE,
    ATF,
    ATG,
    ATH,
    ATI,
    ATJ,
    ATK,
    ATL,
    ATM,
    ATN,
    ATO,
    ATP,
    ATQ,
    ATR,
    ATS,
    ATT,
    ATU,
    ATV,
    ATW,
    ATX,
    ATY,
    ATZ,
    AU,
    AUA,
    AUB,
    AUC,
    AUD,
    AUE,
    AUF,
    AUG,
    AUH,
    AUI,
    AUJ,
    AUK,
    AUL,
    AUM,
    AUN,
    AUO,
    AUP,
    AUQ,
    AUR,
    AUS,
    AUT,
    AUU,
    AUV,
    AUW,
    AUX,
    AUY,
    AUZ,
    AV,
    AVA,
    AVB,
    AVC,
    AVD,
    AVE,
    AVF,
    AVG,
    AVH,
    AVI,
    AVJ,
    AVK,
    AVL,
    AVM,
    AVN,
    AVO,
    AVP,
    AVQ,
    AVR,
    AVS,
    AVT,
    AVU,
    AVV,
    AVW,
    AVX,
    AVY,
    AVZ,
    AWA,
    AWB,
    AWC,
    AWD,
    AWE,
    AWF,
    AWG,
    AWH,
    AWI,
    AWJ,
    AWK,
    AWL,
    AWM,
    AWN,
    AWO,
    AWP,
    AWQ,
    AWR,
    AWS,
    AWT,
    AWU,
    AWV,
    AWW,
    AWX,
    AWY,
    AWZ,
    AXA,
    AXB,
    AXC,
    AXD,
    AXE,
    AXF,
    AXG,
    AXH,
    AXI,
    AXJ,
    AXK,
    AXL,
    AXM,
    AXN,
    AXO,
    AXP,
    AXQ,
    AXR,
    AXS,
    BA,
    BC,
    BD,
    BE,
    BH,
    BM,
    BN,
    BO,
    BR,
    BT,
    BTP,
    BW,
    CAS,
    CAT,
    CAU,
    CAV,
    CAW,
    CAX,
    CAY,
    CAZ,
    CBA,
    CBB,
    CD,
    CEC,
    CED,
    CFE,
    CFF,
    CFO,
    CG,
    CH,
    CK,
    CKN,
    CM,
    CMR,
    CN,
    CNO,
    COF,
    CP,
    CR,
    CRN,
    CS,
    CST,
    CT,
    CU,
    CV,
    CW,
    CZ,
    DA,
    DAN,
    DB,
    DI,
    DL,
    DM,
    DQ,
    DR,
    EA,
    EB,
    ED,
    EE,
    EEP,
    EI,
    EN,
    EQ,
    ER,
    ERN,
    ET,
    EX,
    FC,
    FF,
    FI,
    FLW,
    FN,
    FO,
    FS,
    FT,
    FV,
    FX,
    GA,
    GC,
    GD,
    GDN,
    GN,
    HS,
    HWB,
    IA,
    IB,
    ICA,
    ICE,
    ICO,
    II,
    IL,
    INB,
    INN,
    INO,
    IP,
    IS,
    IT,
    IV,
    JB,
    JE,
    LA,
    LAN,
    LAR,
    LB,
    LC,
    LI,
    LO,
    LRC,
    LS,
    MA,
    MB,
    MF,
    MG,
    MH,
    MR,
    MRN,
    MS,
    MSS,
    MWB,
    NA,
    NF,
    OH,
    OI,
    ON,
    OP,
    OR,
    PB,
    PC,
    PD,
    PE,
    PF,
    PI,
    PK,
    PL,
    POR,
    PP,
    PQ,
    PR,
    PS,
    PW,
    PY,
    RA,
    RC,
    RCN,
    RE,
    REN,
    RF,
    RR,
    RT,
    SA,
    SB,
    SD,
    SE,
    SEA,
    SF,
    SH,
    SI,
    SM,
    SN,
    SP,
    SQ,
    SRN,
    SS,
    STA,
    SW,
    SZ,
    TB,
    TCR,
    TE,
    TF,
    TI,
    TIN,
    TL,
    TN,
    TP,
    UAR,
    UC,
    UCN,
    UN,
    UO,
    URI,
    VA,
    VC,
    VGR,
    VM,
    VN,
    VON,
    VOR,
    VP,
    VR,
    VS,
    VT,
    VV,
    WE,
    WM,
    WN,
    WR,
    WS,
    WY,
    XA,
    XC,
    XP,
    ZZZ;

    public String value() {
        return name();
    }

    public static ReferenceCodeContentType fromValue(String v) {
        return valueOf(v);
    }

}
