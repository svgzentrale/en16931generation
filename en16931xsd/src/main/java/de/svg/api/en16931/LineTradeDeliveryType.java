
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für LineTradeDeliveryType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LineTradeDeliveryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BilledQuantity" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}QuantityType"/&gt;
 *         &lt;element name="ChargeFreeQuantity" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}QuantityType" minOccurs="0"/&gt;
 *         &lt;element name="PackageQuantity" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}QuantityType" minOccurs="0"/&gt;
 *         &lt;element name="ShipToTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/&gt;
 *         &lt;element name="UltimateShipToTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/&gt;
 *         &lt;element name="ActualDeliverySupplyChainEvent" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}SupplyChainEventType" minOccurs="0"/&gt;
 *         &lt;element name="DespatchAdviceReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/&gt;
 *         &lt;element name="ReceivingAdviceReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryNoteReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineTradeDeliveryType", propOrder = {
    "billedQuantity",
    "chargeFreeQuantity",
    "packageQuantity",
    "shipToTradeParty",
    "ultimateShipToTradeParty",
    "actualDeliverySupplyChainEvent",
    "despatchAdviceReferencedDocument",
    "receivingAdviceReferencedDocument",
    "deliveryNoteReferencedDocument"
})
public class LineTradeDeliveryType {

    @XmlElement(name = "BilledQuantity", required = true)
    protected QuantityType billedQuantity;
    @XmlElement(name = "ChargeFreeQuantity")
    protected QuantityType chargeFreeQuantity;
    @XmlElement(name = "PackageQuantity")
    protected QuantityType packageQuantity;
    @XmlElement(name = "ShipToTradeParty")
    protected TradePartyType shipToTradeParty;
    @XmlElement(name = "UltimateShipToTradeParty")
    protected TradePartyType ultimateShipToTradeParty;
    @XmlElement(name = "ActualDeliverySupplyChainEvent")
    protected SupplyChainEventType actualDeliverySupplyChainEvent;
    @XmlElement(name = "DespatchAdviceReferencedDocument")
    protected ReferencedDocumentType despatchAdviceReferencedDocument;
    @XmlElement(name = "ReceivingAdviceReferencedDocument")
    protected ReferencedDocumentType receivingAdviceReferencedDocument;
    @XmlElement(name = "DeliveryNoteReferencedDocument")
    protected ReferencedDocumentType deliveryNoteReferencedDocument;

    /**
     * Ruft den Wert der billedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QuantityType }
     *     
     */
    public QuantityType getBilledQuantity() {
        return billedQuantity;
    }

    /**
     * Legt den Wert der billedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityType }
     *     
     */
    public void setBilledQuantity(QuantityType value) {
        this.billedQuantity = value;
    }

    /**
     * Ruft den Wert der chargeFreeQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QuantityType }
     *     
     */
    public QuantityType getChargeFreeQuantity() {
        return chargeFreeQuantity;
    }

    /**
     * Legt den Wert der chargeFreeQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityType }
     *     
     */
    public void setChargeFreeQuantity(QuantityType value) {
        this.chargeFreeQuantity = value;
    }

    /**
     * Ruft den Wert der packageQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QuantityType }
     *     
     */
    public QuantityType getPackageQuantity() {
        return packageQuantity;
    }

    /**
     * Legt den Wert der packageQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityType }
     *     
     */
    public void setPackageQuantity(QuantityType value) {
        this.packageQuantity = value;
    }

    /**
     * Ruft den Wert der shipToTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getShipToTradeParty() {
        return shipToTradeParty;
    }

    /**
     * Legt den Wert der shipToTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setShipToTradeParty(TradePartyType value) {
        this.shipToTradeParty = value;
    }

    /**
     * Ruft den Wert der ultimateShipToTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getUltimateShipToTradeParty() {
        return ultimateShipToTradeParty;
    }

    /**
     * Legt den Wert der ultimateShipToTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setUltimateShipToTradeParty(TradePartyType value) {
        this.ultimateShipToTradeParty = value;
    }

    /**
     * Ruft den Wert der actualDeliverySupplyChainEvent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SupplyChainEventType }
     *     
     */
    public SupplyChainEventType getActualDeliverySupplyChainEvent() {
        return actualDeliverySupplyChainEvent;
    }

    /**
     * Legt den Wert der actualDeliverySupplyChainEvent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplyChainEventType }
     *     
     */
    public void setActualDeliverySupplyChainEvent(SupplyChainEventType value) {
        this.actualDeliverySupplyChainEvent = value;
    }

    /**
     * Ruft den Wert der despatchAdviceReferencedDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getDespatchAdviceReferencedDocument() {
        return despatchAdviceReferencedDocument;
    }

    /**
     * Legt den Wert der despatchAdviceReferencedDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setDespatchAdviceReferencedDocument(ReferencedDocumentType value) {
        this.despatchAdviceReferencedDocument = value;
    }

    /**
     * Ruft den Wert der receivingAdviceReferencedDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getReceivingAdviceReferencedDocument() {
        return receivingAdviceReferencedDocument;
    }

    /**
     * Legt den Wert der receivingAdviceReferencedDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setReceivingAdviceReferencedDocument(ReferencedDocumentType value) {
        this.receivingAdviceReferencedDocument = value;
    }

    /**
     * Ruft den Wert der deliveryNoteReferencedDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getDeliveryNoteReferencedDocument() {
        return deliveryNoteReferencedDocument;
    }

    /**
     * Legt den Wert der deliveryNoteReferencedDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setDeliveryNoteReferencedDocument(ReferencedDocumentType value) {
        this.deliveryNoteReferencedDocument = value;
    }

}
