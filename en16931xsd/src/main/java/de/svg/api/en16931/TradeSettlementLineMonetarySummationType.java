
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TradeSettlementLineMonetarySummationType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TradeSettlementLineMonetarySummationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LineTotalAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType"/&gt;
 *         &lt;element name="TotalAllowanceChargeAmount" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}AmountType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeSettlementLineMonetarySummationType", propOrder = {
    "lineTotalAmount",
    "totalAllowanceChargeAmount"
})
public class TradeSettlementLineMonetarySummationType {

    @XmlElement(name = "LineTotalAmount", required = true)
    protected AmountType lineTotalAmount;
    @XmlElement(name = "TotalAllowanceChargeAmount")
    protected AmountType totalAllowanceChargeAmount;

    /**
     * Ruft den Wert der lineTotalAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getLineTotalAmount() {
        return lineTotalAmount;
    }

    /**
     * Legt den Wert der lineTotalAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setLineTotalAmount(AmountType value) {
        this.lineTotalAmount = value;
    }

    /**
     * Ruft den Wert der totalAllowanceChargeAmount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getTotalAllowanceChargeAmount() {
        return totalAllowanceChargeAmount;
    }

    /**
     * Legt den Wert der totalAllowanceChargeAmount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setTotalAllowanceChargeAmount(AmountType value) {
        this.totalAllowanceChargeAmount = value;
    }

}
