
package de.svg.api.en16931;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für HeaderTradeSettlementType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HeaderTradeSettlementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreditorReferenceID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" minOccurs="0"/&gt;
 *         &lt;element name="PaymentReference" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/&gt;
 *         &lt;element name="TaxCurrencyCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}CurrencyCodeType" minOccurs="0"/&gt;
 *         &lt;element name="InvoiceCurrencyCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}CurrencyCodeType"/&gt;
 *         &lt;element name="InvoiceIssuerReference" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/&gt;
 *         &lt;element name="InvoicerTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/&gt;
 *         &lt;element name="InvoiceeTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/&gt;
 *         &lt;element name="PayeeTradeParty" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePartyType" minOccurs="0"/&gt;
 *         &lt;element name="TaxApplicableTradeCurrencyExchange" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeCurrencyExchangeType" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedTradeSettlementPaymentMeans" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeSettlementPaymentMeansType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ApplicableTradeTax" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeTaxType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="BillingSpecifiedPeriod" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}SpecifiedPeriodType" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedTradeAllowanceCharge" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeAllowanceChargeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedLogisticsServiceCharge" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LogisticsServiceChargeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedTradePaymentTerms" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradePaymentTermsType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedTradeSettlementHeaderMonetarySummation" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeSettlementHeaderMonetarySummationType"/&gt;
 *         &lt;element name="InvoiceReferencedDocument" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}ReferencedDocumentType" minOccurs="0"/&gt;
 *         &lt;element name="ReceivableSpecifiedTradeAccountingAccount" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeAccountingAccountType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedAdvancePayment" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}AdvancePaymentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderTradeSettlementType", propOrder = {
    "creditorReferenceID",
    "paymentReference",
    "taxCurrencyCode",
    "invoiceCurrencyCode",
    "invoiceIssuerReference",
    "invoicerTradeParty",
    "invoiceeTradeParty",
    "payeeTradeParty",
    "taxApplicableTradeCurrencyExchange",
    "specifiedTradeSettlementPaymentMeans",
    "applicableTradeTax",
    "billingSpecifiedPeriod",
    "specifiedTradeAllowanceCharge",
    "specifiedLogisticsServiceCharge",
    "specifiedTradePaymentTerms",
    "specifiedTradeSettlementHeaderMonetarySummation",
    "invoiceReferencedDocument",
    "receivableSpecifiedTradeAccountingAccount",
    "specifiedAdvancePayment"
})
public class HeaderTradeSettlementType {

    @XmlElement(name = "CreditorReferenceID")
    protected IDType creditorReferenceID;
    @XmlElement(name = "PaymentReference")
    protected TextType paymentReference;
    @XmlElement(name = "TaxCurrencyCode")
    protected CurrencyCodeType taxCurrencyCode;
    @XmlElement(name = "InvoiceCurrencyCode", required = true)
    protected CurrencyCodeType invoiceCurrencyCode;
    @XmlElement(name = "InvoiceIssuerReference")
    protected TextType invoiceIssuerReference;
    @XmlElement(name = "InvoicerTradeParty")
    protected TradePartyType invoicerTradeParty;
    @XmlElement(name = "InvoiceeTradeParty")
    protected TradePartyType invoiceeTradeParty;
    @XmlElement(name = "PayeeTradeParty")
    protected TradePartyType payeeTradeParty;
    @XmlElement(name = "TaxApplicableTradeCurrencyExchange")
    protected TradeCurrencyExchangeType taxApplicableTradeCurrencyExchange;
    @XmlElement(name = "SpecifiedTradeSettlementPaymentMeans")
    protected List<TradeSettlementPaymentMeansType> specifiedTradeSettlementPaymentMeans;
    @XmlElement(name = "ApplicableTradeTax", required = true)
    protected List<TradeTaxType> applicableTradeTax;
    @XmlElement(name = "BillingSpecifiedPeriod")
    protected SpecifiedPeriodType billingSpecifiedPeriod;
    @XmlElement(name = "SpecifiedTradeAllowanceCharge")
    protected List<TradeAllowanceChargeType> specifiedTradeAllowanceCharge;
    @XmlElement(name = "SpecifiedLogisticsServiceCharge")
    protected List<LogisticsServiceChargeType> specifiedLogisticsServiceCharge;
    @XmlElement(name = "SpecifiedTradePaymentTerms")
    protected List<TradePaymentTermsType> specifiedTradePaymentTerms;
    @XmlElement(name = "SpecifiedTradeSettlementHeaderMonetarySummation", required = true)
    protected TradeSettlementHeaderMonetarySummationType specifiedTradeSettlementHeaderMonetarySummation;
    @XmlElement(name = "InvoiceReferencedDocument")
    protected ReferencedDocumentType invoiceReferencedDocument;
    @XmlElement(name = "ReceivableSpecifiedTradeAccountingAccount")
    protected List<TradeAccountingAccountType> receivableSpecifiedTradeAccountingAccount;
    @XmlElement(name = "SpecifiedAdvancePayment")
    protected List<AdvancePaymentType> specifiedAdvancePayment;

    /**
     * Ruft den Wert der creditorReferenceID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IDType }
     *     
     */
    public IDType getCreditorReferenceID() {
        return creditorReferenceID;
    }

    /**
     * Legt den Wert der creditorReferenceID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IDType }
     *     
     */
    public void setCreditorReferenceID(IDType value) {
        this.creditorReferenceID = value;
    }

    /**
     * Ruft den Wert der paymentReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getPaymentReference() {
        return paymentReference;
    }

    /**
     * Legt den Wert der paymentReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setPaymentReference(TextType value) {
        this.paymentReference = value;
    }

    /**
     * Ruft den Wert der taxCurrencyCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getTaxCurrencyCode() {
        return taxCurrencyCode;
    }

    /**
     * Legt den Wert der taxCurrencyCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setTaxCurrencyCode(CurrencyCodeType value) {
        this.taxCurrencyCode = value;
    }

    /**
     * Ruft den Wert der invoiceCurrencyCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getInvoiceCurrencyCode() {
        return invoiceCurrencyCode;
    }

    /**
     * Legt den Wert der invoiceCurrencyCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setInvoiceCurrencyCode(CurrencyCodeType value) {
        this.invoiceCurrencyCode = value;
    }

    /**
     * Ruft den Wert der invoiceIssuerReference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getInvoiceIssuerReference() {
        return invoiceIssuerReference;
    }

    /**
     * Legt den Wert der invoiceIssuerReference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setInvoiceIssuerReference(TextType value) {
        this.invoiceIssuerReference = value;
    }

    /**
     * Ruft den Wert der invoicerTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getInvoicerTradeParty() {
        return invoicerTradeParty;
    }

    /**
     * Legt den Wert der invoicerTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setInvoicerTradeParty(TradePartyType value) {
        this.invoicerTradeParty = value;
    }

    /**
     * Ruft den Wert der invoiceeTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getInvoiceeTradeParty() {
        return invoiceeTradeParty;
    }

    /**
     * Legt den Wert der invoiceeTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setInvoiceeTradeParty(TradePartyType value) {
        this.invoiceeTradeParty = value;
    }

    /**
     * Ruft den Wert der payeeTradeParty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradePartyType }
     *     
     */
    public TradePartyType getPayeeTradeParty() {
        return payeeTradeParty;
    }

    /**
     * Legt den Wert der payeeTradeParty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradePartyType }
     *     
     */
    public void setPayeeTradeParty(TradePartyType value) {
        this.payeeTradeParty = value;
    }

    /**
     * Ruft den Wert der taxApplicableTradeCurrencyExchange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradeCurrencyExchangeType }
     *     
     */
    public TradeCurrencyExchangeType getTaxApplicableTradeCurrencyExchange() {
        return taxApplicableTradeCurrencyExchange;
    }

    /**
     * Legt den Wert der taxApplicableTradeCurrencyExchange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeCurrencyExchangeType }
     *     
     */
    public void setTaxApplicableTradeCurrencyExchange(TradeCurrencyExchangeType value) {
        this.taxApplicableTradeCurrencyExchange = value;
    }

    /**
     * Gets the value of the specifiedTradeSettlementPaymentMeans property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTradeSettlementPaymentMeans property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecifiedTradeSettlementPaymentMeans().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeSettlementPaymentMeansType }
     * 
     * 
     */
    public List<TradeSettlementPaymentMeansType> getSpecifiedTradeSettlementPaymentMeans() {
        if (specifiedTradeSettlementPaymentMeans == null) {
            specifiedTradeSettlementPaymentMeans = new ArrayList<TradeSettlementPaymentMeansType>();
        }
        return this.specifiedTradeSettlementPaymentMeans;
    }

    /**
     * Gets the value of the applicableTradeTax property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicableTradeTax property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicableTradeTax().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeTaxType }
     * 
     * 
     */
    public List<TradeTaxType> getApplicableTradeTax() {
        if (applicableTradeTax == null) {
            applicableTradeTax = new ArrayList<TradeTaxType>();
        }
        return this.applicableTradeTax;
    }

    /**
     * Ruft den Wert der billingSpecifiedPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SpecifiedPeriodType }
     *     
     */
    public SpecifiedPeriodType getBillingSpecifiedPeriod() {
        return billingSpecifiedPeriod;
    }

    /**
     * Legt den Wert der billingSpecifiedPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecifiedPeriodType }
     *     
     */
    public void setBillingSpecifiedPeriod(SpecifiedPeriodType value) {
        this.billingSpecifiedPeriod = value;
    }

    /**
     * Gets the value of the specifiedTradeAllowanceCharge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTradeAllowanceCharge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecifiedTradeAllowanceCharge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeAllowanceChargeType }
     * 
     * 
     */
    public List<TradeAllowanceChargeType> getSpecifiedTradeAllowanceCharge() {
        if (specifiedTradeAllowanceCharge == null) {
            specifiedTradeAllowanceCharge = new ArrayList<TradeAllowanceChargeType>();
        }
        return this.specifiedTradeAllowanceCharge;
    }

    /**
     * Gets the value of the specifiedLogisticsServiceCharge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedLogisticsServiceCharge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecifiedLogisticsServiceCharge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LogisticsServiceChargeType }
     * 
     * 
     */
    public List<LogisticsServiceChargeType> getSpecifiedLogisticsServiceCharge() {
        if (specifiedLogisticsServiceCharge == null) {
            specifiedLogisticsServiceCharge = new ArrayList<LogisticsServiceChargeType>();
        }
        return this.specifiedLogisticsServiceCharge;
    }

    /**
     * Gets the value of the specifiedTradePaymentTerms property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTradePaymentTerms property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecifiedTradePaymentTerms().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradePaymentTermsType }
     * 
     * 
     */
    public List<TradePaymentTermsType> getSpecifiedTradePaymentTerms() {
        if (specifiedTradePaymentTerms == null) {
            specifiedTradePaymentTerms = new ArrayList<TradePaymentTermsType>();
        }
        return this.specifiedTradePaymentTerms;
    }

    /**
     * Ruft den Wert der specifiedTradeSettlementHeaderMonetarySummation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradeSettlementHeaderMonetarySummationType }
     *     
     */
    public TradeSettlementHeaderMonetarySummationType getSpecifiedTradeSettlementHeaderMonetarySummation() {
        return specifiedTradeSettlementHeaderMonetarySummation;
    }

    /**
     * Legt den Wert der specifiedTradeSettlementHeaderMonetarySummation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeSettlementHeaderMonetarySummationType }
     *     
     */
    public void setSpecifiedTradeSettlementHeaderMonetarySummation(TradeSettlementHeaderMonetarySummationType value) {
        this.specifiedTradeSettlementHeaderMonetarySummation = value;
    }

    /**
     * Ruft den Wert der invoiceReferencedDocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public ReferencedDocumentType getInvoiceReferencedDocument() {
        return invoiceReferencedDocument;
    }

    /**
     * Legt den Wert der invoiceReferencedDocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedDocumentType }
     *     
     */
    public void setInvoiceReferencedDocument(ReferencedDocumentType value) {
        this.invoiceReferencedDocument = value;
    }

    /**
     * Gets the value of the receivableSpecifiedTradeAccountingAccount property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receivableSpecifiedTradeAccountingAccount property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceivableSpecifiedTradeAccountingAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TradeAccountingAccountType }
     * 
     * 
     */
    public List<TradeAccountingAccountType> getReceivableSpecifiedTradeAccountingAccount() {
        if (receivableSpecifiedTradeAccountingAccount == null) {
            receivableSpecifiedTradeAccountingAccount = new ArrayList<TradeAccountingAccountType>();
        }
        return this.receivableSpecifiedTradeAccountingAccount;
    }

    /**
     * Gets the value of the specifiedAdvancePayment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedAdvancePayment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecifiedAdvancePayment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdvancePaymentType }
     * 
     * 
     */
    public List<AdvancePaymentType> getSpecifiedAdvancePayment() {
        if (specifiedAdvancePayment == null) {
            specifiedAdvancePayment = new ArrayList<AdvancePaymentType>();
        }
        return this.specifiedAdvancePayment;
    }

}
