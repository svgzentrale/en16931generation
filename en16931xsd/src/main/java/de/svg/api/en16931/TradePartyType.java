
package de.svg.api.en16931;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TradePartyType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TradePartyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GlobalID" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}IDType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Name" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedLegalOrganization" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}LegalOrganizationType" minOccurs="0"/&gt;
 *         &lt;element name="DefinedTradeContact" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeContactType" minOccurs="0"/&gt;
 *         &lt;element name="PostalTradeAddress" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeAddressType" minOccurs="0"/&gt;
 *         &lt;element name="URIUniversalCommunication" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}UniversalCommunicationType" minOccurs="0"/&gt;
 *         &lt;element name="SpecifiedTaxRegistration" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TaxRegistrationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradePartyType", propOrder = {
    "id",
    "globalID",
    "name",
    "description",
    "specifiedLegalOrganization",
    "definedTradeContact",
    "postalTradeAddress",
    "uriUniversalCommunication",
    "specifiedTaxRegistration"
})
public class TradePartyType {

    @XmlElement(name = "ID")
    protected List<IDType> id;
    @XmlElement(name = "GlobalID")
    protected List<IDType> globalID;
    @XmlElement(name = "Name")
    protected TextType name;
    @XmlElement(name = "Description")
    protected TextType description;
    @XmlElement(name = "SpecifiedLegalOrganization")
    protected LegalOrganizationType specifiedLegalOrganization;
    @XmlElement(name = "DefinedTradeContact")
    protected TradeContactType definedTradeContact;
    @XmlElement(name = "PostalTradeAddress")
    protected TradeAddressType postalTradeAddress;
    @XmlElement(name = "URIUniversalCommunication")
    protected UniversalCommunicationType uriUniversalCommunication;
    @XmlElement(name = "SpecifiedTaxRegistration")
    protected List<TaxRegistrationType> specifiedTaxRegistration;

    /**
     * Gets the value of the id property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the id property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IDType }
     * 
     * 
     */
    public List<IDType> getID() {
        if (id == null) {
            id = new ArrayList<IDType>();
        }
        return this.id;
    }

    /**
     * Gets the value of the globalID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the globalID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGlobalID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IDType }
     * 
     * 
     */
    public List<IDType> getGlobalID() {
        if (globalID == null) {
            globalID = new ArrayList<IDType>();
        }
        return this.globalID;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setName(TextType value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setDescription(TextType value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der specifiedLegalOrganization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LegalOrganizationType }
     *     
     */
    public LegalOrganizationType getSpecifiedLegalOrganization() {
        return specifiedLegalOrganization;
    }

    /**
     * Legt den Wert der specifiedLegalOrganization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LegalOrganizationType }
     *     
     */
    public void setSpecifiedLegalOrganization(LegalOrganizationType value) {
        this.specifiedLegalOrganization = value;
    }

    /**
     * Ruft den Wert der definedTradeContact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradeContactType }
     *     
     */
    public TradeContactType getDefinedTradeContact() {
        return definedTradeContact;
    }

    /**
     * Legt den Wert der definedTradeContact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeContactType }
     *     
     */
    public void setDefinedTradeContact(TradeContactType value) {
        this.definedTradeContact = value;
    }

    /**
     * Ruft den Wert der postalTradeAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradeAddressType }
     *     
     */
    public TradeAddressType getPostalTradeAddress() {
        return postalTradeAddress;
    }

    /**
     * Legt den Wert der postalTradeAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeAddressType }
     *     
     */
    public void setPostalTradeAddress(TradeAddressType value) {
        this.postalTradeAddress = value;
    }

    /**
     * Ruft den Wert der uriUniversalCommunication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UniversalCommunicationType }
     *     
     */
    public UniversalCommunicationType getURIUniversalCommunication() {
        return uriUniversalCommunication;
    }

    /**
     * Legt den Wert der uriUniversalCommunication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UniversalCommunicationType }
     *     
     */
    public void setURIUniversalCommunication(UniversalCommunicationType value) {
        this.uriUniversalCommunication = value;
    }

    /**
     * Gets the value of the specifiedTaxRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specifiedTaxRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecifiedTaxRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxRegistrationType }
     * 
     * 
     */
    public List<TaxRegistrationType> getSpecifiedTaxRegistration() {
        if (specifiedTaxRegistration == null) {
            specifiedTaxRegistration = new ArrayList<TaxRegistrationType>();
        }
        return this.specifiedTaxRegistration;
    }

}
