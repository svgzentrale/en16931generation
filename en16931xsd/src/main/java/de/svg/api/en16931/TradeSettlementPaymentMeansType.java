
package de.svg.api.en16931;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TradeSettlementPaymentMeansType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="TradeSettlementPaymentMeansType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TypeCode" type="{urn:un:unece:uncefact:data:standard:QualifiedDataType:100}PaymentMeansCodeType"/&gt;
 *         &lt;element name="Information" type="{urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100}TextType" minOccurs="0"/&gt;
 *         &lt;element name="ApplicableTradeSettlementFinancialCard" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}TradeSettlementFinancialCardType" minOccurs="0"/&gt;
 *         &lt;element name="PayerPartyDebtorFinancialAccount" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}DebtorFinancialAccountType" minOccurs="0"/&gt;
 *         &lt;element name="PayeePartyCreditorFinancialAccount" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}CreditorFinancialAccountType" minOccurs="0"/&gt;
 *         &lt;element name="PayeeSpecifiedCreditorFinancialInstitution" type="{urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100}CreditorFinancialInstitutionType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradeSettlementPaymentMeansType", propOrder = {
    "typeCode",
    "information",
    "applicableTradeSettlementFinancialCard",
    "payerPartyDebtorFinancialAccount",
    "payeePartyCreditorFinancialAccount",
    "payeeSpecifiedCreditorFinancialInstitution"
})
public class TradeSettlementPaymentMeansType {

    @XmlElement(name = "TypeCode", required = true)
    protected PaymentMeansCodeType typeCode;
    @XmlElement(name = "Information")
    protected TextType information;
    @XmlElement(name = "ApplicableTradeSettlementFinancialCard")
    protected TradeSettlementFinancialCardType applicableTradeSettlementFinancialCard;
    @XmlElement(name = "PayerPartyDebtorFinancialAccount")
    protected DebtorFinancialAccountType payerPartyDebtorFinancialAccount;
    @XmlElement(name = "PayeePartyCreditorFinancialAccount")
    protected CreditorFinancialAccountType payeePartyCreditorFinancialAccount;
    @XmlElement(name = "PayeeSpecifiedCreditorFinancialInstitution")
    protected CreditorFinancialInstitutionType payeeSpecifiedCreditorFinancialInstitution;

    /**
     * Ruft den Wert der typeCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMeansCodeType }
     *     
     */
    public PaymentMeansCodeType getTypeCode() {
        return typeCode;
    }

    /**
     * Legt den Wert der typeCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMeansCodeType }
     *     
     */
    public void setTypeCode(PaymentMeansCodeType value) {
        this.typeCode = value;
    }

    /**
     * Ruft den Wert der information-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getInformation() {
        return information;
    }

    /**
     * Legt den Wert der information-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setInformation(TextType value) {
        this.information = value;
    }

    /**
     * Ruft den Wert der applicableTradeSettlementFinancialCard-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TradeSettlementFinancialCardType }
     *     
     */
    public TradeSettlementFinancialCardType getApplicableTradeSettlementFinancialCard() {
        return applicableTradeSettlementFinancialCard;
    }

    /**
     * Legt den Wert der applicableTradeSettlementFinancialCard-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TradeSettlementFinancialCardType }
     *     
     */
    public void setApplicableTradeSettlementFinancialCard(TradeSettlementFinancialCardType value) {
        this.applicableTradeSettlementFinancialCard = value;
    }

    /**
     * Ruft den Wert der payerPartyDebtorFinancialAccount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DebtorFinancialAccountType }
     *     
     */
    public DebtorFinancialAccountType getPayerPartyDebtorFinancialAccount() {
        return payerPartyDebtorFinancialAccount;
    }

    /**
     * Legt den Wert der payerPartyDebtorFinancialAccount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DebtorFinancialAccountType }
     *     
     */
    public void setPayerPartyDebtorFinancialAccount(DebtorFinancialAccountType value) {
        this.payerPartyDebtorFinancialAccount = value;
    }

    /**
     * Ruft den Wert der payeePartyCreditorFinancialAccount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CreditorFinancialAccountType }
     *     
     */
    public CreditorFinancialAccountType getPayeePartyCreditorFinancialAccount() {
        return payeePartyCreditorFinancialAccount;
    }

    /**
     * Legt den Wert der payeePartyCreditorFinancialAccount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditorFinancialAccountType }
     *     
     */
    public void setPayeePartyCreditorFinancialAccount(CreditorFinancialAccountType value) {
        this.payeePartyCreditorFinancialAccount = value;
    }

    /**
     * Ruft den Wert der payeeSpecifiedCreditorFinancialInstitution-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CreditorFinancialInstitutionType }
     *     
     */
    public CreditorFinancialInstitutionType getPayeeSpecifiedCreditorFinancialInstitution() {
        return payeeSpecifiedCreditorFinancialInstitution;
    }

    /**
     * Legt den Wert der payeeSpecifiedCreditorFinancialInstitution-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditorFinancialInstitutionType }
     *     
     */
    public void setPayeeSpecifiedCreditorFinancialInstitution(CreditorFinancialInstitutionType value) {
        this.payeeSpecifiedCreditorFinancialInstitution = value;
    }

}
