package de.svg.controller;

import de.svg.dto.InvoiceItem;
import de.svg.dto.SvgInvoice;
import de.svg.service.en16931.En16931XmlGenerationService;
import de.svg.service.svginvoice.SettlementInvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class SettlementController extends AbstractEn16931GenerationController {

    private final SettlementInvoiceService settlementInvoiceService;
    @Setter
    private String output_directory;

    @Value("${output.directory}")
    private String OUTPUT_DIRECTORY;

    public SettlementController(En16931XmlGenerationService en16931XmlGenerationService, SettlementInvoiceService settlementInvoiceService) {
        super(en16931XmlGenerationService);
        this.settlementInvoiceService = settlementInvoiceService;
    }


    @GetMapping("settlement/{processingGroupAbbreviation}/invoice/{id}/en16931")
    @Operation(description = "create the en16931 xml for the collectice invoice")
    public ResponseEntity<String> createCollectiveInvoice(@PathVariable ProcessingGroupAbbreviation processingGroupAbbreviation, @PathVariable Long id) {

        SvgInvoice
                invoice = settlementInvoiceService.createCollectiveInvoice(processingGroupAbbreviation, id);

        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @GetMapping("settlement/{processingGroupAbbreviation}/country-invoice/{id}/en16931")
    public ResponseEntity<String> createCountryInvoice(@PathVariable ProcessingGroupAbbreviation processingGroupAbbreviation, @PathVariable Long id) {
        SvgInvoice
                invoice = settlementInvoiceService.createCountryInvoice(id, processingGroupAbbreviation);

        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }


    @PostMapping("settlement/{processingGroupAbbreviation}/invoice/{id}/en16931Files")
    @Operation(description = "Get shell files.")
    public ResponseEntity generateInvoiceXmlFile(@PathVariable ProcessingGroupAbbreviation processingGroupAbbreviation, @PathVariable Long id) {

        setOutput_directory("/settlement/" + processingGroupAbbreviation.name() +"/");

        SvgInvoice
                collectiveInvoice = settlementInvoiceService.createCollectiveInvoice(processingGroupAbbreviation, id);

        List<SvgInvoice> invoicesToFileprint = collectiveInvoice.getItems()
                .stream()
                .map(InvoiceItem::getItemId)
                .map(invoiceNumber ->
                {
                    try {
                        return settlementInvoiceService.createCountryInvoice(invoiceNumber, processingGroupAbbreviation);
                    } catch (EntityNotFoundException e) {
                        log.info(e.getLocalizedMessage());
                        return null;
                    }

                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        invoicesToFileprint.add(collectiveInvoice);
        invoicesToFileprint.forEach(this::writeInvoiceXmlToFile);
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @Override
    public String getOutputDirectory() {
        return OUTPUT_DIRECTORY + output_directory;
    }

}
