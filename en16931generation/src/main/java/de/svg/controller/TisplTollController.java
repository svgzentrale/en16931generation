package de.svg.controller;

import de.svg.dto.SvgInvoice;
import de.svg.service.en16931.En16931XmlGenerationService;
import de.svg.service.svginvoice.TisplTollInvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TisplTollController extends AbstractEn16931GenerationController {

    private final TisplTollInvoiceService tisplTollInvoiceService;

    @Value("${output.directory}")
    private String OUTPUT_DIRECTORY;

    public TisplTollController(En16931XmlGenerationService en16931XmlGenerationService, TisplTollInvoiceService tisplTollInvoiceService) {
        super(en16931XmlGenerationService);
        this.tisplTollInvoiceService = tisplTollInvoiceService;
    }

    @GetMapping("tispl/invoice/{id}/en16931")
    @Operation(description = "Get the tispl invoice xml.")
    public ResponseEntity<String> generateInvoiceXml(@PathVariable Long id) {

        SvgInvoice
                invoice = tisplTollInvoiceService.createInvoice(id);
        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @PostMapping("tispl/invoice/{id}/en16931File")
    @Operation(description = "create the xml and save it as a file")
    public ResponseEntity generateInvoiceXmlFile(@PathVariable Long id) {

        SvgInvoice
                collectiveInvoice = tisplTollInvoiceService.createInvoice(id);

        List<SvgInvoice> invoicesToFileprint = new ArrayList<>();
        invoicesToFileprint.add(collectiveInvoice);
        invoicesToFileprint.forEach(this::writeInvoiceXmlToFile);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public String getOutputDirectory() {
        return OUTPUT_DIRECTORY + "/tispl/";
    }

}
