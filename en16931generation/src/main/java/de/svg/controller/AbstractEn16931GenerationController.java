package de.svg.controller;

import de.svg.dto.SvgInvoice;
import de.svg.service.en16931.En16931XmlGenerationService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.xml.bind.JAXBException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RequiredArgsConstructor
public abstract class AbstractEn16931GenerationController {

    @Value("${lobster.output.directory}")
    private String LOBSTER_OUTPUT_DIRECTORY;

    private final En16931XmlGenerationService en16931XmlGenerationService;

    private static final Logger logger = LoggerFactory.getLogger(UlliController.class);

    protected void writeInvoiceXmlToFile(SvgInvoice svgInvoice) {
        String xml = null;
        try {
            xml = en16931XmlGenerationService.doTheXmlThing(svgInvoice);

            //write to file
            String fileName = getOutputDirectory() + svgInvoice.getInvoiceNumber() + "/factur-x.xml";
            writeToFile(xml, fileName );

            if (svgInvoice.isLobsterExport() ) {
                String lobsterFileName = LOBSTER_OUTPUT_DIRECTORY + "/" + svgInvoice.getBuyer().getId() + "-" +svgInvoice.getInvoiceNumber() + ".xml";
                writeToFile(xml, lobsterFileName);
            }

        } catch (IOException | JAXBException e) {
            String msg = "error creating x-rechnung file";
            logger.error(msg, e);
            throw new RuntimeException(msg);
        }

    }

    private void writeToFile(String xml, String completePath) throws IOException{
        Path pathToFile = Paths.get(completePath);
        Files.createDirectories(pathToFile.getParent());
        BufferedWriter writer = Files.newBufferedWriter(pathToFile, StandardCharsets.UTF_8);
        writer.write(xml);
        writer.close();
    }

    protected String getInvoiceXmlString(SvgInvoice svgInvoice) {
        try {
            return en16931XmlGenerationService.doTheXmlThing(svgInvoice);
        } catch (JAXBException e) {
            String msg = "error creating x-rechnung";
            logger.error(msg, e);
            throw new RuntimeException(msg);
        }
    }

    protected abstract String getOutputDirectory();
}
