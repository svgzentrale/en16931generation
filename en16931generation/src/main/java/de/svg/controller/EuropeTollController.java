package de.svg.controller;

import de.svg.dto.InvoiceItem;
import de.svg.dto.SvgInvoice;
import de.svg.service.en16931.En16931XmlGenerationService;
import de.svg.service.svginvoice.EuropeTollInvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class EuropeTollController extends AbstractEn16931GenerationController{

    private final EuropeTollInvoiceService europeTollInvoiceService;

    @Value("${output.directory}")
    private String OUTPUT_DIRECTORY;

    public EuropeTollController(En16931XmlGenerationService en16931XmlGenerationService, EuropeTollInvoiceService europeTollInvoiceService) {
        super(en16931XmlGenerationService);
        this.europeTollInvoiceService = europeTollInvoiceService;
    }

    @GetMapping("europe-toll/invoice/{id}/en16931")
    @Operation(description = "Get europe toll invoices.")
    public ResponseEntity<String> generateInvoiceXml(@PathVariable Long id) {
        SvgInvoice
                invoice = europeTollInvoiceService.createCollectiveInvoice(id);


        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @GetMapping("europe-toll/country-invoice/{id}/en16931")
    @Operation(description = "Get europe toll country invoices.")
    public ResponseEntity<String> generateCountryInvoiceXml(@PathVariable Long id) {
        SvgInvoice
                invoice = europeTollInvoiceService.createCountryInvoice(id);


        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @PostMapping("europe-toll/invoice/{id}/en16931Files")
    @Operation(description = "Get europe toll files.")
    public ResponseEntity generateInvoiceXmlFile(@PathVariable Long id) {
        SvgInvoice
                collectiveInvoice = europeTollInvoiceService.createCollectiveInvoice(id);

        List<SvgInvoice> invoicesToFileprint = collectiveInvoice.getItems()
                .stream()
                .map(InvoiceItem::getItemId)
                .map(europeTollInvoiceService::createCountryInvoice)
                .collect(Collectors.toList());
        invoicesToFileprint.add(collectiveInvoice);
        invoicesToFileprint.forEach(this::writeInvoiceXmlToFile);

        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @Override
    protected String getOutputDirectory() {
        return OUTPUT_DIRECTORY + "/europeToll/";
    }

}
