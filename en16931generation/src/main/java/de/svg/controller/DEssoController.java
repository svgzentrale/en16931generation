package de.svg.controller;

import de.svg.dto.InvoiceItem;
import de.svg.dto.SvgInvoice;
import de.svg.service.en16931.En16931XmlGenerationService;
import de.svg.service.svginvoice.DessoInvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class DEssoController extends AbstractEn16931GenerationController {

    private final DessoInvoiceService dessoInvoiceService;

    @Value("${output.directory}")
    private String OUTPUT_DIRECTORY;

    public DEssoController(En16931XmlGenerationService en16931XmlGenerationService, DessoInvoiceService dessoInvoiceService) {
        super(en16931XmlGenerationService);
        this.dessoInvoiceService = dessoInvoiceService;
    }

    @GetMapping("desso/invoice/{id}/en16931")
    @Operation(description = "create the en16931 xml for the collectice invoice")
    public ResponseEntity<String> createCollectiveInvoice(@PathVariable Long id) {

        SvgInvoice
                invoice = dessoInvoiceService.createCollectiveInvoice(id);

        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @GetMapping("desso/country-invoice/{id}/en16931")
    public ResponseEntity<String> createCountryInvoice(@PathVariable Long id) {
        SvgInvoice
                invoice = dessoInvoiceService.createCountryInvoice(id);

        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @PostMapping("desso/invoice/{id}/en16931Files")
    @Operation(description = "Get shell files.")
    public ResponseEntity generateInvoiceXmlFile(@PathVariable Long id) {

        SvgInvoice
                collectiveInvoice = dessoInvoiceService.createCollectiveInvoice(id);

        List<SvgInvoice> invoicesToFileprint = collectiveInvoice.getItems()
                .stream()
                .map(InvoiceItem::getItemId)
                .map(dessoInvoiceService::createCountryInvoice)
                .collect(Collectors.toList());
        invoicesToFileprint.add(collectiveInvoice);
        invoicesToFileprint.forEach(this::writeInvoiceXmlToFile);
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @Override
    protected String getOutputDirectory() {
        return OUTPUT_DIRECTORY + "/desso/";
    }
}
