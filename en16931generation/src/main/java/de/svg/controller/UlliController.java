package de.svg.controller;

import de.svg.dto.SvgInvoice;
import de.svg.service.svginvoice.BildungInvoiceService;
import de.svg.service.en16931.En16931XmlGenerationService;
import de.svg.service.en16931.PdfA3Service;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
@RequiredArgsConstructor
public class UlliController {

    private final PdfA3Service pdfA3Service;
private final BildungInvoiceService bildungInvoiceService;
private final En16931XmlGenerationService en16931XmlGenerationService;

    private static final Logger logger = LoggerFactory.getLogger(UlliController.class);
    @Operation(description = "Post training attach.")
    @PostMapping("bildung/attach-invoice-xml/{invoiceId}")
    public ResponseEntity<Resource>  attachInvoiceXml(
            @PathVariable Long invoiceId,
            @RequestParam("file") MultipartFile file,
            HttpServletRequest request) throws IOException, JAXBException {

        SvgInvoice invoice = bildungInvoiceService.createInvoice(invoiceId);

        String s = en16931XmlGenerationService.doTheXmlThing(invoice);
        InputStream targetStream = new ByteArrayInputStream(s.getBytes());

        byte[] bytes = pdfA3Service.attachFile(file, targetStream);


        Resource resource = new ByteArrayResource(bytes) ;

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);

    }

    @GetMapping("bildung/invoice/{invoiceId}/en16931")
    @Operation(description = "Get invoice xml.")
    public ResponseEntity<String> generateInvoiceXml(@PathVariable Long invoiceId) {

        SvgInvoice
                invoice = bildungInvoiceService.createInvoice(invoiceId);

        try {
            return ResponseEntity.ok(en16931XmlGenerationService.doTheXmlThing(invoice));
        } catch (JAXBException e) {
            String msg = "error creating x-rechnung";
            logger.error(msg, e);
            throw new RuntimeException(msg);
        }
    }


    @GetMapping("bildung/is-up")
    @Operation(description = "Get training.")
    public ResponseEntity<String> isUp() {
        return ResponseEntity.ok("OK");
    }
}
