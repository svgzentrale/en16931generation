package de.svg.controller;

import de.svg.dto.InvoiceItem;
import de.svg.dto.SvgInvoice;
import de.svg.service.en16931.En16931XmlGenerationService;
import de.svg.service.svginvoice.TotalInvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TotalController extends AbstractEn16931GenerationController{


    private final TotalInvoiceService totalInvoiceService;

    @Value("${output.directory}")
    private String OUTPUT_DIRECTORY;

    public TotalController(En16931XmlGenerationService en16931XmlGenerationService, TotalInvoiceService totalInvoiceService) {
        super(en16931XmlGenerationService);
        this.totalInvoiceService = totalInvoiceService;
    }

    @GetMapping("total/invoice/{id}/en16931")
    @Operation(description = "Get total invoices.")
    public ResponseEntity<String> generateInvoiceXml(@PathVariable Long id) {

        SvgInvoice
            invoice = totalInvoiceService.createCollectiveInvoice(id);
        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @PostMapping("total/invoice/{id}/en16931Files")
    @Operation(description = "Get total files.")
    public ResponseEntity generateInvoiceXmlFile(@PathVariable Long id) {

        SvgInvoice
                collectiveInvoice = totalInvoiceService.createCollectiveInvoice(id);

        List<SvgInvoice> invoicesToFileprint = collectiveInvoice.getItems()
                .stream()
                .map(InvoiceItem::getItemId)
                .map(totalInvoiceService::createCountryInvoice)
                .collect(Collectors.toList());
        invoicesToFileprint.add(collectiveInvoice);
        invoicesToFileprint.forEach(this::writeInvoiceXmlToFile);
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @GetMapping("total/country-invoice/{id}/en16931")
    @Operation(description = "Get total country invoice.")
    public ResponseEntity<String> generateCountryInvoiceXml(@PathVariable Long id) {

        SvgInvoice invoice = totalInvoiceService.createCountryInvoice(id);
        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }


    @Override
    public String getOutputDirectory() {
        return OUTPUT_DIRECTORY + "/total/";
    }

}
