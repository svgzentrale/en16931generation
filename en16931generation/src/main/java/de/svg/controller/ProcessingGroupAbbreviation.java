package de.svg.controller;

import lombok.Getter;

public enum ProcessingGroupAbbreviation {

    tp(1L), dkvee(2L), dkv(3L);

    @Getter
    private final Long id;

    ProcessingGroupAbbreviation(Long id) {
        this.id = id;
    }


}
