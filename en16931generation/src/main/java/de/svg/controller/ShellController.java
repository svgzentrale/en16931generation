package de.svg.controller;

import de.svg.dto.InvoiceItem;
import de.svg.dto.SvgInvoice;
import de.svg.service.en16931.En16931XmlGenerationService;
import de.svg.service.svginvoice.EuroShellInvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ShellController extends AbstractEn16931GenerationController{

    private final EuroShellInvoiceService shellInvoiceService;

    @Value("${output.directory}")
    private String OUTPUT_DIRECTORY;

    public ShellController(En16931XmlGenerationService en16931XmlGenerationService, EuroShellInvoiceService shellInvoiceService) {
        super(en16931XmlGenerationService);
        this.shellInvoiceService = shellInvoiceService;
    }

    @GetMapping("shell/invoice/{id}/en16931")
    @Operation(description = "Get shell invoices.")
    public ResponseEntity<String> generateInvoiceXml(@PathVariable Long id) {

        SvgInvoice
                invoice = shellInvoiceService.createCollectiveInvoice(id);
        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @PostMapping("shell/invoice/{id}/en16931Files")
    @Operation(description = "Get shell files.")
    public ResponseEntity generateInvoiceXmlFile(@PathVariable Long id) {

        SvgInvoice
                collectiveInvoice = shellInvoiceService.createCollectiveInvoice(id);

        List<SvgInvoice> invoicesToFileprint = collectiveInvoice.getItems()
                .stream()
                .map(InvoiceItem::getItemId)
                .map(shellInvoiceService::createCountryInvoice)
                .collect(Collectors.toList());
        invoicesToFileprint.add(collectiveInvoice);
        invoicesToFileprint.forEach(this::writeInvoiceXmlToFile);
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @GetMapping("shell/country-invoice/{id}/en16931")
    @Operation(description = "Get shell country invoices.")
    public ResponseEntity<String> generateCountryInvoiceXml(@PathVariable Long id) {

        SvgInvoice invoice = shellInvoiceService.createCountryInvoice(id);
        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @Override
    protected String getOutputDirectory() {
        return OUTPUT_DIRECTORY + "/shell/";
    }
}
