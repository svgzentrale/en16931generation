package de.svg.controller;

import de.svg.dto.InvoiceItem;
import de.svg.dto.SvgInvoice;
import de.svg.service.en16931.En16931XmlGenerationService;
import de.svg.service.svginvoice.EssoInvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class EssoController extends AbstractEn16931GenerationController {

    private final EssoInvoiceService essoInvoiceService;

    @Value("${output.directory}")
    private String OUTPUT_DIRECTORY;

    public EssoController(En16931XmlGenerationService en16931XmlGenerationService, EssoInvoiceService essoInvoiceService) {
        super(en16931XmlGenerationService);
        this.essoInvoiceService = essoInvoiceService;
    }

    @GetMapping("esso/invoice/{id}/en16931")
    @Operation(description = "create the en16931 xml for the esso invoice")
    public ResponseEntity<String> createCollectiveInvoice(@PathVariable Long id) {

        SvgInvoice
                invoice = essoInvoiceService.createCollectiveInvoice(id);

        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @GetMapping("esso/country-invoice/{id}/en16931")
    public ResponseEntity<String> createCountryInvoice(@PathVariable Long id) {
        SvgInvoice
                invoice = essoInvoiceService.createCountryInvoice(id);

        return ResponseEntity.ok(getInvoiceXmlString(invoice));

    }

    @PostMapping("esso/invoice/{id}/en16931Files")
    @Operation(description = "Get shell files.")
    public ResponseEntity generateInvoiceXmlFile(@PathVariable Long id) {

        SvgInvoice
                collectiveInvoice = essoInvoiceService.createCollectiveInvoice(id);

        List<SvgInvoice> invoicesToFileprint = collectiveInvoice.getItems()
                .stream()
                .map(InvoiceItem::getItemId)
                .map(essoInvoiceService::createCountryInvoice)
                .collect(Collectors.toList());
        invoicesToFileprint.add(collectiveInvoice);
        invoicesToFileprint.forEach(this::writeInvoiceXmlToFile);
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @Override
    protected String getOutputDirectory() {
        return OUTPUT_DIRECTORY + "/esso/";
    }
}
