package de.svg.dto;

public enum RebateType {
    REBATE, SURCHARGE
}
