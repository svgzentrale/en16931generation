package de.svg.dto;

import lombok.Data;

@Data
public class InvoiceContactPerson {

    private String name;

    private String email;

    private String phone;

    private String fax;
}
