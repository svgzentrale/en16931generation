package de.svg.dto;

public enum ItemUnit {

    LITER, PIECE
}
