package de.svg.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class VatCorrection {

    private Integer vatRate;

    private BigDecimal correctionGrossAmount;

    private BigDecimal correctionVatAmount;
}
