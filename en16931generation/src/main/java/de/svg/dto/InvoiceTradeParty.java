package de.svg.dto;

import lombok.Data;

import java.util.List;

@Data
public class InvoiceTradeParty {

    private String id;

    private String name;

    private String description;

    private String vatNumber;

    private String fiscalNumber;

    private String street;

    private String city;

    private String zipCode;

    private String phoneNumber;

    private String fax;

    private String eMail;

    private String isoCountryCode;

    /** Pflichtfeld bei Verkäufer, also der SVG*/
    private InvoiceContactPerson contact;

}
