package de.svg.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RebateOrExtraCharge {

    private BigDecimal value;

    private BigDecimal basisQunatity;

    private Boolean valueIsPercentValue;

    private String reason;

    private RebateType type;
}
