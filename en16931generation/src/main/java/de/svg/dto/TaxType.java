package de.svg.dto;

import lombok.Getter;

public enum TaxType {

//     S = Umsatzsteuer fällt mit Normalsatz an
//     *  Z = nach dem Nullsatz zu versteuernde Waren
//     *  E = Steuerbefreit
//     *  AE = Umkehrung der Steuerschuldnerschaft
//     *  K = Kein Ausweis der Umsatzsteuer bei innergemeinschaftlichen Lieferungen
//     *  G = Steuer nicht erhoben aufgrund von Export außerhalb der EU
//     *  O = Außerhalb des Steueranwendungsbereichs

    NORMAL("S"), NULLRATE("Z"), EXEMPT("E"), UNCONSIDERED("O");

    @Getter
    private final String taxTypeCode;

    TaxType(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }
}
