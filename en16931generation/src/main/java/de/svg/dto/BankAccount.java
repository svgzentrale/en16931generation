package de.svg.dto;

import lombok.Data;
import lombok.ToString;

@Data
public class BankAccount {

    private String iban;

    private String bic;

    private String accountName;

    private String bankName;

    @Override
    public String toString() {
        return String.format("Bank: %5s  IBAN: %5s BIC: %5s", bankName.trim(), iban, bic);
    }
}
