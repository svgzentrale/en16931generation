package de.svg.dto;

public enum InvoiceType {

    COUNTRY_INVOICE, COLLECTIVE_INVOICE;
}
