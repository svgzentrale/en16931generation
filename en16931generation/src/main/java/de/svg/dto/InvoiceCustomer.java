package de.svg.dto;

import lombok.Data;

@Data
public class InvoiceCustomer extends InvoiceTradeParty {

    /** Leitweg ID*/
    private String globalReferenceNumber;
}
