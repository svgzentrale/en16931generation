package de.svg.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PaymentTerms {

    private BankAccount sellerAccount;

    private PaymentType type;

    /** SEPA Gläubiger Id*/
    private String sepaId;

    /** SEPA Mandat*/
    private String sepaMandat;

    private BankAccount buyerAccount;

    /** Fälligkeitsdatum */
    private LocalDate dueDate;

    /** Freitext für Angaben der Zahlungsmodalitäten*/
    private String description;

    private boolean paymentAlreadyReceived = false;

}
