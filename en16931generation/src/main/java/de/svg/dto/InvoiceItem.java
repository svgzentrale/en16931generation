package de.svg.dto;

import de.svg.api.en16931.TaxTypeCodeType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class InvoiceItem {

    private Long itemId;

    private String itemText;

    private String description;

    /** attributes that are describing the item can be paced in here, e.g the license plate. */
    private Map<String, String> attributes;

    private BigDecimal quantity = BigDecimal.ONE;

    private ItemUnit unit;

    private BigDecimal totalNetPrice;

    private BigDecimal totalGrossPrice;

    private BigDecimal unitNetPrice;

    private BigDecimal unitGrossPrice;

    private BigDecimal percentage;

    private Integer vatRate = 0;

    private BigDecimal vatAmount = BigDecimal.ZERO;

    private RebateOrExtraCharge rebate;

    private RebateOrExtraCharge surcharge;

    private TaxType taxType = TaxType.NORMAL;

}
