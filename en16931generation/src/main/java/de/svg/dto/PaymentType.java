package de.svg.dto;

public enum PaymentType {

    /** Überweiser: Kundenart 2 */
    BANK_TRANSFER,

    /** Abbucher SVGZ, AF-Abbucher: Kundenart 1,5 */
    SEPA,

    /** LS, Abbucher SVG: Kundenart 3,4 */
    DIRECT_DEBIT

}
