package de.svg.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class SvgInvoice {

    private String invoiceNumber;

    private LocalDate invoiceDate;

    private List<String> comments;

    private List<InvoiceItem> items;

    private List<VatCorrection> vatCorrections;

    private InvoiceCustomer buyer;

    private InvoiceTradeParty seller;

    private PaymentTerms paymentTerms;

    private InvoiceType invoiceType;

    private String currencyCode = "EUR";

    private boolean lobsterExport = false;
}
