package de.svg.config;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

import java.util.HashMap;
import java.util.Map;

public class En16931NamespacePrefixMapper extends NamespacePrefixMapper {

    private Map<String, String> namespaceMap = new HashMap<>();

    /**
     * Create mappings.
     */
    public En16931NamespacePrefixMapper() {
        namespaceMap.put("urn:un:unece:uncefact:data:standard:CrossIndustryInvoice:100", "rsm");
        namespaceMap.put("urn:un:unece:uncefact:data:standard:QualifiedDataType:100", "qdt");
        namespaceMap.put("urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100", "ram");
        namespaceMap.put("urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100", "udt");
    }

    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean b) {
        return namespaceMap.getOrDefault(namespaceUri, suggestion);
    }
}
