package de.svg.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
	entityManagerFactoryRef = "bildungEntityManagerFactory",
	transactionManagerRef = "bildungTransactionManager",
	basePackages = { "de.svg.jpa.bildung" }
)
public class BildungDatabaseConfig {

	@Bean(name = "bildungDataSourceProperties")
	@ConfigurationProperties("bildung.datasource")
	public DataSourceProperties piaDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean(name = "bildungDataSource")
	@ConfigurationProperties(prefix = "bildung.datasource.tomcat")
	public DataSource piaDataSource(@Qualifier("bildungDataSourceProperties") DataSourceProperties bildungDataSourceProperties) {
		return bildungDataSourceProperties.initializeDataSourceBuilder().build();
	}

	@Bean(name = "bildungEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean bildungEntityManagerFactory(
		EntityManagerFactoryBuilder builder,
		@Qualifier("bildungDataSource") DataSource bildungDataSource
	) {
		LocalContainerEntityManagerFactoryBean em = builder
			.dataSource(bildungDataSource)
			.packages("de.svg.jpa.bildung")
			.persistenceUnit("bildungPu")
			.build();
		em.setJpaProperties(additionalJpaProperties());
		return em;
	}

	@Bean(name = "bildungTransactionManager")
	public PlatformTransactionManager bildungTransactionManager(
		@Qualifier("bildungEntityManagerFactory") EntityManagerFactory bildungEntityManagerFactory
	) {
		return new JpaTransactionManager(bildungEntityManagerFactory);
	}

	Properties additionalJpaProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");
		properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");

		return properties;
	}
}
