package de.svg.jpa.bildung.dao;

import de.svg.jpa.bildung.model.BankingAccountBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BankingAccountRepository extends JpaRepository<BankingAccountBo, Long> {

    Optional<BankingAccountBo> findBySvgNumberAndCustomerNumber(String svgNumber, String customerNumber);

}
