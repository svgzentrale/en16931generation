package de.svg.jpa.bildung.dao;

import de.svg.jpa.bildung.model.ZugferdCustomerBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ZugferdCustomerRepository extends JpaRepository <ZugferdCustomerBo, Long> {

    Optional<ZugferdCustomerBo> findByCustomerNumberAndFiburaId(String customerNumber, Long fiburaId);
}
