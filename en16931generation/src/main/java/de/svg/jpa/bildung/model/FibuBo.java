package de.svg.jpa.bildung.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "[JRechnungSVG]", name = "tblFibu")
@Data
public class FibuBo {

    @Id
    @Column(name = "PK")
    private Long id;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "ustID")
    private String vatNumber;

    @Column(name = "steuernummer")
    private String taxNumber;

    @Column(name = "IBAN")
    private String iban;

    @Column(name = "FibuBezeichnung")
    private String fibuName;

    @Column(name = "strasse")
    private String street;

    @Column(name = "plz")
    private String zipCode;

    @Column(name = "ort")
    private String city;

    @Column(name = "apEmail")
    private String contactMail;

    @Column(name = "telefon")
    private String contactPhone;

    @Column(name = "fax")
    private String contactFax;

    @Column(name = "ap")
    private String contactName;
}
