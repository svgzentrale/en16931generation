package de.svg.jpa.bildung.dao;

import de.svg.jpa.bildung.model.BildungInvoiceBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BildungInvoiceRepository extends JpaRepository<BildungInvoiceBo, Long> {

    Optional<BildungInvoiceBo> findByInvoiceNumber(String invoiceNumber);
}
