package de.svg.jpa.bildung.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "[JRechnungSVG]", name = "tblZahlungsBedingung")
@Data
public class PaymentTermsBo {

    @Id
    @Column(name = "PK")
    private Long id;

    @Column(name = "ZahlungsBedingung")
    private String terms;
}
