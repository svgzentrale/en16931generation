package de.svg.jpa.bildung.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "[JRechnungSVG]", name = "tblBankverbindung")
@Data
public class BankingAccountBo {

    @Id
    @Column(name = "Id")
    private Long id;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "KundenNr")
    private String customerNumber;

    @Column(name = "IBAN")
    private String iban;

    @Column(name = "BIC")
    private String bic;

    @Column(name = "Bank")
    private String bankName;

}
