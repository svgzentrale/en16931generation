package de.svg.jpa.bildung.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(catalog = "[JRechnungSVG]", name = "tblRechnungDetail")
@Data
public class BildungInvoiceItemBo {


    @Id
    @Column(name = "PK")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "fkRechnungKopf")
    @ToString.Exclude
    private BildungInvoiceBo invoiceHeader;

    @Column(name = "RgPosition")
    private Integer itemPosition;

    @Column(name = "Bezeichnung")
    private String description;

    @Column(name = "Menge")
    private BigDecimal quantity;

    @Column(name = "BetragEuro")
    private BigDecimal totalNetPrice;

    @Column(name = "mwstBetrag")
    private BigDecimal vatAmount;

    @Column(name = "MWST")
    private Integer vatRate;

    @Column(name = "Einzelpreis")
    private BigDecimal unitNetPrice;



}
