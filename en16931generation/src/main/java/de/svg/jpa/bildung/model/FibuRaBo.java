package de.svg.jpa.bildung.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "[JRechnungSVG]", name = "tblFibuRa")
@Data
public class FibuRaBo {

    @Id
    @Column(name="PK")
    private Long id;

    @ManyToOne
    @JoinColumn(name="fkFiBu", referencedColumnName = "PK", insertable=false, updatable=false )
    private FibuBo fibu;
}
