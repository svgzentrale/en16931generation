package de.svg.jpa.bildung.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(catalog = "[JRechnungSVG]", name = "tblRechnungKopf")
@Data
public class BildungInvoiceBo {

    @Id
    @Column(name = "PK")
    private Long id;

    @Column(name = "RechnungsNr")
    private String invoiceNumber;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "KundenNr")
    private String customerNumber;

    @Column(name = "UstIdentNr")
    private String vatNumber;

    @Column(name = "Rechnungstext")
    private String additionalText;

    @Column(name = "Anschrift")
    private String buyersCompleteAddress;

    @Column(name = "RechnungDatum")
    private LocalDate invoiceDate;

    @Column(name = "[Fälligkeit]")
    private LocalDate dueDate;

    @Column(name = "Netto")
    private BigDecimal totalNetAmount;

    @Column(name = "Brutto")
    private BigDecimal totalGrossAmount;

    @OneToMany(mappedBy = "invoiceHeader")
    private List<BildungInvoiceItemBo> items;

    @Column(name = "fkFibuRa")
    private Long fiburaId;

    @ManyToOne
    @JoinColumn(name = "TextAbbucherBarzahler", insertable = false, updatable = false)
    private PaymentTermsBo paymentTerms;
}
