package de.svg.jpa.bildung.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "[JRechnungSVG]", name = "tblZugFerd")
@Data
public class ZugferdCustomerBo {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "kundenNr")
    String customerNumber;

    @Column(name = "fiburaID")
    private Long fiburaId;

    @Column(name = "firma")
    private String customerName;

    @Column(name = "strasse")
    private String street;

    @Column(name = "plz")
    private String zipCode;

    @Column(name = "ort")
    private String city;

    /** Leitweg ID*/
    @Column(name = "leitwegID")
    private String globalReferenceNumber;


}
