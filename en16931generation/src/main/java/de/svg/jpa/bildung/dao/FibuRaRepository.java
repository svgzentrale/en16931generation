package de.svg.jpa.bildung.dao;

import de.svg.jpa.bildung.model.FibuRaBo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FibuRaRepository  extends JpaRepository<FibuRaBo, Long> {
}
