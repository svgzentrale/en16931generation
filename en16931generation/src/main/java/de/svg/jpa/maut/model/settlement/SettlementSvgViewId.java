package de.svg.jpa.maut.model.settlement;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class SettlementSvgViewId implements Serializable {

    @Column(name = "FK_VerarbeitungsGruppe" )
    private Long processingGroupId;

    @Column(name = "SVG")
    private String svgNumber;
}
