package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.AustriaInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AustriaInvoiceItemRepository extends JpaRepository<AustriaInvoiceItemView, Long>, CountryInvoiceItemRepo<AustriaInvoiceItemView> {

}
