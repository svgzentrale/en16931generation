package de.svg.jpa.maut.model.europetoll;

import de.svg.jpa.maut.model.AbstractItemPositionTextBo;

import javax.persistence.*;

@Entity
@Table(catalog = "EuropaMautZ", name = "tblPositionsTexte")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "description", column = @Column(name = "Bezeichnung"))
})
public class EuropeTollItemPositionTextBo extends AbstractItemPositionTextBo {

}
