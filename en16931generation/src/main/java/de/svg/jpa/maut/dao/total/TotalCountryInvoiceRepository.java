package de.svg.jpa.maut.dao.total;

import de.svg.jpa.maut.model.total.TotalCountryInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TotalCountryInvoiceRepository extends JpaRepository<TotalCountryInvoiceView, Long> {

    Optional<TotalCountryInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}
