package de.svg.jpa.maut.model.total;

import de.svg.dto.InvoiceContactPerson;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.service.svginvoice.InvoiceServiceUtil;
import lombok.Data;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Entity
@Table(catalog = "TotalCardSvg", name = "tblOrganisationen")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "svgNumber", column = @Column(name = "SVG")),
        @AttributeOverride(name = "addressLine1", column = @Column(name = "AdZeile1")),
        @AttributeOverride(name = "addressLine2", column = @Column(name = "AdZeile2")),
        @AttributeOverride(name = "addressLine3", column = @Column(name = "AdStrasse")),
        @AttributeOverride(name = "addressLine4", column = @Column(name = "AdPLZ")),
        @AttributeOverride(name = "addressLine5", column = @Column(name = "AdOrt")),
        @AttributeOverride(name = "completeSendAdress", column = @Column(name = "AbsenderAdresse")),
        @AttributeOverride(name = "taxNumber", column = @Column(name = "SteuerNr")),
        @AttributeOverride(name = "contact", column = @Column(name = "Ansprechpartner")),
        @AttributeOverride(name = "bankAccount", column = @Column(name = "Bankverbindung"))
})
@Data
public class TotalSvgBo extends AbstractSvgBo {

    @Column(name = "Telefon")
    private String secContactPhone;

    @Column(name = "mail")
    private String secContactMail;

    @Column(name = "Ust_ID_Nr")
    private String vatNumber;

    @Override
    public String getZipCode() {
        return getAddressLine4();
    }

    @Override
    public String getCity() {
        return getAddressLine5();
    }

    @Override
    public String getStreet() {
        return getAddressLine3();
    }

    @Override
    public String getName() {
        return getAddressLine1() + " " + getAddressLine2();
    }

    @Override
    public InvoiceContactPerson getContactInformation() {
        return InvoiceServiceUtil.extractContactInformationFromCompleteString(
                getContact(),
                getName(),
                getSecContactMail(),
                getSecContactPhone());
    }

}
