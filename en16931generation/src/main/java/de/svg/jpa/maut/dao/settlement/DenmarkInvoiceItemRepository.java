package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.DenmarkInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DenmarkInvoiceItemRepository extends JpaRepository<DenmarkInvoiceItemView, Long>, CountryInvoiceItemRepo<DenmarkInvoiceItemView> {

}
