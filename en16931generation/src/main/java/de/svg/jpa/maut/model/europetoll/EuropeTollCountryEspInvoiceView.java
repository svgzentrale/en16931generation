package de.svg.jpa.maut.model.europetoll;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(catalog = "EuropaMautZ", name = "VW_RechnungsDruckKopfGrundlageESP")
@Data
public class EuropeTollCountryEspInvoiceView {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "KundenId")
    private Long customerId;

    @Column(name = "RechnungsNr")
    private String invoiceNumber;

    @Column(name = "RechDatum")
    private LocalDate invoiceDate;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "Währung")
    private String currency;



}
