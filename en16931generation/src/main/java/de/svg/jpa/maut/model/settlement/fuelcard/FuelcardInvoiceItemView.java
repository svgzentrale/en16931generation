package de.svg.jpa.maut.model.settlement.fuelcard;

import de.svg.jpa.maut.model.settlement.CountryInvoiceView;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(catalog = "Abrechnung", name = "[VW_TSK_rpt_Rg_Pos]")
@Data
public class FuelcardInvoiceItemView {

    @Id
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn( name="FK_Rg_land", insertable=false, updatable=false )
    private CountryInvoiceView countryInvoice;

    @Column(name = "LieferDatum")
    private LocalDate deliveryDate;

    @Column(name = "LieferScheinNummer")
    private String deliveryNumber;

    @Column(name = "KM_Stand")
    private Integer mileage;

    @Column(name = "TankstellenNr")
    private String gasStationNumber;

    @Column(name = "WarenCode_in_GF")
    private String goodsCode;

    @Column(name = "CodeText")
    private String goodsCodeText;

    @Column(name = "KfzKz")
    private String licensePlate;

    @Column(name = "PAN")
    private String pan;

    @Column(name = "Menge")
    private BigDecimal quantity;

    @Column(name = "Mengen_Einheit")
    private String unit;

    @Column(name = "NettoRgBetrag")
    private BigDecimal totalNetPrice;

    @Column(name = "BruttoRgBetrag")
    private BigDecimal totalGrossPrice;

    @Column(name = "EinzelPreisNetto")
    private BigDecimal unitNetPrice;


    @Column(name = "EinzelPreisBrutto")
    private BigDecimal unitGrossPrice;

    @Column(name = "USt")
    private Integer vatRate;

    @Column(name = "UstBetrag")
    private BigDecimal vatAmount;


}
