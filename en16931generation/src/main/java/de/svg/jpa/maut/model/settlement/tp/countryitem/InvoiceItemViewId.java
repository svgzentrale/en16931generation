package de.svg.jpa.maut.model.settlement.tp.countryitem;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class InvoiceItemViewId implements Serializable {

    private Long countryInvoiceId;

    private String itemText;

    private BigDecimal netAmount;

    private BigDecimal vatAmount;

    private Integer vatRate;

    private BigDecimal grossAmount;
}
