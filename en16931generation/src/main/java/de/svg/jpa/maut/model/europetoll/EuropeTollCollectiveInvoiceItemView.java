package de.svg.jpa.maut.model.europetoll;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(catalog = "EuropaMautZ", name = "[VW_RechnungsDruckDeckblatt]")
@Data
public class EuropeTollCollectiveInvoiceItemView {
    @Id
    @Column(name = "RechnungsNr")
    private Long invoiceNumber;

    @Column(name = "Pos")
    private Integer position;

    @Column(name = "Land")
    private String country;

    @Column(name = "SummeGesWertBrutto")
    private BigDecimal totalGrossPrice;

    @Column(name = "GesWertNetto")
    private BigDecimal totalNetPrice;

    @Column(name = "UStBetrag")
    private BigDecimal vatAmount;

}
