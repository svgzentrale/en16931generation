package de.svg.jpa.maut.model.settlement;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "Abrechnung", name = "tblNST_MwStSaetze")
@Data
public class VatRateBo {

    @Id
    @Column(name = "Id")
    Long id;

    @Column(name = "MwStProz")
    Integer vatRate;
}
