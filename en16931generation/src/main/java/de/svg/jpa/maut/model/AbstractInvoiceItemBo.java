package de.svg.jpa.maut.model;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;
import java.time.LocalDate;

@MappedSuperclass
@Data
public abstract class AbstractInvoiceItemBo {

    public abstract AbstractItemPositionTextBo getItemPositionText();

    public abstract AbstractFuelcardBo getFuelcard();

    @Id
    private Long id;

    private LocalDate deliveryDate;

    private String stationName;

    private String transactionNumber;

    private Integer mileage;

    private String unit;

    private BigDecimal quantity;

    private BigDecimal unitNetPrice;

    private BigDecimal unitGrossPrice;

    private BigDecimal totalNetPrice;

    private BigDecimal totalGrossPrice;

    private BigDecimal vatAmount;

    private Integer vatRate;

    private BigDecimal surcharge;

    private String surchargeIndicator;

    private BigDecimal rebate;

    private String rebateIndicator;

    public boolean isRebatePercentual() {
        return "%".equals(rebateIndicator);
    }

    public boolean isSurchargePercentual() {
        return "%".equals(surchargeIndicator);
    }
}
