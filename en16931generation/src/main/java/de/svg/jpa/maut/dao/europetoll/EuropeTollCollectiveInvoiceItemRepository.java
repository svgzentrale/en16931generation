package de.svg.jpa.maut.dao.europetoll;

import de.svg.jpa.maut.model.europetoll.EuropeTollCollectiveInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EuropeTollCollectiveInvoiceItemRepository extends JpaRepository<EuropeTollCollectiveInvoiceItemView, Long> {

    List<EuropeTollCollectiveInvoiceItemView> findByInvoiceNumberIn(List<Long> invoiceNumbers);
}
