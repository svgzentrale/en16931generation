package de.svg.jpa.maut.model.tispl;

import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(catalog = "TISPLZ", name = "tblRechnungsKpf")
@Getter
public class TisplTollInvoiceHeaderBo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "RechnungsNr")
    private Long invoiceNumber;

    @Column(name = "RechDatum")
    private LocalDate invoiceDate;

    @ManyToOne
    @JoinColumn( name="KundenId", insertable=false, updatable=false )
    private CustomerBo customer;

    @ManyToOne
    @JoinColumn(name="KundenId", referencedColumnName = "KundenId", insertable=false, updatable=false )
    private InvoiceAddressBo address;

    @OneToMany(mappedBy = "svgInvoice")
    private List<TisplTollInvoiceItemSvgBo> svgInvoiceItems;

    @OneToMany(mappedBy = "etInvoice")
    private List<TisplTollInvoiceItemEtBo> etInvoiceItems;
}
