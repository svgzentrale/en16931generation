package de.svg.jpa.maut.model.desso;


import de.svg.jpa.maut.model.FuelcardCollectiveItemView;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(catalog = "DEssoPurZ", name = "[VW_RechnungsDruckDeckblatt]")
@Data
public class DessoCollectiveInvoiceItemView implements FuelcardCollectiveItemView {

    @Id
    @Column(name = "RechnungsNr")
    private String itemNumber;

    @Column(name = "RgNrKpf")
    private Long invoiceNumber;

    @Column(name = "Pos")
    private Integer itemPosition;

    @Column(name = "LandbezeichnungD")
    private String country;

    @Column(name = "SummeGesWertBrutto")
    private BigDecimal totalGrossPriceCurrency;

    @Column(name = "RgBetragEUR")
    private BigDecimal totalGrossPriceEuro;

    @Column(name = "Währung")
    private String currency;
}
