package de.svg.jpa.maut.model;

import java.time.LocalDate;

public interface FuelcardCollectiveInvoiceView {

    Long getId();

    Long getCustomerId();

    Long getInvoiceNumber();

    LocalDate getInvoiceDate();

    String getSvgNumber();

    Integer getCustomerType();

    String getDebitComment();

    String getAdditionalMessage();
}
