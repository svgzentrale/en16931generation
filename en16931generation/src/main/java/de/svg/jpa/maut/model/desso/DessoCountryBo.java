package de.svg.jpa.maut.model.desso;

import de.svg.jpa.maut.model.AbstractCountryBo;

import javax.persistence.*;

@Entity
@Table(catalog = "DessoPurZ", name = "tblLand")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
        @AttributeOverride(name = "countryNameGerman", column = @Column(name = "LandBezeichnungD")),
})
public class DessoCountryBo extends AbstractCountryBo {

}
