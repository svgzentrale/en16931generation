package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.ItalyInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItalyInvoiceItemRepository extends JpaRepository<ItalyInvoiceItemView, Long>, CountryInvoiceItemRepo<ItalyInvoiceItemView> {

}
