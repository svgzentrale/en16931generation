package de.svg.jpa.maut.dao.total;

import de.svg.jpa.maut.model.total.TotalCountryInvoiceItemBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TotalCountryInvoiceItemRepository extends JpaRepository<TotalCountryInvoiceItemBo, Long> {
    List<TotalCountryInvoiceItemBo> findByCountryInvoiceId(Long id);
}
