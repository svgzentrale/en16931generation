package de.svg.jpa.maut.model.settlement;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Data
public class VatCorrectionViewId implements Serializable {

    @Column(name = "FK_Rg_Land")
    private Long countryInvoiceId;

    @ManyToOne
    @JoinColumn( name="MwStKz", insertable=false, updatable=false )
    private VatRateBo vatRate;

}
