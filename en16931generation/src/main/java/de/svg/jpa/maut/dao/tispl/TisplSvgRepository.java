package de.svg.jpa.maut.dao.tispl;

import de.svg.jpa.maut.dao.BaseSvgRepository;
import de.svg.jpa.maut.model.tispl.TisplSvgBo;

public interface TisplSvgRepository extends BaseSvgRepository<TisplSvgBo, Long> {
}
