package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.GermanyInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GermanyInvoiceItemRepository extends JpaRepository<GermanyInvoiceItemView, Long>, CountryInvoiceItemRepo<GermanyInvoiceItemView> {

}
