package de.svg.jpa.maut.model.europetoll;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "EuropaMautZ", name = "tblRechnungLand")
@Data
public class EuropeTollCountryInvoiceBo  {


    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "fkRgKpfId")
    private Long collectiveInvoiceId;

    @Column(name = "RechnungsNr")
    private Long countryInvoiceNumber;

}
