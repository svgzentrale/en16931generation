package de.svg.jpa.maut.model.settlement;

import de.svg.jpa.maut.model.AbstractCountryBo;

import javax.persistence.*;

@Entity
@Table(catalog = "Abrechnung", name = "tblNST_Laender")
public class SettlementCountryBo extends AbstractCountryBo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "Name")
    private String country;
}
