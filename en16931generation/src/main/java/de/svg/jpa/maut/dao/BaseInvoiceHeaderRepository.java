package de.svg.jpa.maut.dao;

import de.svg.jpa.maut.model.AbstractInvoiceHeaderBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.Optional;

@NoRepositoryBean
public interface BaseInvoiceHeaderRepository <T extends AbstractInvoiceHeaderBo, ID extends Serializable> extends JpaRepository<T, ID> {

    Optional<T> findByInvoiceNumber(Long invoiceNumber);
}
