package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.PortugalInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortugalInvoiceItemRepository extends JpaRepository<PortugalInvoiceItemView, Long>, CountryInvoiceItemRepo<PortugalInvoiceItemView> {
}
