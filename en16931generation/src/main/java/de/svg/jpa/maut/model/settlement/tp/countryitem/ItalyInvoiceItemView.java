package de.svg.jpa.maut.model.settlement.tp.countryitem;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(catalog = "Abrechnung", name = "VW_TP_urptItalienRechnung")
@Data
@IdClass(InvoiceItemViewId.class)
public class ItalyInvoiceItemView implements TpCountryInvoiceItem {

    @Id
    @Column(name = "FK_Rg_Land")
    private Long countryInvoiceId;

    @Id
    @Column(name = "Consumi")
    private String itemText;

    @Id
    @Column(name = "Imponibile")
    private BigDecimal netAmount;

    @Id
    @Column(name = "IVA")
    private BigDecimal vatAmount;


    @Id
    @Column(name = "VAT_Rate")
    private Integer vatRate;

    @Id
    @Column(name = "Importo")
    private BigDecimal grossAmount;

    @Override
    public String getItemDescription() {
        return null;     //TODO
    }


    @Column(name = "VATdescription")
    private String vatDescription;

    @Override
    public Map<String, String> getAdditionalAttributes() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy");
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("Referenzgesetz", getVatDescription());
        return attributesMap;
    }

}
