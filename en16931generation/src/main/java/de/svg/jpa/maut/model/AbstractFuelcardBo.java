package de.svg.jpa.maut.model;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
public abstract class AbstractFuelcardBo {

    @Id
    private Long id;

    private String pan;

    private String licensePlate;

    public abstract String getCostcenter();

}
