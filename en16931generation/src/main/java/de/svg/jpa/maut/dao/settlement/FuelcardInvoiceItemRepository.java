package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.fuelcard.FuelcardInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FuelcardInvoiceItemRepository extends JpaRepository<FuelcardInvoiceItemView, Long> {

    List<FuelcardInvoiceItemView> findByCountryInvoiceId(Long countryInvoiceId);
}
