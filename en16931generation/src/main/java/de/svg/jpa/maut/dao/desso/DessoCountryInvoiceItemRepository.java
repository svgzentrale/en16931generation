package de.svg.jpa.maut.dao.desso;

import de.svg.jpa.maut.model.desso.DessoInvoiceItemBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DessoCountryInvoiceItemRepository extends JpaRepository<DessoInvoiceItemBo, Long> {
    List<DessoInvoiceItemBo> findByCountryInvoiceId(Long id);
}
