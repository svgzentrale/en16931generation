package de.svg.jpa.maut.dao.esso;

import de.svg.jpa.maut.model.esso.EssoInvoiceItemBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EssoCountryInvoiceItemRepository extends JpaRepository<EssoInvoiceItemBo, Long> {
    List<EssoInvoiceItemBo> findByCountryInvoiceId(Long id);
}
