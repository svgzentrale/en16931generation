package de.svg.jpa.maut.model.desso;

import de.svg.jpa.maut.model.FuelcardCountryInvoiceView;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(catalog = "DEssoPurZ", name = "VW_RechnungsDruckKopfGrundlage")
@Data
public class DessoCountryInvoiceView implements FuelcardCountryInvoiceView {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "RechnungsNr")
    private Long invoiceNumber;

    @Column(name = "KundenId")
    private Long customerId;

    @Column(name = "RechDatum")
    private LocalDate invoiceDate;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "TextRechnungAbrechnung")
    private String invoiceText;

    @Column(name = "TextLandbezeichnung")
    private String countryName;

    @Column(name = "LandbezeichnungD")
    private String countryNameGerman;

    @Column(name = "TextZusatzunterAnschrift")
    private String footnote;

    @Column(name = "UstIdNrSVGZ")
    private String vatNumberSvg;

    @Column(name = "[Währung]")
    private String currencyCode;
}
