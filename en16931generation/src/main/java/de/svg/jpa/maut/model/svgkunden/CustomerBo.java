package de.svg.jpa.maut.model.svgkunden;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(catalog = "SVG_Kunden", name = "tblKundenStamm")
@Data
public class CustomerBo {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "KundenNr")
    private String customerNumber;

    /** Leitweg ID*/
    @Column(name = "LeitwegID")
    private String globalReferenceNumber;

    @OneToMany(mappedBy = "customer")
    private List<TaxNumberBo> taxNumbers;


    public String getGlobalCustomerNumber() {
        return svgNumber + customerNumber;
    }
}
