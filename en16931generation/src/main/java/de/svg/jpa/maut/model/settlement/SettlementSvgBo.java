package de.svg.jpa.maut.model.settlement;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "Abrechnung", name = "tblNST_SVG")
@Data
public class SettlementSvgBo {

    @Id
    @Column(name = "Id")
    private Long id;

    @Column(name = "IBAN")
    private String iban;

    @Column(name = "BIC")
    private String bic;
}
