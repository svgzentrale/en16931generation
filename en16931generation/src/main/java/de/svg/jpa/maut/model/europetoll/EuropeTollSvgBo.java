package de.svg.jpa.maut.model.europetoll;

import de.svg.dto.InvoiceContactPerson;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.service.svginvoice.InvoiceServiceUtil;
import lombok.Data;

import javax.persistence.*;

//TODO oder tblWinSys
@Entity
@Table(catalog = "EuropaMautSVG", name = "tblWinSys")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "svgNumber", column = @Column(name = "SVG_Nr")),
        @AttributeOverride(name = "addressLine1", column = @Column(name = "SVG_Adresse1")),
        @AttributeOverride(name = "addressLine2", column = @Column(name = "SVG_Adresse2")),
        @AttributeOverride(name = "addressLine3", column = @Column(name = "SVG_Adresse3")),
        @AttributeOverride(name = "addressLine4", column = @Column(name = "SVG_Adresse4")),
        @AttributeOverride(name = "addressLine5", column = @Column(name = "SVG_Adresse5")),
        @AttributeOverride(name = "completeSendAdress", column = @Column(name = "Absender")),
        @AttributeOverride(name = "taxNumber", column = @Column(name = "SteuerNr")),
        @AttributeOverride(name = "contact", column = @Column(name = "Ansprechpartner")),
        @AttributeOverride(name = "bankAccount", column = @Column(name = "Bankverbindung"))
})
@Data
public class EuropeTollSvgBo extends AbstractSvgBo {

    @Column(name = "MailAbsender")
    private String secContactMail;

    @Column(name = "Ust_ID_Nr")
    private String vatNumber;

    @Override
    public String getZipCode() {
        return getAddressLine4();
    }

    @Override
    public String getCity()  {
        return getAddressLine5();
    }

    @Override
    public String getStreet() {
        return getAddressLine3();
    }

    @Override
    public String getName() {
        return getAddressLine1() + " " + getAddressLine2();
    }

    @Override
    public InvoiceContactPerson getContactInformation() {
        return InvoiceServiceUtil.extractContactInformationFromCompleteString(
                getContact(),
                getName(),
                getSecContactMail(),
                "n/a");
    }


}
