package de.svg.jpa.maut.model.svgkunden;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "SVG_Kunden", name = "tblBV")
@Where(clause = "inUse=1 and BVMandatStatus=2")
@Data
public class BankAccountBo {

    public static final Integer TYPE_TOLL  = 1;
    public static final Integer TYPE_ESSO_AND_TOTAL  = 2;
    public static final Integer TYPE_SHELL  = 4;
    public static final Integer TYPE_VIALTIS  = 8;
    public static final Integer TYPE_SVGBOX  = 16;
    public static final Integer TYPE_TELEPASS  = 32;
    public static final Integer TYPE_DKV  = 128;

    @Id
    @Column(name = "BVID")
    private Long id;

    @Column(name = "BVKundenID")
    private Long customerId;

    @Column(name = "BVKontoinhaber")
    private String accountName;

    @Column(name = "BVBIC")
    private String bic;

    @Column(name = "BVIBAN")
    private String iban;

    @Column(name = "BVMandatNr")
    private String sepaMandatNumber;

    /** tblBankverbindungArten */
    @Column(name = "BVTyp")
    private Integer type;
}
