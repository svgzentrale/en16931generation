package de.svg.jpa.maut.model;

import java.time.LocalDate;

public interface FuelcardCountryInvoiceView {

    Long getId();

    Long getInvoiceNumber();

    Long getCustomerId();

    LocalDate getInvoiceDate();

    String getSvgNumber();

    String getInvoiceText();

    String getCountryName();

    String getFootnote();

    String getVatNumberSvg();

    String getCurrencyCode();

}
