package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.CollecticeInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SettlementTollCollecticeInvoiceViewRepository extends JpaRepository<CollecticeInvoiceView, Long> {
    Optional<CollecticeInvoiceView> findByInvoiceNumberAndProcessingGroupId(Long invoiceNumber, Long processingGroupId);
}
