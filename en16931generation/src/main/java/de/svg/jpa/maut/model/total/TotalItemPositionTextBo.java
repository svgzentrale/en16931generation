package de.svg.jpa.maut.model.total;

import de.svg.jpa.maut.model.AbstractItemPositionTextBo;

import javax.persistence.*;

@Entity
@Table(catalog = "TotalCardZ", name = "tblPosTexte")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
        @AttributeOverride(name = "description", column = @Column(name = "Bezeichnung"))
})
public class TotalItemPositionTextBo extends AbstractItemPositionTextBo {

}
