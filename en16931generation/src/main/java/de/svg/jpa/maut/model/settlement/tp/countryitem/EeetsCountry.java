package de.svg.jpa.maut.model.settlement.tp.countryitem;

import de.svg.jpa.maut.dao.settlement.*;
import lombok.Getter;

import javax.persistence.EntityNotFoundException;
import java.util.stream.Stream;

public enum EeetsCountry {

    BELGIUM_TUNNEL(15, BelgiumTunnelInvoiceItemRepository.class),
    BELGIUM(60, TollItemRepository.class), //Belgien (DebitNote)
    DENMARK(19, DenmarkInvoiceItemRepository.class),
    GERMANY(22, GermanyInvoiceItemRepository.class),
    GERMANY_ABRECHNUNG(39, GermanyInvoiceItemRepository.class),
    FRANCE(21, FranceInvoiceItemRepository.class),
    ITALY(26, ItalyInvoiceItemRepository.class),
    NORWAY(9, NorwayInvoiceItemRepository.class),
    AUSTRIA(14, AustriaInvoiceItemRepository.class),
    POLAND(8, PolandInvoiceItemRepository.class),
    PORTUGAL(28, PortugalInvoiceItemRepository.class),
    SWITZERLAND(2, TollItemRepository.class),
    SPAIN(4, SpainInvoiceItemRepository.class);

    @Getter
    private Integer countryId;
    @Getter
    private Class<? extends CountryInvoiceItemRepo> repoClazz;

    EeetsCountry(Integer countryId, Class<? extends CountryInvoiceItemRepo> repo) {
        this.countryId = countryId;
        this.repoClazz = repo;
    }

    public static EeetsCountry getByCountryId(Integer countryId) {
        return Stream.of(values())
                .filter(eeetsCountry -> countryId.equals(eeetsCountry.countryId))
                .findAny()
                .orElseThrow(() -> new EntityNotFoundException("country id not valid" + countryId));
    }
}
