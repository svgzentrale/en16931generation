package de.svg.jpa.maut.dao.europetoll;

import de.svg.jpa.maut.model.europetoll.EuropeTollCountryEspInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EuropeTollCountryEspInvoiceRepository extends JpaRepository<EuropeTollCountryEspInvoiceView, Long> {

    Optional<EuropeTollCountryEspInvoiceView> findByInvoiceNumber(String invoiceNumber);
}
