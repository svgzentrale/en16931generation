package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.SettlementSvgView;
import de.svg.jpa.maut.model.settlement.SettlementSvgViewId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SvgRepository extends JpaRepository<SettlementSvgView, SettlementSvgViewId> {

    Optional<SettlementSvgView> findByIdSvgNumberAndIdProcessingGroupId(String svgNumber, Long processingGroupId);
}
