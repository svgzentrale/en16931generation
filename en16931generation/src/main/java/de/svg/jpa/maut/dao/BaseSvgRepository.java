package de.svg.jpa.maut.dao;

import de.svg.jpa.maut.model.AbstractSvgBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.Optional;

@NoRepositoryBean
public interface BaseSvgRepository<T extends AbstractSvgBo, ID extends Serializable> extends JpaRepository<T, ID> {

    Optional<T> findBySvgNumber(String customerNumber);
}
