package de.svg.jpa.maut.model.settlement.tp.countryitem;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(catalog = "Abrechnung", name = "VW_urptDeutschlandSVG")
@Data
@IdClass(InvoiceItemViewId.class)
public class GermanyInvoiceItemView implements TpCountryInvoiceItem {

    @Id
    @Column(name = "FK_Rg_land")
    private Long countryInvoiceId;

    @Column(name = "Land")
    private String country;

    @Column(name = "BasisWert")
    private String baseValue;

    @Column(name = "EinzelPreis")
    private String unitPrice;

    @Column(name = "Anzahl_Prozent")
    private String unitType;

    @Id
    @Column(name = "PosText")
    private String itemText;

    @Id
    @Column(name = "NettoBetrag")
    private BigDecimal netAmount;

    @Id
    @Column(name = "MwstBetrag")
    private BigDecimal vatAmount;

    @Id
    @Column(name = "MwStProz")
    private Integer vatRate;

    @Id
    @Column(name = "BruttoBetrag")
    private BigDecimal grossAmount;

    @Override
    public String getItemDescription() {
        return null;
    }

    @Override
    public Map<String, String> getAdditionalAttributes() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy");
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("Land", getCountry());
        attributesMap.put("Bezeichnung", getItemText());
        attributesMap.put("Basisbetrag/-menge", getBaseValue());
        attributesMap.put("Einzelpreis oder %-Satz", getUnitPrice() + " " + getUnitType());
        return attributesMap;
    }
}
