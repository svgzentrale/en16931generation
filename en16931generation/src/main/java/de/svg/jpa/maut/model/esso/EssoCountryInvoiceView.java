package de.svg.jpa.maut.model.esso;

import de.svg.jpa.maut.model.FuelcardCountryInvoiceView;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(catalog = "EssoCardZ", name = "VW_RechnungsDruckKopfGrundlage")
@Data
public class EssoCountryInvoiceView implements FuelcardCountryInvoiceView {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "RechnungsNr")
    private String invoiceNumber;

    @Column(name = "KundenId")
    private Long customerId;

    @Column(name = "RechDatum")
    private LocalDate invoiceDate;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "TextRechnungAbrechnung")
    private String invoiceText;

    @Column(name = "TextLandbezeichnung")
    private String countryName;

    @Column(name = "LandbezeichnungD")
    private String countryNameGerman;

    @Column(name = "TextZusatzunterAnschrift")
    private String footnote;

    @Column(name = "UstIdNrSVGZ")
    private String vatNumberSvg;

    @Column(name = "[Währung]")
    private String currencyCode;

    @Override
    public Long getInvoiceNumber() {
        return Long.valueOf(invoiceNumber);
    }
}