package de.svg.jpa.maut.dao.esso;

import de.svg.jpa.maut.dao.BaseSvgRepository;
import de.svg.jpa.maut.model.esso.EssoSvgBo;
import org.springframework.stereotype.Repository;

@Repository
public interface EssoSvgRepository  extends BaseSvgRepository<EssoSvgBo, Long> {
}
