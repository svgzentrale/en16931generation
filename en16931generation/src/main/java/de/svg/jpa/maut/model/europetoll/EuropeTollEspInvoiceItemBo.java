package de.svg.jpa.maut.model.europetoll;

import de.svg.jpa.maut.model.AbstractInvoiceItemBo;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(catalog = "EuropaMautZ", name = "tblRechPosEsp")
@Data
public class EuropeTollEspInvoiceItemBo {


    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "fkRechnungLand")
    private Long espInvoiceId;

    @Column(name = "KfzKz")
    private String licensePlate;

    @Column(name = "Achsklasse")
    private String axleClass;

    @Column(name = "Schadstoffklasse")
    private String emissionsClass;

    @Column(name = "[GültigVon]")
    private LocalDate validFrom;

    @Column(name = "[GültigBis]")
    private LocalDate validTo;

    @Column(name = "Verkaufstelle")
    private String pointOfSale;

    @Column(name = "BetragLW")
    private BigDecimal netPriceCountry;

    @Column(name = "BetragEur")
    private BigDecimal netPrice;
}
