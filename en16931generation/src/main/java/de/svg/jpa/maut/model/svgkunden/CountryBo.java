package de.svg.jpa.maut.model.svgkunden;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "SVG_Kunden", name = "tblLand")
@Data
public class CountryBo {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CountryCodeISO3166")
    private String isoCountryCode;
}
