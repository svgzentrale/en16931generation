package de.svg.jpa.maut.dao.settlement;


import de.svg.jpa.maut.model.settlement.SettlementInvoiceHeaderBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettlementInvoiceHeaderRepository extends JpaRepository<SettlementInvoiceHeaderBo, Long> {
}

