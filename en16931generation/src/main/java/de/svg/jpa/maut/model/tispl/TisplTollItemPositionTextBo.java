package de.svg.jpa.maut.model.tispl;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "TISPLZ", name = "tblPosTexte")
@Data
public class TisplTollItemPositionTextBo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "PosText")
    private String description;
}
