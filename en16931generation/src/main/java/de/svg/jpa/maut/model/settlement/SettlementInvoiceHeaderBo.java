package de.svg.jpa.maut.model.settlement;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "Abrechnung", name = "tbl_Rg_Kopf")
@Data
public class SettlementInvoiceHeaderBo {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "FK_KundenArt")
    private Integer customerType;
}
