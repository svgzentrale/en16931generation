package de.svg.jpa.maut.dao.euroshell;

import de.svg.jpa.maut.model.euroshell.ShellCollectiveInvoiceItemView;
import de.svg.jpa.maut.model.total.TotalCollectiveInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShellCollectiveInvoiceItemRepository extends JpaRepository<ShellCollectiveInvoiceItemView, Long> {
    List<ShellCollectiveInvoiceItemView> findByInvoiceNumber(Long invoiceNumber);
}
