package de.svg.jpa.maut.model.euroshell;

import de.svg.dto.InvoiceContactPerson;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.service.svginvoice.InvoiceServiceUtil;
import lombok.Data;

import javax.persistence.*;


@Entity
@Table(catalog = "EuroShellSvg", name = "tblOrganisationen")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "svgNumber", column = @Column(name = "SVG")),
        @AttributeOverride(name = "addressLine1", column = @Column(name = "AdZeile1")),
        @AttributeOverride(name = "addressLine2", column = @Column(name = "AdZeile2")),
        @AttributeOverride(name = "addressLine3", column = @Column(name = "AdStrasse")),
        @AttributeOverride(name = "addressLine4", column = @Column(name = "AdPLZ")),
        @AttributeOverride(name = "addressLine5", column = @Column(name = "AdOrt")),
        @AttributeOverride(name = "completeSendAdress", column = @Column(name = "AbsenderAdresse")),
        @AttributeOverride(name = "taxNumber", column = @Column(name = "SteuerNr")),
        @AttributeOverride(name = "contact", column = @Column(name = "Ansprechpartner")),
        @AttributeOverride(name = "bankAccount", column = @Column(name = "Bankverbindung"))
})
@Data
public class ShellSvgBo extends AbstractSvgBo {

    @Column(name = "Telefon")
    private String secContactPhone;

    @Column(name = "Mail")
    private String secContactMail;

    @Column(name = "Ust_ID_Nr")
    private String vatNumber;

    @Override
    public String getZipCode() {
        return getAddressLine4();
    }

    @Override
    public String getCity() {
        return getAddressLine5();
    }

    @Override
    public String getStreet() {
        return getAddressLine3();
    }

    @Override
    public String getName() {
        return getAddressLine1() + " " + getAddressLine2();
    }

    @Override
    public InvoiceContactPerson getContactInformation() {
        return InvoiceServiceUtil.extractContactInformationFromCompleteString(
                getContact(),
                getName(),
                getSecContactMail(),
                getSecContactPhone());
    }
}
