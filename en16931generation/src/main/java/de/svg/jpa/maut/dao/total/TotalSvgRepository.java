package de.svg.jpa.maut.dao.total;

import de.svg.jpa.maut.dao.BaseSvgRepository;
import de.svg.jpa.maut.model.total.TotalSvgBo;
import org.springframework.stereotype.Repository;

@Repository
public interface TotalSvgRepository extends BaseSvgRepository<TotalSvgBo, Long> {
}
