package de.svg.jpa.maut.model;

import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@MappedSuperclass
@Data
public abstract class AbstractInvoiceHeaderBo {

    public abstract List<? extends AbstractCountryInvoiceBo> getCountryInvoices();

    @Id
    private Long id;

    private Long invoiceNumber;

    private LocalDate invoiceDate;

    @ManyToOne
    @ToString.Exclude
    private CustomerBo customer;

    @ManyToOne
    @ToString.Exclude
    private InvoiceAddressBo address;

    public String getGlobalCustomerNumber() {
        return customer.getGlobalCustomerNumber();
    };
}
