package de.svg.jpa.maut.model;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.List;

@MappedSuperclass
@Data
public abstract class AbstractCountryInvoiceBo {

    public abstract AbstractInvoiceHeaderBo getHeader();

    public abstract List<? extends AbstractInvoiceItemBo> getItems();

    public abstract AbstractCountryBo getCountry();

    @Id
    private Long id;

    private Long invoiceNumber;
}
