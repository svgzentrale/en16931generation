package de.svg.jpa.maut.dao.svgkunden;

import de.svg.jpa.maut.model.svgkunden.BankAccountBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccountBo, Long> {

    Optional<BankAccountBo> findByCustomerIdAndType(Long customerId, Integer type);
}
