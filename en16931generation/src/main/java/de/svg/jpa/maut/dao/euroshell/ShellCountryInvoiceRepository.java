package de.svg.jpa.maut.dao.euroshell;

import de.svg.jpa.maut.model.euroshell.ShellCountryInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ShellCountryInvoiceRepository extends JpaRepository<ShellCountryInvoiceView, Long> {

    Optional<ShellCountryInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}
