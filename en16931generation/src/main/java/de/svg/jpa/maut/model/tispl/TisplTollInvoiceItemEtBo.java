package de.svg.jpa.maut.model.tispl;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

/** die abgerechneten Fremdleistungen */
@Entity
@Table(catalog = "TISPLZ", name = "tblRechnungsPosET")
@Data
public class TisplTollInvoiceItemEtBo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    /** used as description text for the invoice item*/
    @Column(name = "RgNr")
    private String invoiceNumber;

    @Column(name = "RgDatum")
    private LocalDate invoiceDate;

    @Column(name = "RgBetrag")
    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name = "fkRechnungsKpf")
    @ToString.Exclude
    private TisplTollInvoiceHeaderBo etInvoice;
}
