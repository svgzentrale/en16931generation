package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.SepaMandatInfoView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SepaMandatInfoRepository extends JpaRepository<SepaMandatInfoView, Long> {

    Optional<SepaMandatInfoView> findByCustomerId(Long customerID);
}
