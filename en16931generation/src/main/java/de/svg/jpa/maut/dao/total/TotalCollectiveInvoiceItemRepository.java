package de.svg.jpa.maut.dao.total;

import de.svg.jpa.maut.model.total.TotalCollectiveInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TotalCollectiveInvoiceItemRepository extends JpaRepository<TotalCollectiveInvoiceItemView, String> {

    List<TotalCollectiveInvoiceItemView> findByInvoiceNumber(Long invoiceNumber);
}
