package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.VatCorrectionView;
import de.svg.jpa.maut.model.settlement.VatCorrectionViewId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VatCorrectionRepository extends JpaRepository<VatCorrectionView, VatCorrectionViewId> {

    List<VatCorrectionView> findByIdCountryInvoiceId(Long countryInvoiceId);
}
