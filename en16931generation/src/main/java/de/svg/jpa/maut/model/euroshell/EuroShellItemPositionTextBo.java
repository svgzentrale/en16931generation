package de.svg.jpa.maut.model.euroshell;

import de.svg.jpa.maut.model.AbstractItemPositionTextBo;

import javax.persistence.*;


@Entity
@Table(catalog = "EuroShellZ", name = "tblPosTexte")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
        @AttributeOverride(name = "description", column = @Column(name = "Bezeichnung"))
})
public class EuroShellItemPositionTextBo extends AbstractItemPositionTextBo {

}
