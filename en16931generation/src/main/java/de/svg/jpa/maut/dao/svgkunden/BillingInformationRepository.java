package de.svg.jpa.maut.dao.svgkunden;

import de.svg.jpa.maut.model.svgkunden.BillingInformationBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BillingInformationRepository  extends JpaRepository<BillingInformationBo, Long> {

    Optional<BillingInformationBo> findByCustomerIdAndProcessingGroupId(Long customerId, Long processingGroupId);
}
