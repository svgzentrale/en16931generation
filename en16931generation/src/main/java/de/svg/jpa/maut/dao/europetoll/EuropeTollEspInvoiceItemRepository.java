package de.svg.jpa.maut.dao.europetoll;

import de.svg.jpa.maut.model.europetoll.EuropeTollEspInvoiceItemBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EuropeTollEspInvoiceItemRepository extends JpaRepository<EuropeTollEspInvoiceItemBo, Long> {
    
    List<EuropeTollEspInvoiceItemBo> findByEspInvoiceId(Long espInvoiceId);
}
