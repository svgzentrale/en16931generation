package de.svg.jpa.maut.dao.europetoll;

import de.svg.jpa.maut.model.europetoll.EuropeTollCollectiveInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EuropeTollCollectiveInvoiceRepository extends JpaRepository<EuropeTollCollectiveInvoiceView, Long> {
    Optional<EuropeTollCollectiveInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}
