package de.svg.jpa.maut.dao.esso;

import de.svg.jpa.maut.model.esso.EssoCollectiveInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EssoCollectiveInvoiceItemRepository extends JpaRepository<EssoCollectiveInvoiceItemView, Long> {

    List<EssoCollectiveInvoiceItemView> findByInvoiceNumber(Long invoiceNumber);
}
