package de.svg.jpa.maut.model.settlement;

import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import lombok.Data;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(catalog = "Abrechnung", name = "VW_rptLaenderRechnung")
@Data
public class CountryInvoiceView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FilterWert")
    private Long id;

    @Column(name = "KundenNr")
    private String customerNumber;

    @Column(name = "RechnungsNr")
    private Long invoiceNumber;

    @Column(name = "UstIdKunde")
    private String customerVatNumber;

    @Column(name = "UstIdRgSteller")
    private String svgVatNumber;

    @ManyToOne
    @JoinColumn( name="KundenId", insertable=false, updatable=false )
    private CustomerBo customer;

    @ManyToOne
    @JoinColumn(name="KundenId", referencedColumnName = "KundenId", insertable=false, updatable=false )
    private InvoiceAddressBo address;

    @Column(name = "RechDatum")
    private LocalDate invoiceDate;

    @Column(name = "Absender")
    private String sender;

    @Column(name = "TextRg")
    private String invoiceText;

    @Column(name = "Fälligkeit")
    private LocalDate dueDate;

    @Column(name = "GesamtBetragEUR")
    private BigDecimal totalAmount;

    @Column(name = "DruckDatumMerker")
    private String invoiceTextPrintDate;

    @Column(name = "LandNr")
    private Integer countryId;

    @Column(name = "Waehrung")
    private String currencyCode;

}
