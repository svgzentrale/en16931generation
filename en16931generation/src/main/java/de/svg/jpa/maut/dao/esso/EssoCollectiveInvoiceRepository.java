package de.svg.jpa.maut.dao.esso;

import de.svg.jpa.maut.model.esso.EssoCollectiveInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EssoCollectiveInvoiceRepository extends JpaRepository<EssoCollectiveInvoiceView, Long> {
    Optional<EssoCollectiveInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}
