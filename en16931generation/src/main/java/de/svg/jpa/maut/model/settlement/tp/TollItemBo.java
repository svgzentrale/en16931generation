package de.svg.jpa.maut.model.settlement.tp;

import lombok.Data;
import sun.util.resources.cldr.en.TimeZoneNames_en_Dsrt;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Entity
@Table(catalog = "Abrechnung", name = "tbl_TP_RgPos_Abrechnung")
@Data
public class TollItemBo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "FK_Rg_land")
    private Long countryInvoiceId;

    @Column(name = "Kennzeichen")
    private String licensePlate;

    @Column(name = "DocumentNumber")
    private String documentNumber;

    @Column(name = "ErsteEinfahrt")
    private ZonedDateTime entryTime;

    @Column(name = "LetzteAusfahrt")
    private ZonedDateTime exitTime;

    @Column(name = "KM")
    private BigDecimal mileage;

    @Column(name = "WarenGruppeHGK")
    private String productGroup;

    @Column(name = "Betrag")
    private BigDecimal grossAmount;

    //no vat for those items in this table. its Abrechnung from Belgium or Switzerland...
    public BigDecimal getNetAmount() {
        return getGrossAmount();
    }

    public BigDecimal getVatAmount() {
        return BigDecimal.ZERO;
    }

    public Integer getVatRate() {
        return 0;
    }

}
