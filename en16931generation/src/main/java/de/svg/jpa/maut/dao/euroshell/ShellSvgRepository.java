package de.svg.jpa.maut.dao.euroshell;

import de.svg.jpa.maut.dao.BaseSvgRepository;
import de.svg.jpa.maut.model.euroshell.ShellSvgBo;
import org.springframework.stereotype.Repository;

@Repository
public interface ShellSvgRepository extends BaseSvgRepository<ShellSvgBo, Long> {
}
