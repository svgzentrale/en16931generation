package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.FranceInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranceInvoiceItemRepository extends JpaRepository<FranceInvoiceItemView, Long>, CountryInvoiceItemRepo<FranceInvoiceItemView> {

}
