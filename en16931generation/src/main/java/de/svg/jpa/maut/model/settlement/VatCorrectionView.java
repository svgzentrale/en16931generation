package de.svg.jpa.maut.model.settlement;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(catalog = "Abrechnung", name = "VW_MwSt_Korrekturen")
@Data
public class VatCorrectionView {

    @EmbeddedId
    private VatCorrectionViewId id;

    @Column(name = "MwStKorrektur")
    private BigDecimal correctionVatAmount;

    @Column(name = "BruttoKorrektur")
    private BigDecimal correctionTotalGrossAmount;
}
