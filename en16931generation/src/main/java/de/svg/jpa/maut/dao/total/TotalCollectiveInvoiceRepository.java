package de.svg.jpa.maut.dao.total;

import de.svg.jpa.maut.model.total.TotalCollectiveInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TotalCollectiveInvoiceRepository extends JpaRepository<TotalCollectiveInvoiceView, Long> {
    Optional<TotalCollectiveInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}
