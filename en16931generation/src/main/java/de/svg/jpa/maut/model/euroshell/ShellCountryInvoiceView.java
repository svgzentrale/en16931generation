package de.svg.jpa.maut.model.euroshell;

import de.svg.jpa.maut.model.FuelcardCountryInvoiceView;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(catalog = "EuroShellZ", name = "VW_RechnungsDruckKopfGrundlage")
@Data
public class ShellCountryInvoiceView implements FuelcardCountryInvoiceView {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "RechnungsNr")
    private Long invoiceNumber;

    @Column(name = "KundenId")
    private Long customerId;

    @Column(name = "RechDatum")
    private LocalDate invoiceDate;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "TextRechnungAbrechnung")
    private String invoiceText;

    @Column(name = "TextLandbezeichnung")
    private String countryName;

    @Column(name = "TextZusatzunterAnschrift")
    private String footnote;

    @Column(name = "Ust_Id_Nr")
    private String vatNumberSvg;

    @Column(name = "[Währung]")
    private String currencyCode;

}
