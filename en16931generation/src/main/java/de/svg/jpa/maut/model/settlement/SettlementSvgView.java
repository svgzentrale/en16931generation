package de.svg.jpa.maut.model.settlement;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "Abrechnung", name = "VW_SVGStamm")
@Data
public class SettlementSvgView {

    @EmbeddedId
    private SettlementSvgViewId id;

    @Column(name = "Zeile1")
    private String addressLine1;

    @Column(name = "Zeile2")
    private String addressLine2;

    @Column(name = "Zeile3")
    private String addressLine3;

    @Column(name = "Zeile4")
    private String addressLine4;

    @Column(name = "UstIdNr")
    private String vatNumber;

    @Column(name = "AP_Name")
    private String contactPerson;

    @Column(name = "AP_Telefon")
    private String contactPhone;

    @Column(name = "AP_Fax")
    private String contactFax;

    @Column(name = "AP_Mail")
    private String contactMail;

    @OneToOne
    @JoinColumn(name = "SVG_Id")
    private SettlementSvgBo svgBo;

}
