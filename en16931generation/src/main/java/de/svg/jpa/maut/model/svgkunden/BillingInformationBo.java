package de.svg.jpa.maut.model.svgkunden;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(catalog = "SVG_Kunden", name = "tbl_AbrechnungsInformationen")
@Data
public class BillingInformationBo {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "FK_KundenID")
    private Long customerId;

    @ManyToOne
    @JoinColumn( name = "FK_VerarbeitungsGruppe", insertable=false, updatable=false )
    @ToString.Exclude
    private ProcessingGroupBo processingGroup;

    /** Zahlungsziel in Tagen*/
    @Column(name = "Zahlungsziel")
    private Integer paymentTarget;
}
