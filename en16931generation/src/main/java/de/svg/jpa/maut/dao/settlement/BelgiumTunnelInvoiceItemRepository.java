package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.BelgiumTunnelInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BelgiumTunnelInvoiceItemRepository extends JpaRepository<BelgiumTunnelInvoiceItemView, Long>, CountryInvoiceItemRepo<BelgiumTunnelInvoiceItemView> {
}
