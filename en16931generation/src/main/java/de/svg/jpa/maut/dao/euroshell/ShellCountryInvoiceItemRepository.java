package de.svg.jpa.maut.dao.euroshell;

import de.svg.jpa.maut.model.euroshell.ShellCountryInvoiceItemBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShellCountryInvoiceItemRepository extends JpaRepository<ShellCountryInvoiceItemBo, Long> {
    List<ShellCountryInvoiceItemBo> findByCountryInvoiceId(Long id);
}
