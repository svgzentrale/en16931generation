package de.svg.jpa.maut.model.settlement.tp.countryitem;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

/** for all telepass country invoice items */
public interface TpCountryInvoiceItem {

    Long getCountryInvoiceId();

    BigDecimal getNetAmount();

    BigDecimal getGrossAmount();

    BigDecimal getVatAmount();

    Integer getVatRate();

    String getItemText();

    String getItemDescription();

    Map<String, String> getAdditionalAttributes();
}
