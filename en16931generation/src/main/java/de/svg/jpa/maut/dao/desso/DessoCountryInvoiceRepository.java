package de.svg.jpa.maut.dao.desso;

import de.svg.jpa.maut.model.desso.DessoCountryInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DessoCountryInvoiceRepository  extends JpaRepository<DessoCountryInvoiceView, Long> {

    Optional<DessoCountryInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}
