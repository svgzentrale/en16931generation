package de.svg.jpa.maut.model.europetoll;

import de.svg.jpa.maut.model.AbstractCountryBo;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "EuropaMautZ", name = "tblLand")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "countryNameGerman", column = @Column(name = "Land")),
})
@Data
public class EuropeTollCountryBo extends AbstractCountryBo {

//    @Column(name = "Land")
//    private String country;
//
//    @Column(name = "LandKuerzel")
//    private String shortName;
//
//    @Column(name = "TelefonVorwahl")
//    private String phonePrefix;
//
//    @Column(name = "UstIdNrHGK")
//    private String ustIdNrHGK;
//
//    @Column(name = "LandIdTC")
//    private String landIdTC;
//
//    @Column(name = "Währung")
//    private String currency;
//
//    @Column(name = "Lieferant")
//    private String supplier;
//
//    @Column(name = "PostLand")
//    private String postContry;
//
//    @Column(name = "MautArt")
//    private String tollType;
}
