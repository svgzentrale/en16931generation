package de.svg.jpa.maut.dao.europetoll;

import de.svg.jpa.maut.dao.BaseSvgRepository;
import de.svg.jpa.maut.model.europetoll.EuropeTollSvgBo;
import org.springframework.stereotype.Repository;

@Repository
public interface EuropeTollSvgRepository extends BaseSvgRepository<EuropeTollSvgBo, Long> {
}
