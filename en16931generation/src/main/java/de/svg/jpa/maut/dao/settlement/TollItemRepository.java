package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.TollItemBo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TollItemRepository extends JpaRepository<TollItemBo, Long>, CountryInvoiceItemRepo<TollItemBo>{

}
