package de.svg.jpa.maut.dao.desso;

import de.svg.jpa.maut.model.desso.DessoCollectiveInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DessoCollectiveInvoiceItemRepository extends JpaRepository<DessoCollectiveInvoiceItemView, Long> {

    List<DessoCollectiveInvoiceItemView> findByInvoiceNumber(Long invoiceNumber);
}
