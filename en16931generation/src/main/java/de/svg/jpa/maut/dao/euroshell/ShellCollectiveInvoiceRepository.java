package de.svg.jpa.maut.dao.euroshell;

import de.svg.jpa.maut.model.euroshell.ShellCollectiveInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShellCollectiveInvoiceRepository extends JpaRepository<ShellCollectiveInvoiceView, Long> {
    Optional<ShellCollectiveInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}