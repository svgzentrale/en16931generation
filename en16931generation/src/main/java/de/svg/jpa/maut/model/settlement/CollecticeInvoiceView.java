package de.svg.jpa.maut.model.settlement;

import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import lombok.Getter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(catalog = "Abrechnung", name = "VW_rptDeckblatt")
@Getter
public class CollecticeInvoiceView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FilterWert")
    private Long id;

    @Column(name = "VG")
    private Long processingGroupId;

    @Column(name = "KundenNr")
    private String svgCustomerNumber;

    @ManyToOne
    @JoinColumn( name="FK_Kunde", insertable=false, updatable=false )
    private CustomerBo customer;

    @ManyToOne
    @JoinColumn(name="FK_Kunde", referencedColumnName = "KundenId", insertable=false, updatable=false )
    private InvoiceAddressBo address;

    @Column(name = "BelegNr")
    private Long invoiceNumber;

    @Column(name = "Rg_Datum")
    private LocalDate invoiceDate;

    @Column(name = "BruttoSumme")
    private BigDecimal grossAmount;

    @Column(name = "ZahlungsText")
    private String paymentText;

    @Column(name = "DeckblattBezeichnung")
    private String headerDescription;

    @Column(name = "Absender")
    private String sender;

    @Column(name = "RgInfoText")
    private String invoiceInfoText;

    @Column(name = "NameZusatzfeldDeckblatt")
    private String nameAdditionHeader;

    @Column(name = "ZusatzFeld")
    private String additionArea;

    @OneToMany(mappedBy = "collecticeInvoiceView")
    private List<CollecticeInvoiceItemView> items;

    @Column(name = "AP_Name")
    private String contactName;

    @Column(name = "AP_Telefon")
    private String contactPhone;

    @Column(name = "AP_Fax")
    private String contactFax;

    @Column(name = "AP_Mail")
    private String contactEmai;
}
