package de.svg.jpa.maut.model.settlement;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "Abrechnung", name = "VW_MandatsInfos")
@Data
public class SepaMandatInfoView {

    @Id
    @Column(name = "KundenId")
    private Long customerId;

    @Column(name = "MandatNr")
    private String sepaMandatNumber;

    @Column(name = "IBAN")
    private String iban;

    @Column(name = "BIC")
    private String bic;
}
