package de.svg.jpa.maut.model.esso;

import de.svg.jpa.maut.model.AbstractInvoiceItemBo;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;


@Entity
@Table(catalog = "EssoCardZ", name = "tblRechnungsPos")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "deliveryDate", column = @Column(name = "LieferDatum")),
        @AttributeOverride(name = "stationName", column = @Column(name = "Station")),
        @AttributeOverride(name = "transactionNumber", column = @Column(name = "LS_Nr")),
        @AttributeOverride(name = "mileage", column = @Column(name = "KM_Stand")),
        @AttributeOverride(name = "unit", column = @Column(name = "ME")),
        @AttributeOverride(name = "quantity", column = @Column(name = "Menge")),
        @AttributeOverride(name = "unitNetPrice", column = @Column(name = "EinzelPreisNetto")),
        @AttributeOverride(name = "unitGrossPrice", column = @Column(name = "EinzelPreis")),
        @AttributeOverride(name = "totalNetPrice", column = @Column(name = "NettoRgBetragLW")),
        @AttributeOverride(name = "totalGrossPrice", column = @Column(name = "BruttoRgBetragLW")),
        @AttributeOverride(name = "vatAmount", column = @Column(name = "UstBetrag")),
        @AttributeOverride(name = "vatRate", column = @Column(name = "Ust")),
        @AttributeOverride(name = "surcharge", column = @Column(name = "Aufschlag")),
        @AttributeOverride(name = "surchargeIndicator", column = @Column(name = "AS_KZ")),
        @AttributeOverride(name = "rebate", column = @Column(name = "Nachlass")),
        @AttributeOverride(name = "rebateIndicator", column = @Column(name = "NL_KZ"))
})
@Data
public class EssoInvoiceItemBo extends AbstractInvoiceItemBo {

    @Column(name = "fkRechnungLand")
    private Long countryInvoiceId;

    @ManyToOne
    @JoinColumn(name = "fkKarte")
    @NotFound(action = NotFoundAction.IGNORE) //in database 9999999 is used to say that product is not associated with fuelcard. eg service fees.
    private EssoEcCardBo fuelcard;

    @ManyToOne
    @JoinColumn(name = "fkPosText")
    private EssoItemPositionTextBo itemPositionText;

}