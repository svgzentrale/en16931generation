package de.svg.jpa.maut.model.tispl;

import de.svg.jpa.maut.model.total.TotalItemPositionTextBo;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(catalog = "TISPLZ", name = "tblRechnungsPosSVG")
@Data
public class TisplTollInvoiceItemSvgBo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "Betrag")
    private BigDecimal amount;

    @Column(name = "Prozentsatz")
    private BigDecimal percentage;

    @ManyToOne
    @JoinColumn(name = "fkPosText")
    private TisplTollItemPositionTextBo itemPositionText;

    @ManyToOne
    @JoinColumn(name = "fkRechnungsKpf")
    @ToString.Exclude
    private TisplTollInvoiceHeaderBo svgInvoice;
}
