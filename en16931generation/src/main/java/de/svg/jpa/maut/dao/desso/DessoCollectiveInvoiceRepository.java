package de.svg.jpa.maut.dao.desso;

import de.svg.jpa.maut.model.desso.DessoCollectiveInvoiceView;
import de.svg.jpa.maut.model.total.TotalCollectiveInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DessoCollectiveInvoiceRepository extends JpaRepository<DessoCollectiveInvoiceView, Long> {
    Optional<DessoCollectiveInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}
