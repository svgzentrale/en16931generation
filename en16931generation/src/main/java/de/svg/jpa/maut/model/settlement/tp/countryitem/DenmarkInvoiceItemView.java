package de.svg.jpa.maut.model.settlement.tp.countryitem;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(catalog = "Abrechnung", name = "VW_TP_urptDK")
@Data
@IdClass(InvoiceItemViewId.class)
public class DenmarkInvoiceItemView implements TpCountryInvoiceItem {

    @Id
    @Column(name = "RLId")
    private Long countryInvoiceId;

    @Column(name = "OBU_Id")
    private String obu;

    @Column(name = "KfzKz")
    private String licensePlate;

    @Column(name = "Lieferdatum")
    private ZonedDateTime deliveryDateTime;

    //bitte beachten: falche Schreibweise in der View
    @Column(name = "Transaktionsmummer")
    private String transactionNumber;

    @Column(name = "Leistung")
    private String itemText;

    @Column(name = "Produktbezeichnung")
    private String itemDescription;

    @Column(name = "TollCharger")
    private String tollCharger;

    @Id
    @Column(name = "NettoBetrag")
    private BigDecimal netAmount;

    @Id
    @Column(name = "MwstBetrag")
    private BigDecimal vatAmount;

    @Id
    @Column(name = "MwStProz")
    private Integer vatRate;

    @Id
    @Column(name = "BruttoBetrag")
    private BigDecimal grossAmount;

    @Override
    public Map<String, String> getAdditionalAttributes() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy");
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("Lieferdatum", formatter.format(getDeliveryDateTime()));
        attributesMap.put("OBU", getObu());
        attributesMap.put("Kfz Kennzeichen", getLicensePlate());
        attributesMap.put("Transaktionsnummer", getTransactionNumber());
        attributesMap.put("Mautbetreiber", getTollCharger());
        return attributesMap;
    }

}
