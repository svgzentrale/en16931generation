package de.svg.jpa.maut.model;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
public abstract class AbstractItemPositionTextBo {

    @Id
    private Long id;

    private String description;
}
