package de.svg.jpa.maut.model.tispl;

import de.svg.dto.InvoiceContactPerson;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.service.svginvoice.InvoiceServiceUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Entity
@Table(catalog = "TISPLSVG", name = "tblWinSys")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ID")),
        @AttributeOverride(name = "svgNumber", column = @Column(name = "SVG_Nr")),
        @AttributeOverride(name = "addressLine1", column = @Column(name = "SVG_Adresse1")),
        @AttributeOverride(name = "addressLine2", column = @Column(name = "SVG_Adresse2")),
        @AttributeOverride(name = "addressLine3", column = @Column(name = "SVG_Adresse3")),
        @AttributeOverride(name = "addressLine4", column = @Column(name = "SVG_Adresse4")),
        @AttributeOverride(name = "addressLine5", column = @Column(name = "SVG_Adresse5")),
        @AttributeOverride(name = "completeSendAdress", column = @Column(name = "Absender")),
        @AttributeOverride(name = "taxNumber", column = @Column(name = "SteuerNr")),
        @AttributeOverride(name = "contact", column = @Column(name = "Ansprechpartner")),
        @AttributeOverride(name = "bankAccount", column = @Column(name = "Bankverbindung"))

})
@Data
@EqualsAndHashCode(callSuper = false)
public class TisplSvgBo extends AbstractSvgBo {

    @Column(name = "MailAbsender")
    private String secContactMail;

    @Column(name = "SVG_Adresse5")
    private String addressLine4;

    @Override
    public String getZipCode() {
        String zipCodeAndCity = getAddressLine5() != null ? getAddressLine5() : getAddressLine4();
        return StringUtils.substringBefore(zipCodeAndCity, " ");
    }

    @Override
    public String getCity() {
        String zipCodeAndCity = getAddressLine5() != null ? getAddressLine5() : getAddressLine4();
        return StringUtils.substringAfter(zipCodeAndCity, " ");
    }

    @Override
    public String getStreet() {
        return getAddressLine3() != null ? getAddressLine3() : getAddressLine2();
    }

    @Override
    public String getName() {
        return getAddressLine3() != null ? getAddressLine1() + " " + getAddressLine2() : getAddressLine1()  ;
    }

    @Override
    public InvoiceContactPerson getContactInformation() {
        return InvoiceServiceUtil.extractContactInformationFromCompleteString(
                getContact(),
                getName(),
                getSecContactMail(),
                "n/a");
    }

    @Override
    public String getVatNumber() {
        return null;
    }
}
