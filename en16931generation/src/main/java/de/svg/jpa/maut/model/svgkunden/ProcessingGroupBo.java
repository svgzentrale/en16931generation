package de.svg.jpa.maut.model.svgkunden;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(catalog = "SVG_Kunden", name = "[tblNST_Verarbeitungsgruppe]")
@Data
public class ProcessingGroupBo {

    /**
     * ID	Name
     * 1	DKV
     * 2	SVG-Box
     * 3	Vialtis FDE
     * 4	Esso DE
     * 5	Esso
     * 6	EuroShell
     * 7	Total
     * 8	Deutsche Maut
     * 9	EuropaMaut
     * 10	TIS-PL
     * 11	Vialtis Sat
     * 12	SVG fleXbox
     */
    public static final Long DKV_ID = 1L;
    public static final Long SVGBOX_ID = 2L;
    public static final Long VIALTIS_FDE_ID = 3L;
    public static final Long DESSO_ID = 4L;
    public static final Long ESSO_ID = 5L;
    public static final Long SHELL_ID = 6L;
    public static final Long TOTAL_ID = 7L;
    public static final Long GERMAN_TOLL_ID = 8L;
    public static final Long EURO_TOLL_ID = 9L;
    public static final Long TIS_PL_ID = 10L;
    public static final Long VIALTIS_SAT_ID = 11L;
    public static final Long SVG_FLEXBOX_ID = 12L;

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Max")
    private Integer maxPaymentTarget;
}
