package de.svg.jpa.maut.dao.settlement;

import java.util.List;

public interface CountryInvoiceItemRepo<T> {
    List<T> findByCountryInvoiceId(Long countryInvoiceId);

}
