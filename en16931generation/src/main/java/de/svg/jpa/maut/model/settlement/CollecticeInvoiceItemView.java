package de.svg.jpa.maut.model.settlement;

import lombok.Getter;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(catalog = "Abrechnung", name = "VW_rptDeckBlattPosSaetze")
@Getter
public class CollecticeInvoiceItemView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RechnungsNummer")
    private Long id;

    @OneToMany
    @JoinColumn(name = "fKRechnungLand")
    private List<CollecticeInvoiceItemView> items;

    @Column(name = "GesamtBetragEUR")
    private BigDecimal totalGrossAmount;

    @Column(name = "GesamtBetragFW")
    private BigDecimal totalGrossAmountFW;

    @Column(name = "Landbezeichnung")
    private String country;

    @Column(name = "WaehrungsKuerzel")
    private String currency;

    @ManyToOne
    @JoinColumn(name = "FK_Rg_Kopf", insertable=false, updatable=false)
    private CollecticeInvoiceView collecticeInvoiceView;

}
