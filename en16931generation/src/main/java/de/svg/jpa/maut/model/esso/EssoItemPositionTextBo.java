package de.svg.jpa.maut.model.esso;

import de.svg.jpa.maut.model.AbstractItemPositionTextBo;

import javax.persistence.*;


@Entity
@Table(catalog = "EssoCardZ", name = "tblPosTexte")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
        @AttributeOverride(name = "description", column = @Column(name = "Bezeichnung"))
})
public class EssoItemPositionTextBo extends AbstractItemPositionTextBo {

}
