package de.svg.jpa.maut.model.europetoll;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(catalog = "EuropaMautZ", name = "VW_RechnungsDruckDeckblattGrundlage")
@Data
public class EuropeTollCollectiveInvoiceView {

    @Id
    @Column(name = "Id")
    private Long id;

    @Column(name = "KundenId")
    private Long customerId;

    @Column(name = "RechnungsNr")
    private Long invoiceNumber;

    @Column(name = "RechDatum")
    private LocalDate invoiceDate;

    @Column(name = "SVG")
    private String svgNumber;

    @Column(name = "KundenArt")
    private Integer customerType;

    @Column(name = "Abbuchtext")
    private String debitComment;

    @Column(name = "SteuerNummer")
    private String customerVatNumber;

}
