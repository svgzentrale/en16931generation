package de.svg.jpa.maut.model.svgkunden;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Where;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(catalog = "SVG_Kunden", name = "tblAdressen")
@Where(clause = "AdressArt=1")
@Data
public class InvoiceAddressBo implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "KundenId")
    private Long customerId;

    @Column(name = "Name1")
    private String name1;

    @Column(name = "Name2")
    private String name2;

    @Column(name = "Name3")
    private String name3;

    @Column(name = "Straße")
    private String street;

    @Column(name = "PLZ")
    private String zipCode;

    @Column(name = "Ort")
    private String city;

    @ManyToOne
    @JoinColumn( name = "Land")
    private CountryBo country;

    public String getCustomerName() {
        return (
                name1 +
                        (!StringUtils.isBlank(name2) ? " " + StringUtils.trim(name2) : "") +
                        (!StringUtils.isBlank(name3) ? " " + StringUtils.trim(name3) : "")
        );
    }

}
