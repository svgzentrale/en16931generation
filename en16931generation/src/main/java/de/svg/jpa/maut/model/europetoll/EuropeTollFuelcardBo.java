package de.svg.jpa.maut.model.europetoll;

import de.svg.jpa.maut.model.AbstractFuelcardBo;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "EuropaMautSVG", name = "tblKartenStamm")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
})
@Data
public class EuropeTollFuelcardBo extends AbstractFuelcardBo {

    //    @Column(name = "") //Kostenstelle
//    private String center;
    @Override
    public String getCostcenter() {
        return null;
    }
}
