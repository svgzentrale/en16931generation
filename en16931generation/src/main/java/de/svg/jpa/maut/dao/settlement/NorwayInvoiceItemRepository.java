package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.NorwayInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NorwayInvoiceItemRepository extends JpaRepository<NorwayInvoiceItemView, Long>, CountryInvoiceItemRepo<NorwayInvoiceItemView> {

}
