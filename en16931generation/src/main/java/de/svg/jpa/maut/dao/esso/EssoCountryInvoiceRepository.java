package de.svg.jpa.maut.dao.esso;

import de.svg.jpa.maut.model.esso.EssoCountryInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EssoCountryInvoiceRepository extends JpaRepository<EssoCountryInvoiceView, Long> {

    Optional<EssoCountryInvoiceView> findByInvoiceNumber(String invoiceNumber);
}
