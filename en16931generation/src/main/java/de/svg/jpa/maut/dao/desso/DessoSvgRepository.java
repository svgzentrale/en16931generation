package de.svg.jpa.maut.dao.desso;

import de.svg.jpa.maut.dao.BaseSvgRepository;
import de.svg.jpa.maut.model.desso.DessoSvgBo;
import org.springframework.stereotype.Repository;

@Repository
public interface DessoSvgRepository extends BaseSvgRepository<DessoSvgBo, Long> {
}
