package de.svg.jpa.maut.model.settlement.tp.countryitem;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(catalog = "Abrechnung", name = "VW_TP_urptPL")
@Data
@IdClass(InvoiceItemViewId.class)
public class PolandInvoiceItemView implements TpCountryInvoiceItem {

    @Id
    @Column(name = "RLId")
    private Long countryInvoiceId;

    @Column(name = "OBU_Id")
    private String obu;

    @Column(name = "KfzKz")
    private String licensePlate;

    @Column(name = "LieferDatum")
    private LocalDate deliveryDate;

    @Column(name = "StationOrt")
    private String station;

    @Column(name = "StationNr")
    private String stationNumber;

    @Column(name = "Transaktionsmummer") //TODO vorsicht in der View falsch geschrieben...
    private String transactionNumber;

    @Id
    @Column(name = "ProduktBezeichnung")
    private String itemText;

    @Id
    @Column(name = "NettoBetrag")
    private BigDecimal netAmount;

    @Id
    @Column(name = "MwStBetrag")
    private BigDecimal vatAmount;

    @Id
    @Column(name = "MwStProz")
    private Integer vatRate;

    @Id
    @Column(name = "BruttoBetrag")
    private BigDecimal grossAmount;

    @Override
    public String getItemDescription() {
        return getStationNumber() + " " + getStation();
    }


    @Override
    public Map<String, String> getAdditionalAttributes() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy");
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("Lieferdatum", formatter.format(getDeliveryDate()));
        attributesMap.put("OBU", getObu());
        attributesMap.put("Kfz Kennzeichen", getLicensePlate());
        attributesMap.put("Transaktionsnummer", getTransactionNumber());
        return attributesMap;
    }
}
