package de.svg.jpa.maut.model.esso;

import de.svg.jpa.maut.model.AbstractFuelcardBo;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "EssoCardSVG", name = "tblECKarten")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ECKartenID")),
        @AttributeOverride(name = "pan", column = @Column(name = "PAN")),
        @AttributeOverride(name = "licensePlate", column = @Column(name = "KarteKfzText"))
})
public class EssoEcCardBo extends AbstractFuelcardBo {

    @Override
    public String getCostcenter() {
        return null;
    }
}

