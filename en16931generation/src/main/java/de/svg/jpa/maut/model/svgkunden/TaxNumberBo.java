package de.svg.jpa.maut.model.svgkunden;


import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "SVG_Kunden", name = "tblSteuerNummern")
@Data
public class TaxNumberBo {

    @Id
    @Column(name = "PK")
    private Long id;

    @ManyToOne
    @JoinColumn(name="fkKs" )
    private CustomerBo customer;

    @Column(name = "SteuerArt")
    private Integer typeId;

    @Column(name = "SteuerNummer")
    private String number;

}
