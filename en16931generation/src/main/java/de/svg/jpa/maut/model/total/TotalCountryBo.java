package de.svg.jpa.maut.model.total;

import de.svg.jpa.maut.model.AbstractCountryBo;

import javax.persistence.*;

@Entity
@Table(catalog = "TotalCardZ", name = "tblLand")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
        @AttributeOverride(name = "countryNameGerman", column = @Column(name = "LandBezeichnungD")),
})
public class TotalCountryBo extends AbstractCountryBo {

}
