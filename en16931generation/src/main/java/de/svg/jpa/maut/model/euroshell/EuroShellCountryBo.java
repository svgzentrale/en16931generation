package de.svg.jpa.maut.model.euroshell;

import de.svg.jpa.maut.model.AbstractCountryBo;

import javax.persistence.*;

@Entity
@Table(catalog = "EuroShellZ", name = "tblLand")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
        @AttributeOverride(name = "countryNameGerman", column = @Column(name = "LandBezeichnungD")),
})
public class EuroShellCountryBo extends AbstractCountryBo {

}
