package de.svg.jpa.maut.dao;

import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerInvoiceAddressRepository extends JpaRepository<InvoiceAddressBo, Long> {

    Optional<InvoiceAddressBo> findByCustomerId(Long customerId);


}
