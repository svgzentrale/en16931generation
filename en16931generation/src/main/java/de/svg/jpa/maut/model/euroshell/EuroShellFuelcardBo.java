package de.svg.jpa.maut.model.euroshell;

import de.svg.jpa.maut.model.AbstractFuelcardBo;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "EuroShellSVG", name = "tblKarten")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "KartenId")),
        @AttributeOverride(name = "pan", column = @Column(name = "PAN")),
        @AttributeOverride(name = "licensePlate", column = @Column(name = "KarteKfzText"))
})
@Data
public class EuroShellFuelcardBo extends AbstractFuelcardBo {
    //not mapped for total. just return null
    @Override
    public String getCostcenter() {
        return null;
    }
}
