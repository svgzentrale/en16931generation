package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.SpainInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpainInvoiceItemRepository extends JpaRepository<SpainInvoiceItemView, Long>, CountryInvoiceItemRepo<SpainInvoiceItemView> {
}
