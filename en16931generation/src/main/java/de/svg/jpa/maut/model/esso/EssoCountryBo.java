package de.svg.jpa.maut.model.esso;

import de.svg.jpa.maut.model.AbstractCountryBo;

import javax.persistence.*;

@Entity
@Table(catalog = "EssoCardZ", name = "tblLand")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
        @AttributeOverride(name = "countryNameGerman", column = @Column(name = "LandBezeichnungD")),
})
public class EssoCountryBo extends AbstractCountryBo {

}
