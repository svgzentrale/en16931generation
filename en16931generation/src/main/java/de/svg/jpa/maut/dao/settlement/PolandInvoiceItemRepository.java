package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.tp.countryitem.PolandInvoiceItemView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PolandInvoiceItemRepository extends JpaRepository<PolandInvoiceItemView, Long>, CountryInvoiceItemRepo<PolandInvoiceItemView> {

}
