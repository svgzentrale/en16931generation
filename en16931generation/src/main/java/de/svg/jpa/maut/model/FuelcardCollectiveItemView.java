package de.svg.jpa.maut.model;

import java.math.BigDecimal;

public interface FuelcardCollectiveItemView {

   String getItemNumber();

   Long getInvoiceNumber();

   Integer getItemPosition();

   String getCountry();

   BigDecimal getTotalGrossPriceCurrency();

   BigDecimal getTotalGrossPriceEuro();

   String getCurrency();
}
