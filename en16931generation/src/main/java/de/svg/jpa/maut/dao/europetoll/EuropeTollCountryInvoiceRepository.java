package de.svg.jpa.maut.dao.europetoll;

import de.svg.jpa.maut.model.europetoll.EuropeTollCountryInvoiceBo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EuropeTollCountryInvoiceRepository extends JpaRepository<EuropeTollCountryInvoiceBo, Long> {

    List<EuropeTollCountryInvoiceBo> findByCollectiveInvoiceId(Long id);
}
