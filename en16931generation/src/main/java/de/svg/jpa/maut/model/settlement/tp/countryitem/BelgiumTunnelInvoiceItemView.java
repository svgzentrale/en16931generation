package de.svg.jpa.maut.model.settlement.tp.countryitem;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * View ist der Liefkenshoektunnel
 */
@Entity
@Table(catalog = "Abrechnung", name = "VW_TP_urptBE_TISPL")
@Data
@IdClass(InvoiceItemViewId.class)
public class BelgiumTunnelInvoiceItemView implements TpCountryInvoiceItem {

    @Id
    @Column(name = "RLId")
    private Long countryInvoiceId;

    @Column(name = "OBU_Id")
    private String obu;

    @Column(name = "KfzKz")
    private String licensePlate;

    @Column(name = "DatumUhrzeit")
    private ZonedDateTime deliveryDateTime;

    @Column(name = "Transaktionsnummer")
    private String itemText;

    @Id
    @Column(name = "NettoBetrag")
    private BigDecimal netAmount;

    @Id
    @Column(name = "MwStBetrag")
    private BigDecimal vatAmount;

    @Id
    @Column(name = "MwStProz")
    private Integer vatRate;

    @Id
    @Column(name = "BruttoBetrag")
    private BigDecimal grossAmount;

    @Override
    public String getItemDescription() {
        return null;
    } //TODO

    @Override
    public Map<String, String> getAdditionalAttributes() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy");
    Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("Lieferdatum", formatter.format(getDeliveryDateTime()));
        attributesMap.put("OBU", getObu());
        attributesMap.put("Kfz Kennzeichen", getLicensePlate());
        attributesMap.put("Transaktionsnummer", getItemDescription());
        return attributesMap;
    }
}
