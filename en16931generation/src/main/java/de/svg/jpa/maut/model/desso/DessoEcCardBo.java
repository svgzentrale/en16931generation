package de.svg.jpa.maut.model.desso;

import de.svg.jpa.maut.model.AbstractFuelcardBo;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(catalog = "DessoPurSVG", name = "tblECKarten")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ECKartenID")),
        @AttributeOverride(name = "pan", column = @Column(name = "PAN")),
        @AttributeOverride(name = "licensePlate", column = @Column(name = "KarteKfzText"))
})
@Data
public class DessoEcCardBo extends AbstractFuelcardBo {

    @Column(name = "Freitext9")
    private String costcenter;

}
