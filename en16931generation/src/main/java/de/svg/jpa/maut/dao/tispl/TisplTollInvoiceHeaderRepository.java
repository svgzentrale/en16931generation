package de.svg.jpa.maut.dao.tispl;

import de.svg.jpa.maut.model.tispl.TisplTollInvoiceHeaderBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TisplTollInvoiceHeaderRepository extends JpaRepository<TisplTollInvoiceHeaderBo, Long> {
    Optional<TisplTollInvoiceHeaderBo> findByInvoiceNumber(java.lang.Long invoiceNumber);
}
