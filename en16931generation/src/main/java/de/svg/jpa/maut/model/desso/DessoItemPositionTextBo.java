package de.svg.jpa.maut.model.desso;

import de.svg.jpa.maut.model.AbstractItemPositionTextBo;

import javax.persistence.*;

@Entity
@Table(catalog = "DessoPurZ", name = "tblPosTexte")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "PK")),
        @AttributeOverride(name = "description", column = @Column(name = "Bezeichnung"))
})
public class DessoItemPositionTextBo extends AbstractItemPositionTextBo {

}
