package de.svg.jpa.maut.model.settlement.tp.countryitem;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(catalog = "Abrechnung", name = "VW_TP_urptES_VIAT")
@IdClass(InvoiceItemViewId.class)
@Data
public class SpainInvoiceItemView implements TpCountryInvoiceItem {

    @Id
    @Column(name = "RLId")
    private Long countryInvoiceId;

    @Column(name = "OBU_Id")
    private String obu;

    @Column(name = "KfzKz")
    private String licensePlate;

    @Column(name = "LieferDatum")
    private LocalDate deliveryDate;

    @Column(name = "StationOrt")
    private String stationTown;

    @Column(name = "StationNr")
    private String stationNumber;

    @Id
    @Column(name = "Bezeichnung")
    private String itemText;

    @Id
    @Column(name = "Netto")
    private BigDecimal netAmount;

    @Id
    @Column(name = "MwSt")
    private BigDecimal vatAmount;

    @Id
    @Column(name = "MwStProz")
    private Integer vatRate;

    @Id
    @Column(name = "Brutto")
    private BigDecimal grossAmount;

    @Override
    public String getItemDescription() {
        return getStationNumber() + " " + getStationTown();
    }

    @Override
    public Map<String, String> getAdditionalAttributes() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy");
        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("Lieferdatum", formatter.format(getDeliveryDate()));
        attributesMap.put("OBU", getObu());
        attributesMap.put("Kfz Kennzeichen", getLicensePlate());
        return attributesMap;
    }
}
