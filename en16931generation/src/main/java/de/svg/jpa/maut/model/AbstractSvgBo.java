package de.svg.jpa.maut.model;

import de.svg.dto.InvoiceContactPerson;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.List;

@MappedSuperclass
@Data
public abstract class AbstractSvgBo {

    @Id
    private Long id;

    private String svgNumber;

    private String addressLine1;

    private String addressLine2;

    private String addressLine3;

    private String addressLine4;

    private String addressLine5;

    private String completeSendAdress;

    private String taxNumber;

    private String contact;

    private String bankAccount;

    public abstract String getZipCode();

    public abstract String getCity();

    public abstract String getStreet();

    public abstract String getName();

    public abstract InvoiceContactPerson getContactInformation();

    public abstract String getVatNumber();
}
