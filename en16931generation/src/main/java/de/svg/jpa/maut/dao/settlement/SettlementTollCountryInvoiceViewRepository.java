package de.svg.jpa.maut.dao.settlement;

import de.svg.jpa.maut.model.settlement.CountryInvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SettlementTollCountryInvoiceViewRepository extends JpaRepository<CountryInvoiceView, Long> {
    Optional<CountryInvoiceView> findByInvoiceNumber(Long invoiceNumber);
}
