package de.svg.jpa.maut.dao;

import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerBo, Long> {
}
