package de.svg.service.svginvoice;

import de.svg.dto.SvgInvoice;
import de.svg.jpa.maut.dao.CustomerInvoiceAddressRepository;
import de.svg.jpa.maut.dao.CustomerRepository;
import de.svg.jpa.maut.dao.esso.*;
import de.svg.jpa.maut.dao.svgkunden.BankAccountRepository;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.jpa.maut.model.esso.EssoCollectiveInvoiceItemView;
import de.svg.jpa.maut.model.esso.EssoCollectiveInvoiceView;
import de.svg.jpa.maut.model.esso.EssoCountryInvoiceView;
import de.svg.jpa.maut.model.esso.EssoInvoiceItemBo;
import de.svg.jpa.maut.model.svgkunden.BankAccountBo;
import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import de.svg.jpa.maut.model.svgkunden.ProcessingGroupBo;
import de.svg.service.DueDateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EssoInvoiceService  {

    private final static String HUNGARY = "Ungarn";
    private final EssoCollectiveInvoiceRepository collectiveInvoiceRepository;
    private final EssoSvgRepository essoSvgRepository;
    private final EssoCollectiveInvoiceItemRepository collectiveInvoiceItemRepository;
    private final EssoCountryInvoiceRepository countryInvoiceRepository;
    private final EssoCountryInvoiceItemRepository countryInvoiceItemRepository;

    private final CustomerInvoiceAddressRepository customerInvoiceAddressRepository;
    private final CustomerRepository customerRepository;
    private final BankAccountRepository bankAccountRepository;

    private final DueDateService dueDateService;

    public SvgInvoice createCollectiveInvoice(Long invoiceNumber) {

        // erst mal die Rechnung finden...
        EssoCollectiveInvoiceView invoiceBo = collectiveInvoiceRepository.findByInvoiceNumber(invoiceNumber)
                .orElseThrow(() -> new EntityNotFoundException("invoice is not part of current verarbeitung"));

        AbstractSvgBo svgBo = (AbstractSvgBo) essoSvgRepository.findBySvgNumber(invoiceBo.getSvgNumber()).orElseThrow(EntityNotFoundException::new);

        // den Kunden als Käufer
        final Long customerId = invoiceBo.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();

        BankAccountBo buyersBankAccountBo =
                bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_ESSO_AND_TOTAL)
                        .orElseGet(() -> bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_TOLL) //siehe View VW_BV2
                                .orElse(null));

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.ESSO_ID, invoiceBo.getInvoiceDate());


        // die einzelnen Rechnungsposition der Sammelrechnung
        List<EssoCollectiveInvoiceItemView> items = collectiveInvoiceItemRepository.findByInvoiceNumber(invoiceNumber);

        return InvoiceServiceUtil.mapFuelcardCollectiveInvoiceViewToInvoiceItem(invoiceBo, svgBo, customerBo, invoiceAddressBo, buyersBankAccountBo, dueDate, items );

    }


    public SvgInvoice createCountryInvoice(Long invoiceNumber) {

        EssoCountryInvoiceView essoCountryInvoiceView = countryInvoiceRepository.findByInvoiceNumber(invoiceNumber.toString())
                .orElseThrow(() -> new EntityNotFoundException("country invoice is not part of current verarbeitung"));

        //die Vertragspartner
        // die SVG als Verkäufer
        AbstractSvgBo svgBo = essoSvgRepository.findBySvgNumber(essoCountryInvoiceView.getSvgNumber()).orElseThrow(EntityNotFoundException::new);

        //und der Kunde
        final Long customerId = essoCountryInvoiceView.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();

        //items
        List<EssoInvoiceItemBo> itemBos = countryInvoiceItemRepository.findByCountryInvoiceId(essoCountryInvoiceView.getId());

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.ESSO_ID, essoCountryInvoiceView.getInvoiceDate());
        SvgInvoice svgInvoice = InvoiceServiceUtil.mapFuelcardCountryInvoiceViewToInvoiceItem(essoCountryInvoiceView, svgBo, customerBo, invoiceAddressBo, itemBos, dueDate);
        svgInvoice.setLobsterExport(HUNGARY.equals(essoCountryInvoiceView.getCountryNameGerman()));
        return svgInvoice;
    }

}
