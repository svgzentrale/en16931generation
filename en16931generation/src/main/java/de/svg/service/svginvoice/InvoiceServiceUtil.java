package de.svg.service.svginvoice;

import de.svg.dto.*;
import de.svg.jpa.maut.model.AbstractInvoiceItemBo;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.jpa.maut.model.FuelcardCollectiveInvoiceView;
import de.svg.jpa.maut.model.FuelcardCollectiveItemView;
import de.svg.jpa.maut.model.FuelcardCountryInvoiceView;
import de.svg.jpa.maut.model.svgkunden.BankAccountBo;
import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import de.svg.jpa.maut.model.svgkunden.TaxNumberBo;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static de.svg.api.en16931.TaxTypeCodeContentType.VAT;

public class InvoiceServiceUtil {


    private static final Integer TAXNUMBER_TYPE_FISCAL = 0 ;
    private static final Integer TAXNUMBER_TYPE_VAT = 1;

    /** return the SVG details. */
    public static InvoiceSvgCompany mapToSvgCompany(AbstractSvgBo svgBo) {

        //SVG als Rechnungssteller
        InvoiceSvgCompany svg = new InvoiceSvgCompany();

        svg.setName(svgBo.getName());
        svg.setCity(svgBo.getCity());
        svg.setStreet(svgBo.getStreet());
        svg.setIsoCountryCode("DE");
        svg.setFiscalNumber(svgBo.getTaxNumber());
        svg.setVatNumber(svgBo.getVatNumber());
        svg.setZipCode(StringUtils.deleteWhitespace(svgBo.getZipCode()));
        svg.setId(svgBo.getSvgNumber());

        svg.setContact(svgBo.getContactInformation());
        return  svg;
    }


    /** returning the customerBo mapped to an {@link InvoiceCustomer}*/
    public static  InvoiceCustomer mapToInvoiceCustomer(CustomerBo customerBo, InvoiceAddressBo addressBo) {
        InvoiceCustomer customer = new InvoiceCustomer();
        customer.setId(customerBo.getGlobalCustomerNumber());

        customer.setName(addressBo.getCustomerName());
        customer.setStreet(addressBo.getStreet());
        customer.setZipCode(addressBo.getZipCode());
        customer.setCity(addressBo.getCity());
        customer.setIsoCountryCode(addressBo.getCountry().getIsoCountryCode());
        customer.setVatNumber(getTaxNumberByType(customerBo.getTaxNumbers(), TAXNUMBER_TYPE_VAT));
        customer.setFiscalNumber(getTaxNumberByType(customerBo.getTaxNumbers(),TAXNUMBER_TYPE_FISCAL));

        return customer;
    }

    /** extract the correct tax number from customers txNumbers. */
    private static String getTaxNumberByType(List<TaxNumberBo> taxNumbers, Integer taxnumberType) {
        if (taxNumbers == null) {
            return null;
        }
        return taxNumbers.stream().filter(taxNumberBo -> taxnumberType.equals(taxNumberBo.getTypeId())).findFirst().map(TaxNumberBo::getNumber).orElse(null);
    }

    /**
     *
     */
    public static InvoiceItem mapToFuelcardSvgInvoiceItems(AbstractInvoiceItemBo invoiceItemBo) {
        InvoiceItem item = new InvoiceItem();
        item.setItemId(invoiceItemBo.getId());
        item.setItemText(invoiceItemBo.getItemPositionText().getDescription());
        item.setDescription(invoiceItemBo.getStationName());
        //mengenangabe
        item.setQuantity(invoiceItemBo.getQuantity());
        item.setUnit("L".equals(invoiceItemBo.getUnit()) ? ItemUnit.LITER : ItemUnit.PIECE);
        //einzelpreis
        item.setUnitNetPrice(invoiceItemBo.getUnitNetPrice());
        item.setUnitGrossPrice(invoiceItemBo.getUnitGrossPrice());
        //Gesamtpreis
        item.setTotalGrossPrice(invoiceItemBo.getTotalGrossPrice());
        item.setTotalNetPrice(invoiceItemBo.getTotalNetPrice());
        //Ust-Values
        item.setVatAmount(invoiceItemBo.getVatAmount());
        item.setVatRate(invoiceItemBo.getVatRate());

        //beschreibende Felder
        Map<String, String> attributesMap = new HashMap<>();
        if (invoiceItemBo.getFuelcard() != null) {
            attributesMap.put("Kennzeichen", invoiceItemBo.getFuelcard().getLicensePlate().trim());
            attributesMap.put("Karte", invoiceItemBo.getFuelcard().getPan());
            if(invoiceItemBo.getFuelcard().getCostcenter() != null) {
                attributesMap.put("Kostenstelle", invoiceItemBo.getFuelcard().getCostcenter());
            }
        }
        if (invoiceItemBo.getTransactionNumber() != null) {
            attributesMap.put("Transaktionsnummer", invoiceItemBo.getTransactionNumber());
        }
        if (invoiceItemBo.getStationName() != null) {
            attributesMap.put("Stationsort", invoiceItemBo.getStationName().trim());
        }
        if (invoiceItemBo.getMileage() != null) {
            attributesMap.put("KM-Stand", invoiceItemBo.getMileage().toString());
        }
        if (invoiceItemBo.getSurcharge() != null) {

            RebateOrExtraCharge surcharge = new RebateOrExtraCharge();
            surcharge.setType(RebateType.SURCHARGE);
            surcharge.setValue(invoiceItemBo.getSurcharge());
            if (invoiceItemBo.isSurchargePercentual()) {
                surcharge.setValueIsPercentValue(true);
            } else {
                surcharge.setValueIsPercentValue(false);
                surcharge.setBasisQunatity(BigDecimal.valueOf(100L)); //pro 100Liter
            }
            item.setSurcharge(surcharge);
        }

        if (invoiceItemBo.getRebate() != null) {

            RebateOrExtraCharge rebate = new RebateOrExtraCharge();
            rebate.setType(RebateType.REBATE);
            rebate.setValue(invoiceItemBo.getRebate());
            if (invoiceItemBo.isRebatePercentual()) {
                rebate.setValueIsPercentValue(true);
            } else {
                rebate.setValueIsPercentValue(false);
                rebate.setBasisQunatity(BigDecimal.valueOf(100L)); //pro 100Liter
            }
            item.setRebate(rebate);
        }

        item.setAttributes(attributesMap);
        return item;
    }



    public static PaymentType getPaymentType(Integer customerType) {

        switch (customerType) {
            case 2:
                return PaymentType.BANK_TRANSFER;

            case 3:
            case 4:
                return PaymentType.DIRECT_DEBIT;
            case 1:
            case 5:
            default:
                 return PaymentType.SEPA;

        }

    }


    /** get the payment terms of the customer
     * the invoiceDate is the base to calculate the due date.
     * */
    public static PaymentTerms mapToPaymentTerms(BankAccount buyersBankAccount,
                                               BankAccount sellersBankAccount,
                                               String sepaMandatNumber,
                                               PaymentType paymentType,
                                               LocalDate dueDate,
                                               String debitComment) {
        PaymentTerms paymentTerms = new PaymentTerms();

        paymentTerms.setType(paymentType);
        paymentTerms.setDescription(debitComment);
        paymentTerms.setDueDate(dueDate);

        paymentTerms.setBuyerAccount(buyersBankAccount);
        paymentTerms.setSellerAccount(sellersBankAccount);

        paymentTerms.setSepaMandat(sepaMandatNumber);

        return paymentTerms;

    }

    public static String extractIbanFromBankInformationString(String completeBankInformation) {
        //find "IBAN:" or "IBAN"  dann entweder bis zu "BIC:" oder zu "BIC" oder bis zum Ende
        String afterIban = StringUtils.substringAfterLast(completeBankInformation, "IBAN");
        String afterIbanBeforeBic = StringUtils.substringBefore(afterIban, "BIC");

        return StringUtils.deleteWhitespace(StringUtils.replace(afterIbanBeforeBic, ":", "" ));
    }

    public static String extractBicFromBankInformationString(String completeBankInformation) {
        String afterBic = StringUtils.substringAfter(completeBankInformation, "BIC");
        return StringUtils.deleteWhitespace(StringUtils.replace(afterBic, ":", "" ));
    }

    public static InvoiceContactPerson mapToInvoiceContactPerson(String name, String email, String phone, String fax) {
        InvoiceContactPerson invoiceContactPerson =  new InvoiceContactPerson();
        invoiceContactPerson.setEmail(email);
        invoiceContactPerson.setFax(fax);
        invoiceContactPerson.setName(name);
        invoiceContactPerson.setPhone(phone);
        return invoiceContactPerson;
    }

    public static SvgInvoice mapFuelcardCollectiveInvoiceViewToInvoiceItem(
            FuelcardCollectiveInvoiceView invoiceBo,
            AbstractSvgBo svgBo,
            CustomerBo customerBo,
            InvoiceAddressBo buyersAddressBo,
            BankAccountBo buyersBankAccountBo,
            LocalDate dueDate,
            List<? extends FuelcardCollectiveItemView> items) {

        SvgInvoice invoice = new SvgInvoice();


        if (invoiceBo.getAdditionalMessage() != null) {
            List<String> comments = new ArrayList<>();
            comments.add("gilt nicht als Umsatzsteuererstattungsbeleg (MwSt)");
            comments.add(invoiceBo.getAdditionalMessage());
            invoice.setComments(comments);
        }

        invoice.setInvoiceType(InvoiceType.COLLECTIVE_INVOICE);
        invoice.setInvoiceDate(invoiceBo.getInvoiceDate());
        invoice.setInvoiceNumber(invoiceBo.getInvoiceNumber().toString());

        // die SVG als Verkäufer
        invoice.setSeller(mapToSvgCompany(svgBo));

        // den Kunden als Käufer
        invoice.setBuyer( mapToInvoiceCustomer(customerBo, buyersAddressBo));


        // die Payment Terms.
        PaymentType paymentType = getPaymentType(invoiceBo.getCustomerType());
        BankAccount buyersBankAccount = null;
        BankAccount sellersBankAccount = null;
        String sepaMandantNumber= null;
        if (paymentType != PaymentType.BANK_TRANSFER) {
            if (buyersBankAccountBo == null) {
                throw new EntityNotFoundException("buyers bank account not present");
            }
            buyersBankAccount = new BankAccount();
            buyersBankAccount.setAccountName(buyersBankAccountBo.getAccountName());
            buyersBankAccount.setBic(buyersBankAccountBo.getBic());
            buyersBankAccount.setIban(buyersBankAccountBo.getIban());

            sepaMandantNumber = buyersBankAccountBo.getSepaMandatNumber();
        }
        if (paymentType == PaymentType.BANK_TRANSFER) {
            sellersBankAccount = mapFromCompleteBankAccountString(svgBo.getBankAccount());
        }

        PaymentTerms paymentTerms = mapToPaymentTerms(buyersBankAccount, sellersBankAccount, sepaMandantNumber,  paymentType, dueDate, invoiceBo.getDebitComment());
        invoice.setPaymentTerms(paymentTerms);

        invoice.setItems(items.stream().map(InvoiceServiceUtil::mapItemOfCollectiveInvoice).collect(Collectors.toList()));


        return invoice;
    }

    /** Die Bankinformationen liegen ggfs in einem einzigen Spalte vor und muessen dazu auseinandergebaut werden.
     * Dazu werden mit StringSuche die IBAN und die BIC gesucht.*/
    public static BankAccount mapFromCompleteBankAccountString(String completeBankInformation) {

        //find "IBAN:" or "IBAN"  dann mindestens 22 zeichen, dann bis ein Buchstane kommt oder das Zeilenende
        String afterIban = StringUtils.substringAfterLast(completeBankInformation, "IBAN");

        String afterIbanWithoutWhitespace = StringUtils.deleteWhitespace(StringUtils.replace(afterIban, ":", "" ));
        String iban =  StringUtils.substring(afterIbanWithoutWhitespace, 0, 22);

        //find "BIC:" dann das nächste Wort
        String bic = StringUtils.deleteWhitespace(getNextWord(completeBankInformation, "BIC", "BIC:"));

        BankAccount bankAccount = new BankAccount();
        bankAccount.setIban(iban);
        bankAccount.setBic(bic);
        return bankAccount;
    }

    //find any of words and return the next word of the completeString
    public static String getNextWord(String completeString, String... words) {
        String[] allWordsOfString = completeString.split(" ");
        int i=0;
        int index = -1;
        while(i<words.length && index<0 ) {
            index = Arrays.asList(allWordsOfString).indexOf(words[i]);
            i++;
        }
        return (index == -1) ? null : ((index + 1) == allWordsOfString.length) ? null : allWordsOfString[index + 1];
    }

    /** die Postionen aus der View. */
    private static InvoiceItem mapItemOfCollectiveInvoice(FuelcardCollectiveItemView itemBo) {
        InvoiceItem item = new InvoiceItem();

        //nettopreis ist Pflichtfeld
        item.setUnitNetPrice(itemBo.getTotalGrossPriceEuro());
        item.setUnitGrossPrice(itemBo.getTotalGrossPriceEuro());
        item.setQuantity(BigDecimal.ONE);
        item.setTotalGrossPrice(itemBo.getTotalGrossPriceEuro());
        item.setTotalNetPrice(itemBo.getTotalGrossPriceEuro());
        item.setItemId(Long.valueOf(itemBo.getItemNumber()));
        item.setItemText(itemBo.getCountry());
        item.setTaxType(TaxType.NULLRATE);
        item.setVatRate(0);

        Map<String, String> attributesMap = new HashMap<>();
        String totalGrossAmountFwString = NumberFormat.getNumberInstance(Locale.getDefault()).format(itemBo.getTotalGrossPriceCurrency());
        attributesMap.put( "Gesamtwert brutto LW", totalGrossAmountFwString + " " +itemBo.getCurrency() );
        item.setAttributes(attributesMap);

        return item;

    }

    public static SvgInvoice mapFuelcardCountryInvoiceViewToInvoiceItem
            (FuelcardCountryInvoiceView countryInvoiceView,
             AbstractSvgBo svgBo,
             CustomerBo customerBo,
             InvoiceAddressBo invoiceAddressBo,
             List<? extends AbstractInvoiceItemBo> itemBos,
             LocalDate dueDate) {

        SvgInvoice invoice = new SvgInvoice();

        invoice.setInvoiceNumber(countryInvoiceView.getInvoiceNumber().toString());
        invoice.setInvoiceDate(countryInvoiceView.getInvoiceDate());
        invoice.setInvoiceType(InvoiceType.COUNTRY_INVOICE);
        invoice.setCurrencyCode(countryInvoiceView.getCurrencyCode());

        List<String> comments = new ArrayList<>();
        comments.add(countryInvoiceView.getInvoiceText() + " " + countryInvoiceView.getCountryName());
        comments.add(countryInvoiceView.getFootnote());
        invoice.setComments(comments);

        //die Vertragspartner
        // die SVG als Verkäufer
        InvoiceSvgCompany invoiceSvgCompany = InvoiceServiceUtil.mapToSvgCompany(svgBo);
        invoiceSvgCompany.setVatNumber(countryInvoiceView.getVatNumberSvg());
        invoice.setSeller(invoiceSvgCompany);
        //correction of sender if country invoice is foreign country: use SVG Z instead
        if (!"Deutschland".equals(countryInvoiceView.getCountryName())) {
            invoiceSvgCompany.setName("SVG Bundes-Zentralgenossenschaft Straßenverkehr eG");
            invoiceSvgCompany.setStreet("Breitenbachstr. 1");
            invoiceSvgCompany.setCity("Frankfurt");
            invoiceSvgCompany.setZipCode("60487");
            invoiceSvgCompany.setIsoCountryCode("DE");
            invoiceSvgCompany.setId("50");
        }

        //und der Kunde
        invoice.setBuyer( InvoiceServiceUtil.mapToInvoiceCustomer(customerBo, invoiceAddressBo));

        //items
        invoice.setItems(itemBos.stream().map(InvoiceServiceUtil::mapToFuelcardSvgInvoiceItems).collect(Collectors.toList()));

        //fake paymentTerms um xml-konform zu sein. Dabei word aber der PrepaidAmount gesetzt, so dass der zu zahlende Betrag auf 0 gesetzt wird.
        // dueDate must be set
        //PaymentTerms paymentTerms = mapToPaymentTerms(buyersBankAccount, sellersBankAccount, sepaMandantNumber,  paymentType, dueDate, invoiceBo.getDebitComment());
        BankAccount sellersBankAccount = mapFromCompleteBankAccountString(svgBo.getBankAccount());

        PaymentTerms paymentTerms = new PaymentTerms();
        paymentTerms.setDueDate(dueDate);
        paymentTerms.setSellerAccount(sellersBankAccount);
        paymentTerms.setType(PaymentType.BANK_TRANSFER);
        paymentTerms.setPaymentAlreadyReceived(true);
        invoice.setPaymentTerms(paymentTerms);

        return invoice;
    }


    public static InvoiceContactPerson extractContactInformationFromCompleteString(
            String completeContactString,
            String fallbackName,
            String fallbackMail,
            String fallbackPhone
    ) {

        String[] split = completeContactString.split(System.lineSeparator());
        String email = null;
        String fax = null;
        String phone = null;
        String name = null;
        for (String oneLine : split) {

            if (StringUtils.containsIgnoreCase(oneLine, "@") ) {
                if (email != null) {
                    continue;
                }
                email = oneLine;
            } else if (oneLine.chars().anyMatch(Character::isDigit) && StringUtils.containsIgnoreCase(oneLine, "Fax") ) {
                if (fax != null) {
                    continue;
                }
                //remove all non digits
                fax =  oneLine.replaceAll( "[^\\d]", "" );
            } else if ( oneLine.chars().anyMatch(Character::isDigit) ) {
                if (phone != null) {
                    continue;
                }
                //remove all non digits
                phone =  oneLine.replaceAll( "[^\\d]", "" );
            }
            // kein mail, fax oder Tel. muss dann wohl der Namen sein.
            else {
                if(name != null) {
                    continue;
                }
                name = oneLine;
            }

        }

        // the fallback fields
        if (email == null) {
            email = fallbackMail;
        }
        if (phone == null) {
            phone = fallbackPhone;
        }
        if (name == null) {
            name = fallbackName;
        }

        InvoiceContactPerson invoiceContactPerson = new InvoiceContactPerson();
        invoiceContactPerson.setName(name);
        invoiceContactPerson.setPhone(phone);
        invoiceContactPerson.setEmail(email);
        invoiceContactPerson.setFax(fax);

        return invoiceContactPerson;
    }
}
