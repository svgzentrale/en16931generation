package de.svg.service.svginvoice;

import de.svg.dto.SvgInvoice;
import de.svg.jpa.maut.dao.CustomerInvoiceAddressRepository;
import de.svg.jpa.maut.dao.CustomerRepository;
import de.svg.jpa.maut.dao.svgkunden.BankAccountRepository;
import de.svg.jpa.maut.dao.total.*;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.jpa.maut.model.svgkunden.BankAccountBo;
import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import de.svg.jpa.maut.model.svgkunden.ProcessingGroupBo;
import de.svg.jpa.maut.model.total.TotalCollectiveInvoiceItemView;
import de.svg.jpa.maut.model.total.TotalCollectiveInvoiceView;
import de.svg.jpa.maut.model.total.TotalCountryInvoiceItemBo;
import de.svg.jpa.maut.model.total.TotalCountryInvoiceView;
import de.svg.service.DueDateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TotalInvoiceService  {

    private final TotalCollectiveInvoiceRepository collectiveInvoiceRepository;
    private final TotalCollectiveInvoiceItemRepository collectiveInvoiceItemRepository;
    private final TotalCountryInvoiceRepository countryInvoiceRepository;
    private final TotalCountryInvoiceItemRepository countryInvoiceItemRepository;

    private final CustomerInvoiceAddressRepository customerInvoiceAddressRepository;
    private final CustomerRepository customerRepository;
    private final BankAccountRepository bankAccountRepository;
    private final DueDateService dueDateService;
    private final TotalSvgRepository totalSvgRepository;



    /** get the data of that collective invoice and map its values to a {@link SvgInvoice} Dto.*/
    public SvgInvoice createCollectiveInvoice(Long invoiceNumber) {

        // erst mal die Rechnung finden...
        TotalCollectiveInvoiceView invoiceBo = collectiveInvoiceRepository.findByInvoiceNumber(invoiceNumber)
                .orElseThrow(() -> new EntityNotFoundException("invoice is not part of current verarbeitung"));

        AbstractSvgBo svgBo = (AbstractSvgBo) totalSvgRepository.findBySvgNumber(invoiceBo.getSvgNumber()).orElseThrow(EntityNotFoundException::new);

        // den Kunden als Käufer
        final Long customerId = invoiceBo.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();

        BankAccountBo buyersBankAccountBo =
                bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_ESSO_AND_TOTAL)
                        .orElseGet(() -> bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_TOLL) //siehe View VW_BV2
                                .orElse(null));

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.ESSO_ID, invoiceBo.getInvoiceDate());


        // die einzelnen Rechnungsposition der Sammelrechnung
        List<TotalCollectiveInvoiceItemView> items = collectiveInvoiceItemRepository.findByInvoiceNumber(invoiceNumber);

        return InvoiceServiceUtil.mapFuelcardCollectiveInvoiceViewToInvoiceItem(invoiceBo, svgBo, customerBo, invoiceAddressBo, buyersBankAccountBo, dueDate, items );
    }


    /** get the data of that collective invoice and map its values to a {@link SvgInvoice} Dto.*/
    public SvgInvoice createCountryInvoice(Long invoiceNumber) {


        TotalCountryInvoiceView totalCountryInvoiceView = countryInvoiceRepository.findByInvoiceNumber(invoiceNumber)
                .orElseThrow(() -> new EntityNotFoundException("country invoice is not part of current verarbeitung"));

        //die Vertragspartner
        // die SVG als Verkäufer
        AbstractSvgBo svgBo = totalSvgRepository.findBySvgNumber(totalCountryInvoiceView.getSvgNumber()).orElseThrow(EntityNotFoundException::new);

        //und der Kunde
        final Long customerId = totalCountryInvoiceView.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();

        //items
        List<TotalCountryInvoiceItemBo> itemBos = countryInvoiceItemRepository.findByCountryInvoiceId(totalCountryInvoiceView.getId());

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.ESSO_ID, totalCountryInvoiceView.getInvoiceDate());
        return InvoiceServiceUtil.mapFuelcardCountryInvoiceViewToInvoiceItem(totalCountryInvoiceView, svgBo, customerBo, invoiceAddressBo, itemBos, dueDate );


    }

}
