package de.svg.service.svginvoice;

import de.svg.dto.*;
import de.svg.jpa.maut.dao.CustomerInvoiceAddressRepository;
import de.svg.jpa.maut.dao.CustomerRepository;
import de.svg.jpa.maut.dao.europetoll.*;
import de.svg.jpa.maut.dao.svgkunden.BankAccountRepository;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.jpa.maut.model.europetoll.*;
import de.svg.jpa.maut.model.svgkunden.BankAccountBo;
import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import de.svg.jpa.maut.model.svgkunden.ProcessingGroupBo;
import de.svg.service.DueDateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EuropeTollInvoiceService  {

    private final EuropeTollCollectiveInvoiceRepository collectiveInvoiceRepository;
    private final EuropeTollCollectiveInvoiceItemRepository collectiveInvoiceItemRepository;
    private final EuropeTollSvgRepository svgRepository;
    private final EuropeTollCountryInvoiceRepository countryInvoiceRepository;
    private final EuropeTollCountryEspInvoiceRepository countryEspInvoiceRepository;
    private final EuropeTollEspInvoiceItemRepository espInvoiceItemRepository;

    //svgkunde
    private final CustomerInvoiceAddressRepository customerInvoiceAddressRepository;
    private final CustomerRepository customerRepository;
    private final BankAccountRepository bankAccountRepository;

    //services
    private final DueDateService dueDateService;


    public SvgInvoice createCollectiveInvoice(Long invoiceNumber) {
        // erst mal die Rechnung finden...
        EuropeTollCollectiveInvoiceView invoiceBo = collectiveInvoiceRepository.findByInvoiceNumber(invoiceNumber)
                .orElseThrow(() -> new EntityNotFoundException("invoice is not part of current verarbeitung"));

        SvgInvoice invoice = new SvgInvoice();

        //TODO statitische Texte, die die komplette Rechnung betreffen
        invoice.setComments(null);

        invoice.setInvoiceType(InvoiceType.COLLECTIVE_INVOICE);
        invoice.setInvoiceDate(invoiceBo.getInvoiceDate());
        invoice.setInvoiceNumber(invoiceBo.getInvoiceNumber().toString());

        // die SVG als Verkäufer
        AbstractSvgBo svgBo = (AbstractSvgBo) svgRepository.findBySvgNumber(invoiceBo.getSvgNumber()).orElseThrow(EntityNotFoundException::new);
        invoice.setSeller(InvoiceServiceUtil.mapToSvgCompany(svgBo));

        // den Kunden als Käufer
        final Long customerId = invoiceBo.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();
        invoice.setBuyer( InvoiceServiceUtil.mapToInvoiceCustomer(customerBo, invoiceAddressBo));


        // die Payment Terms.
        PaymentType paymentType = InvoiceServiceUtil.getPaymentType(invoiceBo.getCustomerType());
        BankAccount buyersBankAccount = null;
        BankAccount sellersBankAccount = null;
        String sepaMandantNumber= null;
        if (paymentType != PaymentType.BANK_TRANSFER) {
            BankAccountBo buyersBankAccountBo =
                    bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_TOLL)
                                    .orElseThrow(() -> new EntityNotFoundException("could not find customers bank account"));
            buyersBankAccount = new BankAccount();
            buyersBankAccount.setAccountName(buyersBankAccountBo.getAccountName());
            buyersBankAccount.setBic(buyersBankAccountBo.getBic());
            buyersBankAccount.setIban(buyersBankAccountBo.getIban());

            sepaMandantNumber = buyersBankAccountBo.getSepaMandatNumber();
        }
        if (paymentType == PaymentType.BANK_TRANSFER) {

            //TODO besser zwei neue Spalen BIC und IBAN
            String completeBankInformation = svgBo.getBankAccount();

            String iban = InvoiceServiceUtil.extractIbanFromBankInformationString(completeBankInformation);
            String bic = InvoiceServiceUtil.extractBicFromBankInformationString(completeBankInformation);

            sellersBankAccount = new BankAccount();
            sellersBankAccount.setIban(iban);
            sellersBankAccount.setBic(bic);
        }



        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.EURO_TOLL_ID, invoiceBo.getInvoiceDate());
        PaymentTerms paymentTerms = InvoiceServiceUtil.mapToPaymentTerms(buyersBankAccount, sellersBankAccount, sepaMandantNumber,  paymentType, dueDate, invoiceBo.getDebitComment());
        invoice.setPaymentTerms(paymentTerms);

        // die einzelnen Rechnungsposition der Sammelrechnung
        List<EuropeTollCountryInvoiceBo> itemBos = countryInvoiceRepository.findByCollectiveInvoiceId(invoiceBo.getId());

        List<Long> countryInvoiceNumbers = itemBos.stream().map(EuropeTollCountryInvoiceBo::getCountryInvoiceNumber).collect(Collectors.toList());

        List<EuropeTollCollectiveInvoiceItemView> items = collectiveInvoiceItemRepository.findByInvoiceNumberIn(countryInvoiceNumbers);
        invoice.setItems(items.stream().map(this::mapItemOfCollectiveInvoice).collect(Collectors.toList()));

        return invoice;

    }

    /** die Postionen aus der View. */
    private InvoiceItem mapItemOfCollectiveInvoice(EuropeTollCollectiveInvoiceItemView itemBo) {
        InvoiceItem item = new InvoiceItem();

        item.setTotalGrossPrice(itemBo.getTotalGrossPrice());
        item.setTotalNetPrice(itemBo.getTotalNetPrice());
        item.setVatAmount(itemBo.getVatAmount());
        item.setItemId(Long.valueOf(itemBo.getPosition()));
        item.setItemText(itemBo.getCountry());
        return item;

    }





    public SvgInvoice createCountryInvoice(Long invoiceNumber) {
        EuropeTollCountryEspInvoiceView espInvoiceView = countryEspInvoiceRepository.findByInvoiceNumber(invoiceNumber.toString())
                .orElseThrow(() -> new EntityNotFoundException("esp country invoice is not part of current verarbeitung"));;

        SvgInvoice invoice = new SvgInvoice();
        invoice.setInvoiceNumber(espInvoiceView.getInvoiceNumber().toString());
        invoice.setInvoiceDate(espInvoiceView.getInvoiceDate());
        invoice.setInvoiceType(InvoiceType.COUNTRY_INVOICE);

        //die Vertragspartner
        // die SVG als Verkäufer
        AbstractSvgBo svgBo = (AbstractSvgBo) svgRepository.findBySvgNumber(espInvoiceView.getSvgNumber()).orElseThrow(EntityNotFoundException::new);
        InvoiceSvgCompany invoiceSvgCompany = InvoiceServiceUtil.mapToSvgCompany(svgBo);
        invoice.setSeller(invoiceSvgCompany);

        //und der Kunde
        final Long customerId = espInvoiceView.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();
        invoice.setBuyer( InvoiceServiceUtil.mapToInvoiceCustomer(customerBo, invoiceAddressBo));

        //find the esp items
        List<EuropeTollEspInvoiceItemBo> espItemBos = espInvoiceItemRepository.findByEspInvoiceId(espInvoiceView.getId());
        invoice.setItems(espItemBos.stream().map(this::mapToEspInvoiceItems).collect(Collectors.toList()));

        List<String> comments = new ArrayList<>();
        comments.add("Leistungsart  e-Vignette  ist umsatzsteuerfrei");
        invoice.setComments(comments);

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.EURO_TOLL_ID, espInvoiceView.getInvoiceDate());
        //fake paymentTerms um xml-konform zu sein. Dabei word aber der PrepaidAmount gesetzt, so dass der zu zahlende Betrag auf 0 gesetzt wird.
        // dueDate must be set
        BankAccount sellersBankAccount = InvoiceServiceUtil.mapFromCompleteBankAccountString(svgBo.getBankAccount());
        PaymentTerms paymentTerms = new PaymentTerms();
        paymentTerms.setDueDate(dueDate);
        paymentTerms.setSellerAccount(sellersBankAccount);
        paymentTerms.setType(PaymentType.BANK_TRANSFER);
        paymentTerms.setPaymentAlreadyReceived(true);
        invoice.setPaymentTerms(paymentTerms);

        return invoice;
    }

    private InvoiceItem mapToEspInvoiceItems(EuropeTollEspInvoiceItemBo invoiceItemBo) {

        InvoiceItem item = new InvoiceItem();
        item.setItemId(invoiceItemBo.getId());
        item.setDescription("e-Vignette");
        item.setUnit(ItemUnit.PIECE);
        item.setTotalGrossPrice(invoiceItemBo.getNetPrice());
        item.setTotalNetPrice(invoiceItemBo.getNetPrice());
        item.setVatAmount(BigDecimal.ZERO);
        item.setVatRate(0);

        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("Kennzeichen", invoiceItemBo.getLicensePlate().trim());
        attributesMap.put("Schadstoffklasse", invoiceItemBo.getEmissionsClass());
        attributesMap.put("AK", invoiceItemBo.getAxleClass().equals("0") ? "Achsklasse 0 (bis 3 Achsen)" : "Achsklasse 1 (4 und mehr Achsen)");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy");
        attributesMap.put("Gültig von", formatter.format(invoiceItemBo.getValidFrom()));
        attributesMap.put("Gültig bis", formatter.format(invoiceItemBo.getValidTo()));
        attributesMap.put("Verkaufstelle", invoiceItemBo.getPointOfSale());
        item.setAttributes(attributesMap);
        return item;
    }

}
