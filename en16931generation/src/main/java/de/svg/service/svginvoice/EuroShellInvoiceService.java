package de.svg.service.svginvoice;

import de.svg.dto.SvgInvoice;
import de.svg.jpa.maut.dao.CustomerInvoiceAddressRepository;
import de.svg.jpa.maut.dao.CustomerRepository;
import de.svg.jpa.maut.dao.euroshell.*;
import de.svg.jpa.maut.dao.svgkunden.BankAccountRepository;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.jpa.maut.model.euroshell.ShellCollectiveInvoiceItemView;
import de.svg.jpa.maut.model.euroshell.ShellCollectiveInvoiceView;
import de.svg.jpa.maut.model.euroshell.ShellCountryInvoiceItemBo;
import de.svg.jpa.maut.model.euroshell.ShellCountryInvoiceView;
import de.svg.jpa.maut.model.svgkunden.BankAccountBo;
import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import de.svg.jpa.maut.model.svgkunden.ProcessingGroupBo;
import de.svg.service.DueDateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EuroShellInvoiceService  {

    private final ShellCollectiveInvoiceRepository collectiveInvoiceRepository;
    private final ShellCollectiveInvoiceItemRepository collectiveInvoiceItemRepository;
    private final ShellCountryInvoiceRepository countryInvoiceRepository;
    private final ShellSvgRepository svgRepository;
    private final ShellCountryInvoiceItemRepository countryInvoiceItemRepository;

    //SvgKunden Repos
    private final CustomerInvoiceAddressRepository customerInvoiceAddressRepository;
    private final CustomerRepository customerRepository;
    private final BankAccountRepository bankAccountRepository;

    //Services
    private final DueDateService dueDateService;


    public SvgInvoice createCollectiveInvoice(Long invoiceNumber) {
        // erst mal die Rechnung finden...
        ShellCollectiveInvoiceView invoiceBo = collectiveInvoiceRepository.findByInvoiceNumber(invoiceNumber)
                .orElseThrow(() -> new EntityNotFoundException("invoice is not part of current verarbeitung"));

        AbstractSvgBo svgBo = svgRepository.findBySvgNumber(invoiceBo.getSvgNumber()).orElseThrow(EntityNotFoundException::new);

        // den Kunden als Käufer
        final Long customerId = invoiceBo.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();

        BankAccountBo buyersBankAccountBo =
                bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_ESSO_AND_TOTAL)
                        .orElseGet(() -> bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_TOLL) //siehe View VW_BV2
                                .orElse(null));

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.SHELL_ID, invoiceBo.getInvoiceDate());


        // die einzelnen Rechnungsposition der Sammelrechnung
        List<ShellCollectiveInvoiceItemView> items = collectiveInvoiceItemRepository.findByInvoiceNumber(invoiceNumber);

        return InvoiceServiceUtil.mapFuelcardCollectiveInvoiceViewToInvoiceItem(invoiceBo, svgBo, customerBo, invoiceAddressBo, buyersBankAccountBo, dueDate, items );
    }


    public SvgInvoice createCountryInvoice(Long invoiceNumber) {

        ShellCountryInvoiceView totalCountryInvoiceView = countryInvoiceRepository.findByInvoiceNumber(invoiceNumber)
                .orElseThrow(() -> new EntityNotFoundException("country invoice is not part of current verarbeitung"));

        //die Vertragspartner
        // die SVG als Verkäufer
        AbstractSvgBo svgBo = (AbstractSvgBo) svgRepository.findBySvgNumber(totalCountryInvoiceView.getSvgNumber()).orElseThrow(EntityNotFoundException::new);

        //und der Kunde
        final Long customerId = totalCountryInvoiceView.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();

        //items
        List<ShellCountryInvoiceItemBo> itemBos = countryInvoiceItemRepository.findByCountryInvoiceId(totalCountryInvoiceView.getId());

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.SHELL_ID, totalCountryInvoiceView.getInvoiceDate());
        return InvoiceServiceUtil.mapFuelcardCountryInvoiceViewToInvoiceItem(totalCountryInvoiceView, svgBo, customerBo, invoiceAddressBo, itemBos, dueDate);

    }

}

