package de.svg.service.svginvoice;

import de.svg.dto.*;
import de.svg.jpa.bildung.dao.BankingAccountRepository;
import de.svg.jpa.bildung.dao.BildungInvoiceRepository;
import de.svg.jpa.bildung.dao.FibuRaRepository;
import de.svg.jpa.bildung.dao.ZugferdCustomerRepository;
import de.svg.jpa.bildung.model.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BildungInvoiceService {

    private static final Logger logger = LoggerFactory.getLogger(BildungInvoiceService.class);
    private final BildungInvoiceRepository bildungInvoiceRepository;
    private final ZugferdCustomerRepository zugferdCustomerRepository;
    private final FibuRaRepository fibuRaRepository;
    private final BankingAccountRepository bankingAccountRepository;

    public SvgInvoice createInvoice(Long invoiceId) {

        BildungInvoiceBo bildungInvoiceBo = bildungInvoiceRepository.findById(invoiceId)
                .orElseThrow(() -> new EntityNotFoundException("invoice not found"));


        SvgInvoice invoice = new SvgInvoice();

        List<String> commentsList = new ArrayList<>();
        commentsList.add(bildungInvoiceBo.getAdditionalText());
        invoice.setComments(commentsList);
        invoice.setInvoiceType(InvoiceType.COLLECTIVE_INVOICE);
        invoice.setInvoiceDate(bildungInvoiceBo.getInvoiceDate());
        invoice.setInvoiceNumber(bildungInvoiceBo.getInvoiceNumber());

        /** PaymentTerms */
        FibuRaBo fibuRa = fibuRaRepository.findById(bildungInvoiceBo.getFiburaId()).orElseThrow(() -> new EntityNotFoundException("fibuRa not found"));
        FibuBo fibu = fibuRa.getFibu();

        PaymentTerms paymentTerms = new PaymentTerms();
        paymentTerms.setDueDate(bildungInvoiceBo.getDueDate());
        paymentTerms.setDescription(null);
        //get the payment type:
        paymentTerms.setType(PaymentType.BANK_TRANSFER);

        String paymentDescription = bildungInvoiceBo.getPaymentTerms().getTerms() ;

        Optional<BankingAccountBo> bySvgNumberAndCustomerNumber = bankingAccountRepository.findBySvgNumberAndCustomerNumber(bildungInvoiceBo.getSvgNumber(), bildungInvoiceBo.getCustomerNumber());
        if (bySvgNumberAndCustomerNumber.isPresent()) {
            BankingAccountBo bankingAccountBo = bySvgNumberAndCustomerNumber.get();
            BankAccount buyersBankAccount = new BankAccount();
            buyersBankAccount.setBic(bankingAccountBo.getBic());
            buyersBankAccount.setIban(bankingAccountBo.getIban());
            buyersBankAccount.setBankName(bankingAccountBo.getBankName());
            paymentTerms.setBuyerAccount(buyersBankAccount);

            paymentDescription =StringUtils.replace(paymentDescription, "@bank", buyersBankAccount.toString());
        }

        if (bildungInvoiceBo.getDueDate() != null) {
            paymentDescription = StringUtils.replace(paymentDescription, "@fällig", bildungInvoiceBo.getDueDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        } else {
            paymentDescription = StringUtils.replace(paymentDescription, "bis zum @fällig", "");
        }
        paymentTerms.setDescription(paymentDescription);
        paymentTerms.setPaymentAlreadyReceived(StringUtils.containsIgnoreCase(paymentDescription, "erhalten")); //TODO hmmh nicht wirklich schön


        BankAccount sellersAccount = new BankAccount();
        sellersAccount.setIban(fibu.getIban());
        paymentTerms.setSellerAccount(sellersAccount);
        invoice.setPaymentTerms(paymentTerms);


        /** Buyer Customer*/
        ZugferdCustomerBo zugferdCustomerBo =
                zugferdCustomerRepository.findByCustomerNumberAndFiburaId(bildungInvoiceBo.getCustomerNumber(), bildungInvoiceBo.getFiburaId())
                        .orElseThrow(() -> new EntityNotFoundException("customer not found"));
        InvoiceCustomer buyer = new InvoiceCustomer();
        buyer.setZipCode(zugferdCustomerBo.getZipCode());
        buyer.setCity(zugferdCustomerBo.getCity());
        buyer.setName(zugferdCustomerBo.getCustomerName());
        buyer.setStreet(zugferdCustomerBo.getStreet());
        buyer.setIsoCountryCode("DE"); //TODO fehlt im Anschriftsstring
        buyer.setId(zugferdCustomerBo.getCustomerNumber());
        buyer.setGlobalReferenceNumber(zugferdCustomerBo.getGlobalReferenceNumber());
        buyer.setDescription(null);
        buyer.setFiscalNumber(null);
        buyer.setVatNumber(bildungInvoiceBo.getVatNumber());

        invoice.setBuyer(buyer);


        //Seller: The SVG
        InvoiceSvgCompany seller = new InvoiceSvgCompany();
        seller.setFiscalNumber(fibu.getTaxNumber());
        seller.setVatNumber(fibu.getVatNumber());
        seller.setZipCode(fibu.getZipCode());
        seller.setIsoCountryCode("DE");
        seller.setStreet(fibu.getStreet());
        seller.setCity(fibu.getCity());
        seller.setName(fibu.getSvgNumber());
        seller.setDescription(fibu.getSvgNumber());
        seller.setId(fibu.getSvgNumber());

        InvoiceContactPerson sellerContact = new InvoiceContactPerson();
        sellerContact.setEmail(fibu.getContactMail() != null ? fibu.getContactMail() : "n/a");
        sellerContact.setPhone(fibu.getContactPhone() != null ? fibu.getContactPhone() : "n/a");
        sellerContact.setName(fibu.getContactName() != null ? fibu.getContactName() : "n/a");
        sellerContact.setFax(fibu.getContactFax());
        seller.setContact(sellerContact);

        invoice.setSeller(seller);


        /** Invoice Items */
        invoice.setItems(bildungInvoiceBo.getItems().stream().map(this::mapToInvoiceItem).collect(Collectors.toList()));


        return invoice;

    }

    private InvoiceItem mapToInvoiceItem(BildungInvoiceItemBo invoiceItemBo) {
        InvoiceItem item = new InvoiceItem();

        BigDecimal grossPrice = invoiceItemBo.getTotalNetPrice().add(invoiceItemBo.getVatAmount());

        item.setItemId(invoiceItemBo.getItemPosition().longValue());
        item.setItemText(invoiceItemBo.getDescription());
        item.setTotalGrossPrice(grossPrice);
        item.setAttributes(null);
        item.setTotalNetPrice(invoiceItemBo.getTotalNetPrice());
        item.setDescription(invoiceItemBo.getDescription());
        item.setPercentage(null);
        item.setQuantity(invoiceItemBo.getQuantity());
        item.setUnit(null);
        item.setUnitNetPrice(invoiceItemBo.getUnitNetPrice());
        item.setVatAmount(invoiceItemBo.getVatAmount());
        item.setVatRate(invoiceItemBo.getVatRate());
        item.setRebate(null);
        item.setSurcharge(null);
        return item;
    }
}
