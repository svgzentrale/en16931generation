package de.svg.service.svginvoice;

import de.svg.dto.InvoiceItem;
import de.svg.dto.ItemUnit;
import de.svg.jpa.maut.dao.settlement.CountryInvoiceItemRepo;
import de.svg.jpa.maut.dao.settlement.TollItemRepository;
import de.svg.jpa.maut.model.settlement.tp.TollItemBo;
import de.svg.jpa.maut.model.settlement.tp.countryitem.EeetsCountry;
import de.svg.jpa.maut.model.settlement.tp.countryitem.TpCountryInvoiceItem;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SettlementTpCountryInvoiceItemService {

    private final JpaRepositoryFactory jpaRepositoryFactory;

    /** get the items for the country invoice. */
    public List<InvoiceItem> getCountryInvoiceItems(Long countryInvoiceId, Integer countryId) {

        CountryInvoiceItemRepo repository = jpaRepositoryFactory.getRepository(EeetsCountry.getByCountryId(countryId).getRepoClazz());
        //CountryInvoiceItemRepo repository = jpaRepositoryFactory.getRepository(TollItemRepository.class);

        List itemBos = repository.findByCountryInvoiceId(countryInvoiceId);

        //Maut-Items
        List<TollItemBo> tollItems = getTypeSafeItemList(itemBos, TollItemBo.class);
        List<InvoiceItem> items = tollItems.stream().map(this::mapFromTollItems).collect(Collectors.toList());
        //Country-Specific-Items
        List<TpCountryInvoiceItem> countrySpecificItems = getTypeSafeItemList(itemBos, TpCountryInvoiceItem.class);
        items.addAll(countrySpecificItems.stream().map(this::mapToTpCountryInvoiceItems).collect(Collectors.toList()));

        return items;
    }

    public <V> List<V> getTypeSafeItemList(List<Object> input, Class<V> cls) {
        return input.stream()
                .filter(cls::isInstance)
                .map(cls::cast)
                .collect(Collectors.toList());
    }

    private InvoiceItem mapFromTollItems(TollItemBo invoiceItemBo) {
        InvoiceItem item = new InvoiceItem();
        item.setItemId(invoiceItemBo.getId());

        item.setItemText(invoiceItemBo.getLicensePlate());
        item.setDescription(invoiceItemBo.getDocumentNumber());

        item.setTotalGrossPrice(invoiceItemBo.getGrossAmount());
        item.setTotalNetPrice(invoiceItemBo.getNetAmount());
        item.setVatRate(invoiceItemBo.getVatRate());
        item.setVatAmount(invoiceItemBo.getVatAmount());
        item.setUnitNetPrice(invoiceItemBo.getNetAmount());
        item.setUnit(ItemUnit.PIECE);
        item.setQuantity(BigDecimal.ONE);

        return item;
    }

    private InvoiceItem mapToTpCountryInvoiceItems(TpCountryInvoiceItem invoiceItemBo) {
        InvoiceItem item = new InvoiceItem();
        item.setItemId(invoiceItemBo.getVatRate().longValue());
        item.setTotalGrossPrice(invoiceItemBo.getGrossAmount());
        item.setUnitGrossPrice(invoiceItemBo.getGrossAmount());

        item.setTotalNetPrice(invoiceItemBo.getNetAmount());
        item.setUnitNetPrice(invoiceItemBo.getNetAmount());

        item.setVatAmount(invoiceItemBo.getVatAmount());
        item.setVatRate(invoiceItemBo.getVatRate());

        item.setItemText(invoiceItemBo.getItemText());
        item.setDescription(invoiceItemBo.getItemDescription());

        item.setAttributes(invoiceItemBo.getAdditionalAttributes());

        return item;
    }


}
