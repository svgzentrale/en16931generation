package de.svg.service.svginvoice;

import de.svg.controller.ProcessingGroupAbbreviation;
import de.svg.dto.*;
import de.svg.jpa.maut.dao.settlement.*;
import de.svg.jpa.maut.model.settlement.*;
import de.svg.jpa.maut.model.settlement.fuelcard.FuelcardInvoiceItemView;
import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.ProcessingGroupBo;
import de.svg.service.DueDateService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SettlementInvoiceService {

    private final SettlementTollCollecticeInvoiceViewRepository settlementTollCollecticeInvoiceViewRepository;
    private final SettlementTollCountryInvoiceViewRepository settlementTollCountryInvoiceViewRepository;
    private final SettlementInvoiceHeaderRepository settlementInvoiceHeaderRepository;
    private final SepaMandatInfoRepository sepaMandatInfoRepository;
    private final FuelcardInvoiceItemRepository fuelcardInvoiceItemRepository;
    private final VatCorrectionRepository vatCorrectionRepository;

    private final DueDateService dueDateService;
    private final SvgRepository svgRepository;
    private final SettlementTpCountryInvoiceItemService tpCountryInvoiceItemService;



/**
 * creation of SvgInvoice object of the collective invoice.
 * */
    public SvgInvoice createCollectiveInvoice(ProcessingGroupAbbreviation processingGroupAbbreviation, Long invoiceNumber) {

        SvgInvoice invoice = new SvgInvoice();
        CollecticeInvoiceView collecticeInvoiceView = settlementTollCollecticeInvoiceViewRepository
                .findByInvoiceNumberAndProcessingGroupId(invoiceNumber, processingGroupAbbreviation.getId())
                .orElseThrow(()-> new EntityNotFoundException("invoice not present in view: " + processingGroupAbbreviation + ", " + invoiceNumber));

        invoice.setInvoiceType(InvoiceType.COLLECTIVE_INVOICE);
        invoice.setInvoiceNumber(collecticeInvoiceView.getInvoiceNumber().toString());
        invoice.setInvoiceDate(collecticeInvoiceView.getInvoiceDate());

        // customer
        CustomerBo customerBo = collecticeInvoiceView.getCustomer();
        InvoiceCustomer customer = InvoiceServiceUtil.mapToInvoiceCustomer(customerBo, collecticeInvoiceView.getAddress());
        invoice.setBuyer(customer);

        // Svg as seller trade party
        //TODO svg nummer nicht über den customer
        SettlementSvgView svgView = svgRepository.findByIdSvgNumberAndIdProcessingGroupId(customerBo.getSvgNumber(), processingGroupAbbreviation.getId())
                .orElseThrow(() -> new EntityNotFoundException("svg must be present for number and processing group"));
        InvoiceSvgCompany svg = mapSvgCompany(svgView);

        invoice.setSeller(svg);

        Long customerId = collecticeInvoiceView.getCustomer().getId();

        // die Payment Terms.
        Integer customerType = settlementInvoiceHeaderRepository.findById(collecticeInvoiceView.getId()).get()
                .getCustomerType();
        PaymentType paymentType = InvoiceServiceUtil.getPaymentType(customerType);
        BankAccount buyersBankAccount = null;
        BankAccount sellersBankAccount = null;
        String sepaMandantNumber= null;
        if (paymentType != PaymentType.BANK_TRANSFER) {
            SepaMandatInfoView buyersBankAccountBo =
                    sepaMandatInfoRepository.findByCustomerId(customerId)
                            .orElseThrow(() -> new EntityNotFoundException("could not find customers sepa info"));
            buyersBankAccount = new BankAccount();
            buyersBankAccount.setBic(buyersBankAccountBo.getBic());
            buyersBankAccount.setIban(buyersBankAccountBo.getIban());

            sepaMandantNumber = buyersBankAccountBo.getSepaMandatNumber();
        }
        if (paymentType == PaymentType.BANK_TRANSFER) {
            sellersBankAccount = new BankAccount();
            sellersBankAccount.setIban(svgView.getSvgBo() != null ? svgView.getSvgBo().getIban() : null);
            sellersBankAccount.setBic(svgView.getSvgBo() != null ? svgView.getSvgBo().getBic() : null);
        }


        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.SVG_FLEXBOX_ID, collecticeInvoiceView.getInvoiceDate());
        PaymentTerms paymentTerms = InvoiceServiceUtil.mapToPaymentTerms(buyersBankAccount, sellersBankAccount, sepaMandantNumber,  paymentType, dueDate, collecticeInvoiceView.getPaymentText());
        invoice.setPaymentTerms(paymentTerms);

        //Info-Texte
        List<String> comments = new ArrayList<>();
        comments.add(collecticeInvoiceView.getHeaderDescription());
        comments.add(collecticeInvoiceView.getInvoiceInfoText());
        comments.add("gilt nicht als Umsatzsteuererstattungsbeleg (MwSt) und berechtigt nicht zum Vorsteuerabzug"); //TODO immer?
        invoice.setComments(comments);

        List<InvoiceItem> items = new ArrayList<>();
        collecticeInvoiceView.getItems().forEach(item -> items.add(mapToSvgInvoiceItems(item)));
        invoice.setItems(items);

        return invoice;
    }


    public SvgInvoice createCountryInvoice(Long invoiceNumber, ProcessingGroupAbbreviation processingGroupAbbreviation) {

        CountryInvoiceView countryInvoiceView = settlementTollCountryInvoiceViewRepository.findByInvoiceNumberAndProcessingGroupId(invoiceNumber, processingGroupAbbreviation.getId())
                .orElseThrow(()-> new EntityNotFoundException("invoice not present in country view: " + processingGroupAbbreviation + ", " + invoiceNumber));

//        if (StringUtils.containsIgnoreCase(countryInvoiceView.getInvoiceText(), "Abrechnung" ) ) {
//            throw new EntityNotFoundException("Abrechnung is not part of x-rechnung generation");
//        }

        SvgInvoice invoice = new SvgInvoice();
        invoice.setInvoiceType(InvoiceType.COUNTRY_INVOICE);
        invoice.setInvoiceNumber(countryInvoiceView.getInvoiceNumber().toString());
        invoice.setInvoiceDate(countryInvoiceView.getInvoiceDate());

        CustomerBo customerBo = countryInvoiceView.getCustomer();
        InvoiceCustomer customer = InvoiceServiceUtil.mapToInvoiceCustomer(customerBo, countryInvoiceView.getAddress());
        customer.setVatNumber(countryInvoiceView.getCustomerVatNumber());
        invoice.setBuyer(customer);

        SettlementSvgView svgView = svgRepository.findByIdSvgNumberAndIdProcessingGroupId(customerBo.getSvgNumber(), processingGroupAbbreviation.getId())
                .orElseThrow(() -> new EntityNotFoundException("svg must be present for number and processing group"));
        InvoiceSvgCompany svg = mapSvgCompany(svgView);
        invoice.setSeller(svg);

        List<String> comments = new ArrayList<>();
        comments.add(countryInvoiceView.getInvoiceText());
        comments.add(countryInvoiceView.getInvoiceTextPrintDate());
        invoice.setComments(comments);

        Integer countryId = countryInvoiceView.getCountryId();

        invoice.setCurrencyCode(countryInvoiceView.getCurrencyCode());
        //TP CountryInvoiceItems
        List<InvoiceItem> items = new ArrayList<>(tpCountryInvoiceItemService.getCountryInvoiceItems(countryInvoiceView.getId(), countryId));

        //DKV Fuelcard Items
        List<FuelcardInvoiceItemView> fuelcardItems = fuelcardInvoiceItemRepository.findByCountryInvoiceId(countryInvoiceView.getId());
        items.addAll(fuelcardItems.stream().map(this::mapToDkvCountryInvoiceItems).collect(Collectors.toList()));
        invoice.setItems(items);

        //add vat corrections
        invoice.setVatCorrections(
                vatCorrectionRepository.findByIdCountryInvoiceId(countryInvoiceView.getId())
                        .stream()
                        .map(this::mapToVatCorrection)
                        .collect(Collectors.toList()));

        //fake paymentTerms um xml-konform zu sein. Dabei word aber der PrepaidAmount gesetzt, so dass der zu zahlende Betrag auf 0 gesetzt wird.
        // dueDate must be set
        //PaymentTerms paymentTerms = mapToPaymentTerms(buyersBankAccount, sellersBankAccount, sepaMandantNumber,  paymentType, dueDate, invoiceBo.getDebitComment());
        BankAccount sellersBankAccount = new BankAccount();
        sellersBankAccount.setIban(svgView.getSvgBo() != null ? svgView.getSvgBo().getIban() : null);
        sellersBankAccount.setBic(svgView.getSvgBo() != null ? svgView.getSvgBo().getBic() : null);

        PaymentTerms paymentTerms = new PaymentTerms();
        paymentTerms.setSellerAccount(sellersBankAccount);
        paymentTerms.setType(PaymentType.BANK_TRANSFER);
        paymentTerms.setPaymentAlreadyReceived(true);
        paymentTerms.setDueDate(countryInvoiceView.getDueDate());
        invoice.setPaymentTerms(paymentTerms);

        return invoice;
    }

    private VatCorrection mapToVatCorrection(VatCorrectionView vatCorrectionView) {
        VatCorrection vatCorrection = new VatCorrection();
        vatCorrection.setCorrectionGrossAmount(vatCorrectionView.getCorrectionTotalGrossAmount());
        vatCorrection.setCorrectionVatAmount(vatCorrectionView.getCorrectionVatAmount());
        vatCorrection.setVatRate(vatCorrectionView.getId().getVatRate().getVatRate());
        return vatCorrection;
    }

    private InvoiceSvgCompany mapSvgCompany(SettlementSvgView svgView) {


        //SVG als Rechnungssteller
        InvoiceSvgCompany svg = new InvoiceSvgCompany();
        svg.setDescription(svgView.getAddressLine1());

        //the last address field is zip and city

        String zipAndCity;
        String street;
        String name;
        if (svgView.getAddressLine4()!=null) {
            zipAndCity = svgView.getAddressLine4();
            street= svgView.getAddressLine3();
            name = svgView.getAddressLine1() + " " + svgView.getAddressLine2();
        } else {
            zipAndCity = svgView.getAddressLine3();
            street= svgView.getAddressLine2();
            name = svgView.getAddressLine1();
        }
        svg.setName(name);
        svg.setStreet(street);
        svg.setIsoCountryCode("DE");
        svg.setCity(StringUtils.substringAfter(zipAndCity, " "));
        svg.setZipCode(StringUtils.substringBefore(zipAndCity, " "));

        //Ansprechpartner
        InvoiceContactPerson invoiceContactPerson = InvoiceServiceUtil.mapToInvoiceContactPerson(
                svgView.getContactPerson(),
                svgView.getContactMail(),
                svgView.getContactPhone(),
                svgView.getContactFax());
        svg.setContact(invoiceContactPerson);

        svg.setVatNumber(svgView.getVatNumber());
        svg.setId(svgView.getId().getSvgNumber());
        return  svg;
    }

    private InvoiceItem mapToSvgInvoiceItems(CollecticeInvoiceItemView invoiceItemBo) {
        InvoiceItem item = new InvoiceItem();
        item.setItemId(invoiceItemBo.getId());
        item.setItemText(invoiceItemBo.getCountry());

        //keien Umsatzsteuer. Die befindet sich auf den Laenderrechnungen. Deswegn überall den GrossPrice verwenden
        //nettopreis ist Pflichtfeld
        item.setUnitNetPrice(invoiceItemBo.getTotalGrossAmount());
        item.setUnitGrossPrice(invoiceItemBo.getTotalGrossAmount());
        item.setQuantity(BigDecimal.ONE);
        item.setTotalGrossPrice(invoiceItemBo.getTotalGrossAmount());
        item.setTotalNetPrice(invoiceItemBo.getTotalGrossAmount());
        item.setTaxType(TaxType.NULLRATE);

        Map<String, String> attributesMap = new HashMap<>();
        attributesMap.put("Abrechnungs-/Rechnungsnummer", invoiceItemBo.getId().toString());
        attributesMap.put("Währung Serviceland", invoiceItemBo.getCurrency());
        String totalGrossAmountFwString = NumberFormat.getNumberInstance(Locale.getDefault()).format(invoiceItemBo.getTotalGrossAmountFW());
        attributesMap.put("Gesamwert in Servicelandwährung inkl. Ust.", totalGrossAmountFwString);
        item.setAttributes(attributesMap);

        return item;
    }


    private InvoiceItem mapToDkvCountryInvoiceItems(FuelcardInvoiceItemView invoiceItemBo) {
        InvoiceItem item = new InvoiceItem();
        item.setItemId(invoiceItemBo.getId());

        item.setItemText(invoiceItemBo.getGoodsCode());
        item.setDescription(invoiceItemBo.getGoodsCodeText());

        //einzelpreis
        item.setUnitNetPrice(invoiceItemBo.getUnitNetPrice());
        item.setUnitGrossPrice(invoiceItemBo.getUnitGrossPrice());
        //Gesamtpreis
        item.setTotalGrossPrice(invoiceItemBo.getTotalGrossPrice());
        item.setTotalNetPrice(invoiceItemBo.getTotalNetPrice());
        //Ust-Values
        item.setVatAmount(invoiceItemBo.getVatAmount());
        item.setVatRate(invoiceItemBo.getVatRate());


        Map<String, String> attributesMap = new HashMap<>();
            attributesMap.put("Kennzeichen", invoiceItemBo.getLicensePlate().trim());
            attributesMap.put("PAN", invoiceItemBo.getPan());
        if (invoiceItemBo.getDeliveryNumber() != null) {
            attributesMap.put("Transaktionsnummer", invoiceItemBo.getDeliveryNumber());
        }
        if (invoiceItemBo.getDeliveryDate() != null) {
            attributesMap.put("Lieferdatum", invoiceItemBo.getDeliveryDate().toString());
        }
        if (invoiceItemBo.getMileage() != null) {
            attributesMap.put("KM-Stand", invoiceItemBo.getMileage().toString());
        }
        item.setAttributes(attributesMap);
        return item;
    }

}
