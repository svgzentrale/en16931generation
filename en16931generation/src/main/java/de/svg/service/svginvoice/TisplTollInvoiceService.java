package de.svg.service.svginvoice;

import de.svg.dto.*;
import de.svg.jpa.maut.dao.svgkunden.BankAccountRepository;
import de.svg.jpa.maut.dao.tispl.TisplSvgRepository;
import de.svg.jpa.maut.dao.tispl.TisplTollInvoiceHeaderRepository;
import de.svg.jpa.maut.model.svgkunden.BankAccountBo;
import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.ProcessingGroupBo;
import de.svg.jpa.maut.model.tispl.TisplSvgBo;
import de.svg.jpa.maut.model.tispl.TisplTollInvoiceHeaderBo;
import de.svg.jpa.maut.model.tispl.TisplTollInvoiceItemEtBo;
import de.svg.jpa.maut.model.tispl.TisplTollInvoiceItemSvgBo;
import de.svg.service.DueDateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TisplTollInvoiceService {

    private final TisplTollInvoiceHeaderRepository tisplTollInvoiceHeaderRepository;
    private final TisplSvgRepository tisplSvgRepository;
    private final BankAccountRepository bankAccountRepository;
    private final DueDateService dueDateService;

    /** creates  en16931 - xml - files*/
    @Transactional
    public SvgInvoice createInvoice(Long invoiceNumber) {


        TisplTollInvoiceHeaderBo invoiceHeaderBo = tisplTollInvoiceHeaderRepository.findByInvoiceNumber(invoiceNumber).orElseThrow(EntityNotFoundException::new);

        // customer
        CustomerBo customerBo = invoiceHeaderBo.getCustomer();

        InvoiceCustomer customer = InvoiceServiceUtil.mapToInvoiceCustomer(customerBo, invoiceHeaderBo.getAddress());

        // Svg as seller trade party
        TisplSvgBo svgBo = tisplSvgRepository.findBySvgNumber(customerBo.getSvgNumber()).orElseThrow(() -> new EntityNotFoundException("svg not found" + customerBo.getSvgNumber()));
        InvoiceSvgCompany svg = InvoiceServiceUtil.mapToSvgCompany(svgBo);


        //first step is to create in countryInvoices as they are the invoice item of the global invoice
        return mapToTisplInvoice(invoiceHeaderBo, customer, svg);
    }



    /** returning a SvgInvoice mapped from (Total)CountryInvoiceBo and the trade parties. */
    private SvgInvoice mapToTisplInvoice(TisplTollInvoiceHeaderBo tisplTollInvoiceHeaderBo, InvoiceCustomer customer, InvoiceSvgCompany svg) {
        SvgInvoice invoice = new SvgInvoice();

        invoice.setInvoiceType(InvoiceType.COUNTRY_INVOICE);
        invoice.setBuyer(customer);
        invoice.setSeller(svg);


        PaymentTerms paymentTerms = getPaymentTerms(tisplTollInvoiceHeaderBo.getCustomer().getId(), tisplTollInvoiceHeaderBo.getInvoiceDate());
        invoice.setPaymentTerms(paymentTerms);


        invoice.setInvoiceNumber(tisplTollInvoiceHeaderBo.getInvoiceNumber().toString());
        invoice.setInvoiceDate(tisplTollInvoiceHeaderBo.getInvoiceDate());
        //invoice.setComments(Arrays.asList("Bemerkungstext1 zur Rechnung1", "Bemerkungstext2 zur Rechnung1"));

        List<InvoiceItem> items = new ArrayList<>();
        //Abrechnung Fremdleistung
        tisplTollInvoiceHeaderBo.getEtInvoiceItems().forEach(item -> items.add(mapToEtInvoiceItems(item)));

        //in den eigenen wird ein Datensatz als Mehrwertsteuersatz verwendet.
        BigDecimal vatRate = tisplTollInvoiceHeaderBo.getSvgInvoiceItems()
                .stream()
                .filter(svgitem -> 3L == svgitem.getItemPositionText().getId())
                .findAny()
                .map(TisplTollInvoiceItemSvgBo::getPercentage)
                .orElse(BigDecimal.ZERO);

        items.addAll(tisplTollInvoiceHeaderBo.getSvgInvoiceItems()
                .stream()
                .filter(svgitem -> 3L != svgitem.getItemPositionText().getId())
                .map(svgItem -> mapToSvgInvoiceItems(svgItem, vatRate.intValue()))
                .collect(Collectors.toList())
        );


        invoice.setItems(items);

        return invoice;

    }

    /** get the payment terms of the customer */
    private PaymentTerms getPaymentTerms(Long customerId, LocalDate invoiceDate) {
        PaymentTerms paymentTerms = new PaymentTerms();
        paymentTerms.setType(PaymentType.SEPA);

        // BankAccount Type for Total is 2, als Fallback das MautKonto mit type 1 verwenden
        BankAccountBo buyersBankAccountBo =
                bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_ESSO_AND_TOTAL)
                        .orElseGet(() -> bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_TOLL)
                                .orElseThrow(EntityNotFoundException::new));

        //Angabe zur Bankverbindung des Rechnungsempfängers:
        BankAccount buyersBankAccount = new BankAccount();
        buyersBankAccount.setIban(buyersBankAccountBo.getIban());
        buyersBankAccount.setBic(buyersBankAccountBo.getBic());
        buyersBankAccount.setAccountName(buyersBankAccountBo.getAccountName());
        paymentTerms.setBuyerAccount(buyersBankAccount);

        //sepa mandat number
        paymentTerms.setSepaMandat(buyersBankAccountBo.getSepaMandatNumber());
        paymentTerms.setDueDate(dueDateService.getDueDate(customerId, ProcessingGroupBo.TIS_PL_ID, invoiceDate));
        return paymentTerms;

    }

    /**
     *
     */
    private InvoiceItem mapToSvgInvoiceItems(TisplTollInvoiceItemSvgBo invoiceItemBo, Integer vatRate) {
        InvoiceItem item = new InvoiceItem();
        item.setItemId(invoiceItemBo.getId());
        item.setDescription(invoiceItemBo.getPercentage().toString() + "%");
        BigDecimal net = invoiceItemBo.getAmount();
        item.setItemText(invoiceItemBo.getItemPositionText().getDescription());
        item.setVatRate(vatRate);
        //calculate the vatAmount and the gross
        long z = 100L + vatRate;

        BigDecimal gross =  net.multiply(BigDecimal.valueOf(z)).divide(BigDecimal.valueOf(100L), 2, RoundingMode.HALF_UP);
        item.setTotalNetPrice(net);
        item.setUnitNetPrice(net);
        item.setTotalGrossPrice(gross);
        BigDecimal vatAmount = gross.subtract(net);
        item.setVatAmount(vatAmount);

        return item;
    }

    private InvoiceItem mapToEtInvoiceItems(TisplTollInvoiceItemEtBo invoiceItemBo) {
        InvoiceItem item = new InvoiceItem();
        item.setItemId(invoiceItemBo.getId());
        item.setDescription("Abrechnung Fremdleistungen Eurotoll TIS PL-Rechnungen");
        item.setItemText(invoiceItemBo.getInvoiceNumber());
        item.setVatAmount(BigDecimal.ZERO); //no vat on these Fremdleistungen
        item.setVatRate(0);
        item.setTaxType(TaxType.NULLRATE);
        item.setTotalGrossPrice(invoiceItemBo.getAmount());
        item.setTotalNetPrice(invoiceItemBo.getAmount());
        item.setUnitNetPrice(invoiceItemBo.getAmount());
        item.setUnitGrossPrice(invoiceItemBo.getAmount());
        return item;
    }
}
