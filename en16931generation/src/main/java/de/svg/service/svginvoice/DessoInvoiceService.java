package de.svg.service.svginvoice;

import de.svg.dto.PaymentTerms;
import de.svg.dto.SvgInvoice;
import de.svg.jpa.maut.dao.CustomerInvoiceAddressRepository;
import de.svg.jpa.maut.dao.CustomerRepository;
import de.svg.jpa.maut.dao.desso.*;
import de.svg.jpa.maut.dao.svgkunden.BankAccountRepository;
import de.svg.jpa.maut.model.AbstractSvgBo;
import de.svg.jpa.maut.model.desso.*;
import de.svg.jpa.maut.model.svgkunden.BankAccountBo;
import de.svg.jpa.maut.model.svgkunden.CustomerBo;
import de.svg.jpa.maut.model.svgkunden.InvoiceAddressBo;
import de.svg.jpa.maut.model.svgkunden.ProcessingGroupBo;
import de.svg.service.DueDateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DessoInvoiceService {

    private final static String HUNGARY = "Ungarn";

    private final DessoCollectiveInvoiceRepository collectiveInvoiceRepository;
    private final DessoSvgRepository svgRepository;
    private final DessoCollectiveInvoiceItemRepository collectiveInvoiceItemRepository;
    private final DessoCountryInvoiceRepository countryInvoiceRepository;
    private final DessoCountryInvoiceItemRepository countryInvoiceItemRepository;

    private final CustomerInvoiceAddressRepository customerInvoiceAddressRepository;
    private final CustomerRepository customerRepository;
    private final BankAccountRepository bankAccountRepository;

    private final DueDateService dueDateService;


    public SvgInvoice createCollectiveInvoice(Long invoiceNumber) {

        // erst mal die Rechnung finden...
        DessoCollectiveInvoiceView invoiceBo = collectiveInvoiceRepository.findByInvoiceNumber(invoiceNumber)
                .orElseThrow(() -> new EntityNotFoundException("invoice is not part of current verarbeitung"));

        DessoSvgBo svgBo = svgRepository.findBySvgNumber(invoiceBo.getSvgNumber()).orElseThrow(EntityNotFoundException::new);

        // den Kunden als Käufer
        final Long customerId = invoiceBo.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();

        BankAccountBo buyersBankAccountBo =
                bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_ESSO_AND_TOTAL)
                        .orElseGet(() -> bankAccountRepository.findByCustomerIdAndType(customerId, BankAccountBo.TYPE_TOLL) //siehe View VW_BV2
                                .orElse(null));

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.ESSO_ID, invoiceBo.getInvoiceDate());


        // die einzelnen Rechnungsposition der Sammelrechnung
        List<DessoCollectiveInvoiceItemView> items = collectiveInvoiceItemRepository.findByInvoiceNumber(invoiceNumber);

        return InvoiceServiceUtil.mapFuelcardCollectiveInvoiceViewToInvoiceItem(invoiceBo, svgBo, customerBo, invoiceAddressBo, buyersBankAccountBo, dueDate, items );

    }

    public SvgInvoice createCountryInvoice(Long invoiceNumber) {

        DessoCountryInvoiceView dessoCountryInvoiceView = countryInvoiceRepository.findByInvoiceNumber(invoiceNumber)
                .orElseThrow(() -> new EntityNotFoundException("country invoice is not part of current verarbeitung"));

        //die Vertragspartner
        // die SVG als Verkäufer
        AbstractSvgBo svgBo = (AbstractSvgBo) svgRepository.findBySvgNumber(dessoCountryInvoiceView.getSvgNumber()).orElseThrow(EntityNotFoundException::new);

        //und der Kunde
        final Long customerId = dessoCountryInvoiceView.getCustomerId();
        InvoiceAddressBo invoiceAddressBo = customerInvoiceAddressRepository.findByCustomerId(customerId).get();
        CustomerBo customerBo = customerRepository.findById(customerId).get();

        //items
        List<DessoInvoiceItemBo> itemBos = countryInvoiceItemRepository.findByCountryInvoiceId(dessoCountryInvoiceView.getId());

        LocalDate dueDate = dueDateService.getDueDate(customerId, ProcessingGroupBo.ESSO_ID, dessoCountryInvoiceView.getInvoiceDate());
        SvgInvoice svgInvoice = InvoiceServiceUtil.mapFuelcardCountryInvoiceViewToInvoiceItem(dessoCountryInvoiceView, svgBo, customerBo, invoiceAddressBo, itemBos, dueDate);
        svgInvoice.setLobsterExport(HUNGARY.equals(dessoCountryInvoiceView.getCountryNameGerman()));
        return svgInvoice;

    }

}

