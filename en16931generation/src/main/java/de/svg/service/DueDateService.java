package de.svg.service;

import de.svg.jpa.maut.dao.svgkunden.BillingInformationRepository;
import de.svg.jpa.maut.model.svgkunden.BillingInformationBo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DueDateService {

    private final BillingInformationRepository billingInformationRepository;

    private Integer getPaymentTarget(Long customerId, Long processingGroupId) {
        Optional<BillingInformationBo> opt = billingInformationRepository.findByCustomerIdAndProcessingGroupId(customerId, processingGroupId);
        if (opt.isPresent()) {
            BillingInformationBo billingInformationBo = opt.get();
            return billingInformationBo.getPaymentTarget() != null ? billingInformationBo.getPaymentTarget() : billingInformationBo.getProcessingGroup().getMaxPaymentTarget();
        }
        return 0;
    }

    /** ermittelt das Zahlungsziel des Kunden und addiert dieses (Anzahl Tage) auf das Rechnungsdatum. */
    public LocalDate getDueDate(Long customerId, Long processingGroupId, LocalDate invoiceDate) {

        return invoiceDate.plusDays(getPaymentTarget(customerId, processingGroupId));

    }

}
