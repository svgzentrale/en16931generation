package de.svg.service.en16931;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDEmbeddedFilesNameTreeNode;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import org.apache.pdfbox.pdmodel.common.filespecification.PDEmbeddedFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Service
public class PdfA3Service {

    public byte[] attachFile(MultipartFile file, InputStream attachementStream) throws IOException {
        PDDocument doc = PDDocument.load(file.getBytes());

        final PDEmbeddedFile embeddedFile = new PDEmbeddedFile(doc, attachementStream);
        embeddedFile.setSubtype("application/xml");

        final PDComplexFileSpecification fileSpecification = new PDComplexFileSpecification();
        fileSpecification.setFile("factur-x.xml");
        fileSpecification.setEmbeddedFile(embeddedFile);

        final Map<String, PDComplexFileSpecification> embeddedFileMap = new HashMap<>();
        embeddedFileMap.put("factur-x.xml", fileSpecification);

        final PDEmbeddedFilesNameTreeNode efTree = new PDEmbeddedFilesNameTreeNode();
        efTree.setNames(embeddedFileMap);

        final PDDocumentNameDictionary names = new PDDocumentNameDictionary(doc.getDocumentCatalog());
        names.setEmbeddedFiles(efTree);
        doc.getDocumentCatalog().setNames(names);


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        doc.save(baos);
        doc.close();
        // Files.write(Paths.get("/home/benjamin/svg/projekte/zugferd/out/total/filbentest.pdf"), baos.toByteArray());

        return baos.toByteArray();

    }

}
