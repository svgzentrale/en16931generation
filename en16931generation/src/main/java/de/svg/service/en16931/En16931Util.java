package de.svg.service.en16931;

import de.svg.api.en16931.*;
import de.svg.dto.RebateOrExtraCharge;
import de.svg.dto.RebateType;
import de.svg.dto.TaxType;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class En16931Util {

    public final static String DOCUMENT_CODE_TYPE_INVOICE = "380";
    //Type Gutschriftanzeige
    public final static String DOCUMENT_CODE_TYPE_CREDITADVICE = "381";
    //Type: Storno einer Blastung
    public final static String DOCUMENT_CODE_TYPE_STORNO = "457";

    public final static String CODE_TYPE_NOTE = "ADU";
    public final static String UNIT_LITRE = "LTR";
    public final static String UNIT_ONE = "C62";
    //CODE für die Zahlungsart
    // keine Zahlungsart definiert 1
    // SEPA Lastschrift 59 : SEPA Direct Debit
    // Lastschrift 49: DIRECT_DEBIT
    // Überweisung 30: BANK_TRANSFER
    public final static String PAYMENT_MEANS_NONE = "1";
    public final static String PAYMENT_MEANS_SEPA_DIRECT_DEBIT = "59";
    public final static String PAYMENT_MEANS_DIRECT_DEBIT = "49";
    public final static String PAYMENT_MEANS_BANK_TRANSFER = "30";

    public static NoteType noteFromText(String text) {
        NoteType noteType = new NoteType();
        noteType.setContent(fromText(text));
        CodeType codeType = new CodeType();
        codeType.setValue(CODE_TYPE_NOTE);
        noteType.setSubjectCode(codeType);
        return noteType;
    }

    public static TextType fromText(String text) {
        if (text == null) {
           return null;
        }
        TextType textType = new TextType();
        textType.setValue(text.trim());
        return textType;
    }

    public static TradePriceType priceFromValue(BigDecimal chargeAmount, RebateOrExtraCharge priceRebate, RebateOrExtraCharge surcharge, String unitCode) {
        TradePriceType tradePrice = new TradePriceType();

        // Preis des Artikels: Einzelpreis
        tradePrice.setChargeAmount(fromAmount(chargeAmount, null));
        tradePrice.setBasisQuantity(fromQuantity(BigDecimal.ONE, unitCode));

        // Nachlass
        if (priceRebate != null) {
            tradePrice.getAppliedTradeAllowanceCharge().add(allowanceCharge(priceRebate));
        }

        // Aufschlag
        if (surcharge != null) {
            tradePrice.getAppliedTradeAllowanceCharge().add(allowanceCharge(surcharge));
        }

        return tradePrice;
    }

    private static TradeAllowanceChargeType allowanceCharge(RebateOrExtraCharge priceRebate) {
        TradeAllowanceChargeType tradeAllowanceCharge = new TradeAllowanceChargeType();
        tradeAllowanceCharge.setReason(fromText(priceRebate.getReason()));
        tradeAllowanceCharge.setChargeIndicator(priceRebate.getType() == RebateType.REBATE ? indicatorFalse() : indicatorTrue());
        if (priceRebate.getValueIsPercentValue()) {
            tradeAllowanceCharge.setCalculationPercent(getPercent(priceRebate.getValue()));
        } else {
            tradeAllowanceCharge.setBasisAmount(fromAmount(priceRebate.getValue(), null));
            tradeAllowanceCharge.setBasisQuantity(fromQuantity(priceRebate.getBasisQunatity(), UNIT_LITRE));
        }
        return tradeAllowanceCharge;
    }

    private static PercentType getPercent(BigDecimal value) {

        PercentType percent = new PercentType();
        percent.setValue(value);
        return percent;
    }

    private static IndicatorType indicatorTrue() {
        IndicatorType inicator = new IndicatorType();
        inicator.setIndicator(Boolean.TRUE);
        return inicator;
    }

    private static IndicatorType indicatorFalse() {
        IndicatorType inicator = new IndicatorType();
        inicator.setIndicator(Boolean.FALSE);
        return inicator;
    }

    public static AmountType fromAmount(BigDecimal amount, String currencyCode) {
        AmountType amountType = new AmountType();
        amountType.setValue(amount);
        amountType.setCurrencyID(currencyCode);
        return amountType;
    }

    public static QuantityType fromQuantity(BigDecimal quantity, String unitCode) {
        QuantityType quantityType = new QuantityType();
        quantityType.setUnitCode(unitCode);
        quantityType.setValue(quantity);
        return quantityType;
    }

    public static DateTimeType fromLocalDate(LocalDate date) {
        DateTimeType dateTimeType = new DateTimeType();
        DateTimeType.DateTimeString dateTimeString = new DateTimeType.DateTimeString();
        dateTimeString.setFormat("102");//        CCYYMMDD
        dateTimeString.setValue(date.format(DateTimeFormatter.BASIC_ISO_DATE));
        dateTimeType.setDateTimeString(dateTimeString);

        return dateTimeType;
    }


    public static DocumentCodeType getDocumentCodeTypeInvoice() {

        DocumentCodeType d = new DocumentCodeType();
        d.setValue(DOCUMENT_CODE_TYPE_INVOICE);
        return d;
    }

    public static DocumentCodeType getDocumentCodeTypeCreditAdvive() {

        DocumentCodeType d = new DocumentCodeType();
        d.setValue(DOCUMENT_CODE_TYPE_CREDITADVICE);
        return d;
    }

    public static DocumentCodeType getDocumentCodeTypeStorno() {

        DocumentCodeType d = new DocumentCodeType();
        d.setValue(DOCUMENT_CODE_TYPE_CREDITADVICE);
        return d;
    }

    public static PaymentMeansCodeType getPaymentMeansSepaDirectDebit() {

        PaymentMeansCodeType p = new PaymentMeansCodeType();
        p.setValue(PAYMENT_MEANS_SEPA_DIRECT_DEBIT);
        return p;
    }

    public static PaymentMeansCodeType getPaymentMeansNotDefined() {

        PaymentMeansCodeType p = new PaymentMeansCodeType();
        p.setValue(PAYMENT_MEANS_NONE);
        return p;
    }

    public static PaymentMeansCodeType getPaymentMeansDirectDebit() {

        PaymentMeansCodeType p = new PaymentMeansCodeType();
        p.setValue(PAYMENT_MEANS_DIRECT_DEBIT);
        return p;
    }

    public static PaymentMeansCodeType getPaymentMeansBankTransfer() {

        PaymentMeansCodeType p = new PaymentMeansCodeType();
        p.setValue(PAYMENT_MEANS_BANK_TRANSFER);
        return p;
    }

    public static CountryIDType fromIsoCounty(String isoCountryCode) {
        CountryIDType countryId = new CountryIDType();
        countryId.setValue(isoCountryCode);
        return countryId;
    }

    public static CurrencyCodeType getCurrency(String isoCountry) {
        CurrencyCodeType currencyCode = new CurrencyCodeType();
        currencyCode.setValue(CurrencyCodeContentType.fromValue(StringUtils.trim(isoCountry)));
        return currencyCode;
    }


    public static  IDType fromValue(String idValue, String idCode) {
        IDType id = new IDType();
        id.setValue(idValue);
        id.setSchemeID(idCode);
        return id;
    }


    public static PercentType fromPercent(Integer taxPercent) {
        PercentType percentType = new PercentType();
        percentType.setValue(BigDecimal.valueOf(taxPercent));
        return percentType;
    }

    /**
     * Steuerart „Umsatzsteuer“ mit dem Code „VAT“
     * */
    public static TaxTypeCodeType getTaxTypeCodeVat() {
        TaxTypeCodeType taxTypeCodeType = new TaxTypeCodeType();
        taxTypeCodeType.setValue(TaxTypeCodeContentType.VAT);
        return taxTypeCodeType;
    }

    /**
     *  Die Codes für die Umsatzsteuerkategorie sind Folgende:
     *  S = Umsatzsteuer fällt mit Normalsatz an
     *  Z = nach dem Nullsatz zu versteuernde Waren
     *  E = Steuerbefreit
     *  AE = Umkehrung der Steuerschuldnerschaft
     *  K = Kein Ausweis der Umsatzsteuer bei innergemeinschaftlichen Lieferungen
     *  G = Steuer nicht erhoben aufgrund von Export außerhalb der EU
     *  O = Außerhalb des Steueranwendungsbereichs
     */
    public static TaxCategoryCodeType getTaxCategoryCodeType(TaxType taxType) {
        TaxCategoryCodeType taxCategoryCodeType =  new TaxCategoryCodeType();

        if (taxType != null) {
            taxCategoryCodeType.setValue(TaxCategoryCodeContentType.fromValue(taxType.getTaxTypeCode()));
        } else {
            taxCategoryCodeType.setValue(TaxCategoryCodeContentType.S);
        }

        return taxCategoryCodeType;
    }


    /** */
    public static TradeTaxType createTradeTax(Integer vatRate, TaxType taxType, BigDecimal calculatedAmount, BigDecimal basisAmount ) {

        TradeTaxType tradeTaxType = new TradeTaxType();
        tradeTaxType.setCategoryCode(En16931Util.getTaxCategoryCodeType(taxType));
        if (TaxType.UNCONSIDERED != taxType) {
            tradeTaxType.setRateApplicablePercent(En16931Util.fromPercent(vatRate));
        } else {
            tradeTaxType.setExemptionReason(En16931Util.fromText("see country invoice"));
        }
        if (calculatedAmount != null && basisAmount != null) {
            tradeTaxType.setCalculatedAmount(En16931Util.fromAmount(calculatedAmount, null));
            tradeTaxType.setBasisAmount(En16931Util.fromAmount(basisAmount, null));
        }
        tradeTaxType.setTypeCode(En16931Util.getTaxTypeCodeVat());
        return tradeTaxType;
    }

    public static ProductCharacteristicType createProductCharacteristic(String key, String value) {
        ProductCharacteristicType productCharacteristic = new ProductCharacteristicType();
        productCharacteristic.setDescription(fromText(key));
        productCharacteristic.setValue(fromText(value));
        return productCharacteristic;
    }

    public static IndicatorType getTestIndicatorType() {
        IndicatorType indicatorType = new IndicatorType();
        indicatorType.setIndicator(Boolean.TRUE);
        return indicatorType;
    }

    public static UniversalCommunicationType getEmailCommunication(String email) {

        UniversalCommunicationType communicationType = new UniversalCommunicationType();
        communicationType.setURIID(fromValue(email, "URIID"));
        return communicationType;
    }

    public static UniversalCommunicationType getFaxOrPhoneCommunication(String faxOrPhoneNumber) {
        UniversalCommunicationType communicationType = new UniversalCommunicationType();
        communicationType.setCompleteNumber(fromText(faxOrPhoneNumber));
        return communicationType;
    }


}
