package de.svg.service.en16931;

import de.svg.api.en16931.*;
import de.svg.config.En16931NamespacePrefixMapper;
import de.svg.dto.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
@Slf4j
public class En16931XmlGenerationService {

    private final ObjectFactory objectFactory;

    @Value("${en16931.profil}")
    private String en16931Profil;

    @Value("${en16931.test}")
    private boolean testIndicator;

    public String doTheXmlThing(SvgInvoice svgInvoice) throws JAXBException {


        CrossIndustryInvoiceType crossIndustryInvoiceType = objectFactory.createCrossIndustryInvoiceType();

        JAXBContext context = JAXBContext.newInstance(CrossIndustryInvoiceType.class);
        Marshaller mar= context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.name());
        mar.setProperty("com.sun.xml.bind.namespacePrefixMapper", new En16931NamespacePrefixMapper());

        // ExchangedDocumentContext Prozesssteuerung
        crossIndustryInvoiceType.setExchangedDocumentContext(createExchangedDocumentContext());


        // ExchangedDocument : Eigenschaften, die das gesamte Dokument betreffen.
        crossIndustryInvoiceType.setExchangedDocument(
                createExchangedDocument(svgInvoice.getInvoiceNumber(), svgInvoice.getInvoiceDate(), svgInvoice.getComments())
        );

        // SupplyChainTradeTransaction; Informationen zum Geschäftsvorfall, Rechnungsdaten und Vertragspartner
        crossIndustryInvoiceType.setSupplyChainTradeTransaction(
                createSupplyChainTradeTransaction(svgInvoice)
        );

        crossIndustryInvoiceType.getExchangedDocument();


        StringWriter sw = new StringWriter();
        JAXBElement<CrossIndustryInvoiceType> crossIndustryInvoice = objectFactory.createCrossIndustryInvoice(crossIndustryInvoiceType);
        mar.marshal(crossIndustryInvoice, sw);

        return convertToUTF8(sw.toString());
    }

    private static String convertToUTF8(String isoString)  {

        byte[] bytearray = isoString.getBytes();

        CharsetEncoder utf8Encoder =
                StandardCharsets.UTF_8.newEncoder()
                        .onMalformedInput(CodingErrorAction.REPLACE)
                        .onUnmappableCharacter(CodingErrorAction.REPLACE);

        CharBuffer input = CharBuffer.wrap(isoString);
        ByteBuffer encodedByteBuffer = null;
        try {
            encodedByteBuffer = utf8Encoder.encode(input);
            // Now let's convert this ByteBuffer to String
            return StandardCharsets.UTF_8.decode(encodedByteBuffer).toString();
        } catch (CharacterCodingException e) {
            log.warn("error encoding to utf8", e);
        }

        return isoString;
    }

    private SupplyChainTradeTransactionType createSupplyChainTradeTransaction(SvgInvoice svgInvoice) {

        SupplyChainTradeTransactionType supplyChainTradeTransaction =  objectFactory.createSupplyChainTradeTransactionType();

        HeaderTradeDeliveryType tradeDeliveryType = new HeaderTradeDeliveryType();
        supplyChainTradeTransaction.setApplicableHeaderTradeDelivery(tradeDeliveryType);

        // Vertragsangaben
        HeaderTradeAgreementType headerTradeAgreement = new HeaderTradeAgreementType();
        //Leitweg ID
        String globalRefNumber = svgInvoice.getBuyer().getGlobalReferenceNumber() != null ? svgInvoice.getBuyer().getGlobalReferenceNumber() : svgInvoice.getBuyer().getId();
        headerTradeAgreement.setBuyerReference(En16931Util.fromText(globalRefNumber));

        // Käufer
        headerTradeAgreement.setBuyerTradeParty(
                createTradeParty(svgInvoice.getBuyer())
        );

        // Verkäufer
        headerTradeAgreement.setSellerTradeParty(
                createTradeParty(svgInvoice.getSeller())
        );
        supplyChainTradeTransaction.setApplicableHeaderTradeAgreement(headerTradeAgreement);

        // Angaben zur Zahlung und Rechnungsausgleich
        supplyChainTradeTransaction.setApplicableHeaderTradeSettlement(
                createHeaderTradeSettlement(
                        svgInvoice.getCurrencyCode(),
                        svgInvoice.getPaymentTerms(),
                        svgInvoice.getItems(),
                        svgInvoice.getVatCorrections(),
                        svgInvoice.getInvoiceType()));

        //Rechnungspositionen
        List<InvoiceItem> items = svgInvoice.getItems();
        IntStream.range(0, (items.size()) )
                .forEach(index ->
                        supplyChainTradeTransaction.getIncludedSupplyChainTradeLineItem()
                                .add(createSupplyChainTradeLineItem(items.get(index), index)
                                )
                );

        return supplyChainTradeTransaction;
    }


    // Angaben zur Zahlung und Rechnungsausgleich
    private HeaderTradeSettlementType createHeaderTradeSettlement(
            String currencyCode,
            PaymentTerms paymentTerms,
            List<InvoiceItem> items,
            List<VatCorrection> vatCorrections,
            InvoiceType invoiceType) {
        HeaderTradeSettlementType headerTradeSettlementType = new HeaderTradeSettlementType();

        //headerTradeSettlementType.setTaxCurrencyCode(En16931Util.getCurrency(currencyCode));
        headerTradeSettlementType.setInvoiceCurrencyCode(En16931Util.getCurrency(currencyCode));

        //payment terms might not be filled for country invoices
        boolean paymentAlreadyReceived = false;
        if (paymentTerms != null) {
            paymentAlreadyReceived = paymentTerms.isPaymentAlreadyReceived();
            // Zahlungsanweisungen
            TradePaymentTermsType tradePaymentTerms = new TradePaymentTermsType();
            if (paymentTerms.getDueDate() != null) {
                tradePaymentTerms.setDueDateDateTime(En16931Util.fromLocalDate(paymentTerms.getDueDate())); //Fälligkeit
            }
            tradePaymentTerms.setDescription(En16931Util.fromText(paymentTerms.getDescription()));  //Zahlungsbedingungen

            TradeSettlementPaymentMeansType tradeSettlementPaymentMeans = new TradeSettlementPaymentMeansType();

            if (PaymentType.SEPA == paymentTerms.getType()) {
                headerTradeSettlementType.setCreditorReferenceID(En16931Util.fromValue(paymentTerms.getSepaId(), null)); //Sepa Gläubiger-ID
                tradePaymentTerms.setDirectDebitMandateID(En16931Util.fromValue(paymentTerms.getSepaMandat(), null)); //SepaMandat

                // 59 : SEPA Direct Debit
                tradeSettlementPaymentMeans.setTypeCode(En16931Util.getPaymentMeansSepaDirectDebit());
                DebtorFinancialAccountType debtorFinancialAccount = new DebtorFinancialAccountType();
                debtorFinancialAccount.setIBANID(En16931Util.fromValue(paymentTerms.getBuyerAccount().getIban(), "IBANID")); //Buyers IBAN
                tradeSettlementPaymentMeans.setPayerPartyDebtorFinancialAccount(debtorFinancialAccount);

            }
            else if (PaymentType.DIRECT_DEBIT == paymentTerms.getType()) {
                // 49 : bank transfer
                tradeSettlementPaymentMeans.setTypeCode(En16931Util.getPaymentMeansDirectDebit());

            }
            else if (PaymentType.BANK_TRANSFER == paymentTerms.getType()) {
                // 30 : bank transfer
                tradeSettlementPaymentMeans.setTypeCode(En16931Util.getPaymentMeansBankTransfer());

                //IBAN und BIC der SVG
                CreditorFinancialAccountType creditorFinancialAccount = new CreditorFinancialAccountType();
                creditorFinancialAccount.setIBANID(En16931Util.fromValue(paymentTerms.getSellerAccount().getIban(), "IBANID"));
                tradeSettlementPaymentMeans.setPayeePartyCreditorFinancialAccount(creditorFinancialAccount);

                CreditorFinancialInstitutionType creditorFinancialInstitution = new CreditorFinancialInstitutionType();
                creditorFinancialInstitution.setBICID(En16931Util.fromValue(paymentTerms.getSellerAccount().getBic(), "BICID"));
                tradeSettlementPaymentMeans.setPayeeSpecifiedCreditorFinancialInstitution(creditorFinancialInstitution);

            }
            //not defined
            else {
                tradeSettlementPaymentMeans.setTypeCode(En16931Util.getPaymentMeansNotDefined());
            }

            headerTradeSettlementType.getSpecifiedTradePaymentTerms().add(tradePaymentTerms);
            headerTradeSettlementType.getSpecifiedTradeSettlementPaymentMeans().add(tradeSettlementPaymentMeans);
        }

        // Umsatzsteuersummen gruppiert nach Prozentsatz: Pflichtfeld
        //if (InvoiceType.COUNTRY_INVOICE.equals(invoiceType)) {
        Map<Integer, List<InvoiceItem>> groupedItems =
                items
                    .stream()
                    .collect(Collectors.groupingBy(InvoiceItem::getVatRate));


        groupedItems.forEach((vatRate, svgInvoiceItems) -> {

                    BigDecimal sumVatAmount = svgInvoiceItems.stream().map(InvoiceItem::getVatAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
                    if (vatCorrections != null) {
                        BigDecimal correctionVatAmount =
                                vatCorrections.stream()
                                        .filter(vatCorrection -> vatRate.equals(vatCorrection.getVatRate()))
                                        .findFirst().map(VatCorrection::getCorrectionVatAmount)
                                        .orElse(BigDecimal.ZERO);
                        sumVatAmount = sumVatAmount.add(correctionVatAmount);
                    }

                    BigDecimal sumTotalAmount = svgInvoiceItems.stream().map(InvoiceItem::getTotalNetPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
                    TaxType taxType = svgInvoiceItems.stream().findFirst().map(InvoiceItem::getTaxType).orElse(TaxType.NORMAL);
                    headerTradeSettlementType.getApplicableTradeTax().add(En16931Util.createTradeTax(vatRate, taxType, sumVatAmount, sumTotalAmount));
                }
        );

        //Rechnungssummen
        headerTradeSettlementType.setSpecifiedTradeSettlementHeaderMonetarySummation(createHeaderMonetarySummation(items, vatCorrections, currencyCode, paymentAlreadyReceived ));
        return headerTradeSettlementType;
    }

    /** sums of invoice items */
    private TradeSettlementHeaderMonetarySummationType createHeaderMonetarySummation(
            List<InvoiceItem> items,
            List<VatCorrection> vatCorrections,
            String currencyCode,
            boolean prepaid) {

        TradeSettlementHeaderMonetarySummationType monetarySummation = new TradeSettlementHeaderMonetarySummationType();

        //only gross price is available in collectice invoice items
        BigDecimal sumGrossPrice = items.stream().map(InvoiceItem::getTotalGrossPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (vatCorrections != null) {
            BigDecimal sumCorrectionGrossmount = vatCorrections.stream().map(VatCorrection::getCorrectionGrossAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            sumGrossPrice = sumGrossPrice.add(sumCorrectionGrossmount);
        }

        BigDecimal sumNetPrice = items.stream().map(InvoiceItem::getTotalNetPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal sumVatAmount = items.stream().map(InvoiceItem::getVatAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        if (vatCorrections != null) {
            BigDecimal sumCorrectionVatAmount = vatCorrections.stream().map(VatCorrection::getCorrectionVatAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            sumVatAmount = sumVatAmount.add(sumCorrectionVatAmount);
        }

        // LineTotalAmount: Summe aller Rechnungspositions-Nettobeträge der Rechnung
        monetarySummation.setLineTotalAmount(En16931Util.fromAmount(sumNetPrice, null));
        // Die Gesamtsumme der Rechnung ohne Umsatzsteuer:
        // Der Rechnungsgesamtbetrag ohne Umsatzsteuer ist die Summe der Rechnungspositions-Nettobeträge abzüglich
        // der Summe der Zuschläge auf Dokumentenebene zuzüglich der Summe der Abschläge der Dokumentenebene.
        // (fuer SVG: wir haben allerdings keine Abschläge und oder Zuschläge auf Dokumentenebene)
        monetarySummation.getTaxBasisTotalAmount().add(En16931Util.fromAmount(sumNetPrice, null));
        // Der Gesamtbetrag der Umsatzsteuer für die Rechnung: Der Währungscode muss gier angegeben werden
        monetarySummation.getTaxTotalAmount().add(En16931Util.fromAmount(sumVatAmount, currencyCode));
        // Der Gesamtbetrag der Rechnung mit Umsatzsteuer
        monetarySummation.getGrandTotalAmount().add(En16931Util.fromAmount(sumGrossPrice, null));
        // Betrag bereits erhalten.
        if (prepaid) {
            monetarySummation.setTotalPrepaidAmount(En16931Util.fromAmount(sumGrossPrice, null));
            // Der ausstehende Betrag, um dessen Zahlung gebeten wir
            monetarySummation.setDuePayableAmount(En16931Util.fromAmount(BigDecimal.ZERO, null));
        } else {
            // Der ausstehende Betrag, um dessen Zahlung gebeten wir
            monetarySummation.setDuePayableAmount(En16931Util.fromAmount(sumGrossPrice, null));
        }

        return monetarySummation;
    }


    private TradeSettlementLineMonetarySummationType createLineMonetarySummation(InvoiceItem invoiceItem) {

        TradeSettlementLineMonetarySummationType lineMonetarySummationType = new TradeSettlementLineMonetarySummationType();

        //Nettobetrag der Rechnungsposition
        lineMonetarySummationType.setLineTotalAmount(En16931Util.fromAmount(invoiceItem.getTotalNetPrice(), null));
        // Gesamtbetrag der Positionszu- und Abschläge

        // lineMonetarySummationType.setTotalAllowanceChargeAmount();

        return lineMonetarySummationType;
    }

    private TradePartyType createTradeParty(InvoiceTradeParty svgTradeParty) {

        TradePartyType tradeParty = new TradePartyType();

//        tradeParty.setDefinedTradeContact();
        tradeParty.setDescription(En16931Util.fromText(svgTradeParty.getDescription()));
        tradeParty.setName(En16931Util.fromText(svgTradeParty.getName()));

        //Kundennummer
        IDType tradePartyId = new IDType();
        tradePartyId.setValue(svgTradeParty.getId());
        tradeParty.getID().add(tradePartyId);

        TradeAddressType address = new TradeAddressType();
        address.setCityName(En16931Util.fromText(svgTradeParty.getCity()));

        address.setCountryID(En16931Util.fromIsoCounty(svgTradeParty.getIsoCountryCode()));
        address.setLineOne(En16931Util.fromText(svgTradeParty.getStreet()));
//        address.setLineTwo(En16931Util.fromText("line2"));
//        address.setLineThree(En16931Util.fromText("line3"));
        CodeType postcode = new CodeType();
        postcode.setValue(svgTradeParty.getZipCode());
        address.setPostcodeCode(postcode);
        tradeParty.setPostalTradeAddress(address);

        TaxRegistrationType taxRegistration = new TaxRegistrationType();
        IDType id = new IDType();
        //Umsatzsteurnummer oder Steuernummer
        if (svgTradeParty.getVatNumber() != null) {
            id.setValue(StringUtils.deleteWhitespace(svgTradeParty.getVatNumber()));
            id.setSchemeID("VA"); //Umsatzsteuernummer
        } else if (svgTradeParty.getFiscalNumber() != null) {
            id.setValue(StringUtils.deleteWhitespace(svgTradeParty.getFiscalNumber()));
            id.setSchemeID("FC"); //Steuernummer
        }
        taxRegistration.setID(id);
        tradeParty.getSpecifiedTaxRegistration().add(taxRegistration);

        //Ansprechpartner
        InvoiceContactPerson contact = svgTradeParty.getContact();
        if (contact != null) {
            TradeContactType tradeContact = new TradeContactType();
            tradeContact.setPersonName(En16931Util.fromText(contact.getName() != null ? contact.getName() : "n/a"));
            if (contact.getEmail() != null) {
                tradeContact.setEmailURIUniversalCommunication(En16931Util.getEmailCommunication(contact.getEmail()));
            }
            if (contact.getPhone() != null) {
                tradeContact.setTelephoneUniversalCommunication(En16931Util.getFaxOrPhoneCommunication(contact.getPhone()));
            }
            if (contact.getFax() != null) {
                tradeContact.setFaxUniversalCommunication(En16931Util.getFaxOrPhoneCommunication(contact.getFax()));
            }
            tradeParty.setDefinedTradeContact(tradeContact);
        }
        return tradeParty;
    }

    // Rechnungsposition
    private SupplyChainTradeLineItemType createSupplyChainTradeLineItem(InvoiceItem invoiceItem, int index) {

        SupplyChainTradeLineItemType item = new SupplyChainTradeLineItemType();

        //AssociatedDocumentLineDocument: allgemeinen Positionsangaben -> Positionsnummer
        // --------------------------------------------------------------------
        DocumentLineDocumentType documentLineDocumentType = new DocumentLineDocumentType();
        documentLineDocumentType.setLineID(En16931Util.fromValue(String.valueOf(index+1), "LineID"));
        item.setAssociatedDocumentLineDocument(documentLineDocumentType);

        //SpecifiedTradeProduct: Angaben zum Produkt bzw. zur erbrachten Leistung -> Produktbeschreibung
        // --------------------------------------------------------------------
        TradeProductType product = new TradeProductType();
        product.setName(En16931Util.fromText(invoiceItem.getItemText()));
        IDType id = new IDType();
        id.setValue(invoiceItem.getItemId().toString());
        product.setSellerAssignedID(id);
        product.setDescription(En16931Util.fromText(invoiceItem.getDescription()));
        //item characteristics / item attributes
        if (!CollectionUtils.isEmpty(invoiceItem.getAttributes())) {
            invoiceItem.getAttributes().forEach( (key, value) -> product.getApplicableProductCharacteristic().add(En16931Util.createProductCharacteristic(key, value)) );
        }
        item.setSpecifiedTradeProduct(product);

        // SpecifiedLineTradeAgreement:  Informationen über den Preis für die in der betreffenden Rechnungsposition in Rechnung gestellten Waren und Dienstleistungen
        // --------------------------------------------------------------------
        String unitCode = ItemUnit.LITER == invoiceItem.getUnit() ?  En16931Util.UNIT_LITRE : En16931Util.UNIT_ONE;
        LineTradeAgreementType lineTradeAgreement = new LineTradeAgreementType();
        if (invoiceItem.getUnitGrossPrice() != null) {
            lineTradeAgreement.setGrossPriceProductTradePrice(
                    En16931Util.priceFromValue(invoiceItem.getUnitGrossPrice(), null, null, unitCode)
            );
        }
        lineTradeAgreement.setNetPriceProductTradePrice(
                En16931Util.priceFromValue(invoiceItem.getUnitNetPrice(), invoiceItem.getRebate(), invoiceItem.getSurcharge(), unitCode )
        );

        item.setSpecifiedLineTradeAgreement(lineTradeAgreement);

        // SpecifiedLineTradeDelivery: Lieferangaben aus Positionsebene -> hier die Liefermenge
        // --------------------------------------------------------------------
        LineTradeDeliveryType lineTradeDelivery = new LineTradeDeliveryType();
        lineTradeDelivery.setBilledQuantity(En16931Util.fromQuantity(invoiceItem.getQuantity(), unitCode));

        item.setSpecifiedLineTradeDelivery(lineTradeDelivery);

        //TradeSettlement: Umsatzsteuerangaben und LineTotalAmount
        // --------------------------------------------------------------------
        LineTradeSettlementType lineTradeSettlement = new LineTradeSettlementType();
        lineTradeSettlement.getApplicableTradeTax().add(En16931Util.createTradeTax(invoiceItem.getVatRate(), invoiceItem.getTaxType(), null, null));
        lineTradeSettlement.setSpecifiedTradeSettlementLineMonetarySummation(createLineMonetarySummation(invoiceItem));
        item.setSpecifiedLineTradeSettlement(lineTradeSettlement);

        return item;
    }


    private ExchangedDocumentContextType createExchangedDocumentContext() {

        ExchangedDocumentContextType exchangedDocumentType = objectFactory.createExchangedDocumentContextType();

        DocumentContextParameterType type = new DocumentContextParameterType();
        IDType id = new IDType();
        id.setValue(en16931Profil);
        //id.setSchemeID("SchemeId23");
        type.setID(id);
        exchangedDocumentType.setGuidelineSpecifiedDocumentContextParameter(type);
        if (testIndicator) {
            exchangedDocumentType.setTestIndicator(En16931Util.getTestIndicatorType());
        }

        return exchangedDocumentType;
    }

    private ExchangedDocumentType createExchangedDocument(String invoiceNumber, LocalDate invoiceDate, List<String> comments) {

        ExchangedDocumentType exchangedDocumentType = objectFactory.createExchangedDocumentType();

        //Rechnungsnummer
        IDType id = new IDType();
        id.setValue(invoiceNumber);
        exchangedDocumentType.setID(id);

        // Rechnungsdatum
        exchangedDocumentType.setIssueDateTime(En16931Util.fromLocalDate(invoiceDate));

        // Typ derRechnung codiert.
        exchangedDocumentType.setTypeCode(En16931Util.getDocumentCodeTypeInvoice());

        //Bemerkungsfeld
        if (! CollectionUtils.isEmpty(comments)) {
            comments.forEach(c -> exchangedDocumentType.getIncludedNote().add(En16931Util.noteFromText(c)));
        }

        return exchangedDocumentType;

    }

}
