package de.svg;

import de.svg.api.en16931.ObjectFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class En16931GenerationApp  {


    public static void main(String[] args) {
        SpringApplication.run(En16931GenerationApp.class, args);

    }

    @Bean
    public ObjectFactory getObjectFactory() {
        return new ObjectFactory();
    }

}
